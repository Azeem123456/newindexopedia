<div>
  <transition name="fade" mode="out-in">
    <div class="greyout" v-if="!classdetails || loading"></div>
  </transition>
  <transition name="slidefade" mode="out-in">
    <div class="loader" v-if="!classdetails || loading">
      <div class="preloader-wrapper big active">
        <div class="spinner-layer spinner-blue-only">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div><div class="gap-patch">
            <div class="circle"></div>
          </div><div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>
      </div>
    </div>
  </transition>
  <!-- / loader -->

  <div style="padding:2%;padding-top:0;padding-bottom:0">
    <br>
    <nav class="blue darken-3 breadcrumbhead" v-if="classdetails">
      <div class="nav-wrapper p10-left breadcrumbhead">
        <div class="col s12">
          <a class="breadcrumb" @click="$router.push('/maincategories')">CATEGORIES</a>
          <a class="breadcrumb" v-if="classdetails.subparent" @click="$router.push('/category/'+classdetails.subparent.catid)">{{classdetails.subparent.catname.toUpperCase()}}</a>
          <a class="breadcrumb" v-if="!classdetails.subparent" @click="$router.push('/class/'+classdetails.parent.catid)">{{classdetails.parent.catname.toUpperCase()}}</a>
          <a class="breadcrumb" v-if="classdetails.subparent" @click="$router.push('/class/'+classdetails.parent.catid)">{{classdetails.parent.catname.toUpperCase()}}</a>
            <span class="breadcrumb" >{{classdetails.classdetails.classname.toUpperCase()}}</span>
        </div>
      </div>
    </nav>
    <div class="card blue-grey darken-1 full-width" v-if="classdetails">
      <div class="card-content white-text">
        <span class="card-title txt-capitalize">
          {{classdetails.classdetails.classname}}
          <label class="right white-text" style="font-size:15px;"
          ><a    v-if="classdetails.classdetails.locked == 0"   @click="unlockTable(classdetails.classdetails.idclass)">
              <i class="material-icons bi">&#xE897;</i>
            Unlocked</a> </label>
            <label class="right white-text" style="font-size:15px;"

          > <a    v-if="classdetails.classdetails.locked == 1" @click="lockTable(classdetails.classdetails.idclass)"> <i class="material-icons bi">&#xE898;</i>
                    Locked </a></label>

        </span>



        <p>{{classdetails.classdetails.classdescription}}</p>
      </div>
    </div>
    <div class="row grey lighten-4" style="padding:15px;" v-if="userLogin && (classdetails && classdetails.classdetails.locked == 0)">
        <h5>&nbsp Delete / Edit Column</h5>
        <div class="col s12 m4">
            <select v-model="col_delete">
                <option value="">-- Select Column --</option>
                <option v-for="classprop in classdetails.table.classprops" :value="classprop.idclassproperty" v-if="classprop.status=='1'">{{classprop.propertyname}}</option>
            </select>
        </div>
        <div class="col s12 m8" align="right"><button class="btn" @click="editColumn()"><i class="material-icons" style="float:left;margin-right:10px">edit</i> Edit Column</button>
        <button class="btn" @click="deleteColumn()"><i class="material-icons" style="float:left;margin-right:10px">&#xE872;</i> Delete Column</button>
        </div>
    </div>
    <div class="row" v-if="classdetails">
        <h5>&nbsp Filter</h5>
        <div class="col s12 m4">
            <select v-model="search_column">
                <option value="">Search All</option>
                <option v-for="classprop in classdetails.table.classprops" :value="classprop.idclassproperty" v-if="classprop.propertytype != '5' && classprop.propertytype != '6' && classprop.propertytype != '7' && classprop.status != '0'">{{classprop.propertyname}}</option>
            </select>
        </div>
        <div class="col s12 m4">
        
        </div>
        <div class="col s12 m4">
            <input type="text" v-model="search_query" placeholder="Enter Search Query . . ">
        </div>
    </div>
    <hr>
    <table id="myTable" class="responsive-table" v-if="classdetails">
      <thead>
        <tr>

          <th style="cursor:hand;cursor:pointer;" v-for="(classprop, index) in classdetails.table.classprops" v-if="classprop.status == '1'" class="txt-capitalize" @click="sortTable(index)">
            {{classprop.propertyname}}
          </th>
          <th v-if="userLogin && classdetails.classdetails.locked == 0 " style="max-width:60px !important;width:100px;"></th>
          <!--<th v-if="userLogin && classdetails.classdetails.locked == 0  && (classdetails.kols.filter(kol => kol.iduser == userLogin.id)[0] || classdetails.classdetails.idowner == userLogin.id)" style="max-width:120px !important;width:120px;">Lock / Unlock</th>-->
        </tr>
      </thead>
      <tbody>
        <tr v-if="searched_objects.length == 0">
          <td :colspan="classdetails.table.classprops.length + 2" style="text-align:center !important;">
            No Data Found
          </td>
        </tr>
        <tr v-for="obj in searched_objects" v-if="obj.Status == '1'">
            <span v-if="userLogin && classdetails.classdetails.locked == 0 " >
                <i v-if="obj.locked == '1'" class="material-icons bi">&#xE897;</i>
                <i v-if="obj.locked == '0'" class="material-icons bi">&#xE898;</i>
            </span>
          <td v-for="classprop in classdetails.table.classprops" class="txt-capitalize" v-if="classprop.status == '1'">

            <span v-if="classprop.propertytype != '7' && classprop.propertytype != '5' && classprop.propertytype != '6'">
                {{ classdetails.table.propvalues.filter(propvalue => propvalue.idobject==obj.idobject && propvalue.idclassproperty==classprop.idclassproperty)[0] ?
                   classdetails.table.propvalues.filter(propvalue => propvalue.idobject==obj.idobject && propvalue.idclassproperty==classprop.idclassproperty)[0].value : ''
                }}
            </span>
            <div class="mediPlayer" v-else-if="classprop.propertytype == '6'">
            <audio class="listen" preload="none" data-size="50"
                   :src="classdetails.table.propvalues.filter(propvalue => propvalue.idobject==obj.idobject && propvalue.idclassproperty==classprop.idclassproperty)[0] ?
                   'uploads/'+classdetails.table.propvalues.filter(propvalue => propvalue.idobject==obj.idobject && propvalue.idclassproperty==classprop.idclassproperty)[0].value : ''"
                  >
            </audio>
            </div>
            <a @click="showImagePopup(classdetails.table.propvalues.filter(propvalue => propvalue.idobject==obj.idobject && propvalue.idclassproperty==classprop.idclassproperty)[0] ?
              'uploads/'+classdetails.table.propvalues.filter(propvalue => propvalue.idobject==obj.idobject && propvalue.idclassproperty==classprop.idclassproperty)[0].value : '')"
               v-else-if="classprop.propertytype == '5'">
              <img v-if="classdetails.table.propvalues.filter(propvalue => propvalue.idobject==obj.idobject && propvalue.idclassproperty==classprop.idclassproperty)[0]" style="height:25px;width:25px" :src="'uploads/'+classdetails.table.propvalues.filter(propvalue => propvalue.idobject==obj.idobject && propvalue.idclassproperty==classprop.idclassproperty)[0].value">
            </a>
            <a v-else @click="showVideoPopup(classdetails.table.propvalues.filter(propvalue => propvalue.idobject==obj.idobject && propvalue.idclassproperty==classprop.idclassproperty)[0] ?
              classdetails.table.propvalues.filter(propvalue => propvalue.idobject==obj.idobject && propvalue.idclassproperty==classprop.idclassproperty)[0].value : '')">
              Show Video
            </a>
          </td>
          <td v-if="userLogin && classdetails.classdetails.locked == 0 ">
             <a style="margin-right:15px;" class='btn btn-flat dropdown-trigger' :data-activates="'ddd'+obj.idobject"><i class="material-icons bi">&#xE5D3;</i></a>
              <ul :id="'ddd'+obj.idobject" class='dropdown-content'>
                <li v-if="obj.locked == '0'"><a @click="editRow(obj.idobject)"><i class="material-icons bi">&#xE254;</i> Edit</a></li>
                
                <li v-if="obj.locked == '0'"><a @click="deleteRow(obj.idobject)"><i class="material-icons bi">&#xE872;</i> Delete</a></li>
                
                <li v-if="userLogin && classdetails.classdetails.locked == 0  && (classdetails.kols.filter(kol => kol.iduser == userLogin.id)[0] || classdetails.classdetails.idowner == userLogin.id)">
                    
                    <a v-if="obj.locked == '0'" @click="lockobj(obj.idobject)"><i class="material-icons bi">&#xE897;</i> Lock</a>
                    
                    <a v-else @click="unlockobj(obj.idobject)"><i class="material-icons bi">&#xE898;</i> Unlock</a>
                    
                </li>
              </ul>
          </td>
            
        </tr>
      </tbody>
    </table>


    <div class="fixed-action-btn horizontal" v-if="userLogin && (classdetails && classdetails.classdetails.locked == 0) ">
      <a class="btn-floating btn-large red">
        <i class="large material-icons">mode_edit</i>
      </a>
      <ul>
        <li v-if="classdetails && classdetails.table.classprops.length > 0"><a class="btn-floating red tooltipped" @click="addRowPopup.visible = true" data-position="top" data-delay="50" data-tooltip="Add Row"><i class="material-icons">&#xE8E9;</i></a></li>
        
        <li><a class="btn-floating yello tooltipped" @click="addColumnPopup.visible = true;" data-position="top" data-delay="50" data-tooltip="Add Column"><i class="material-icons">&#xE8EC;</i></a></li>
      </ul>
    </div>

  </div>


<!--  Popups -->

  <div class="popupblackout" v-if="editColumnPopup.visible && classdetails"></div>
  <transition name="slidefade" mode="out-in">
    <div class="popupbox z-depth-4" v-if="editColumnPopup.visible && classdetails">
      <div class="inner-popup">
        <header class="blue darken-3">
          <br>
            <div class="header-text">Edit Column</div>
            <button @click="editColumnPopup.visible = false"><i class="material-icons">cancel</i></button>
        </header>
        <transition name="slidefadesmall" mode="out-in">
          <div class="progress" v-if="editColumnPopup.loading">
              <div class="indeterminate"></div>
          </div>
        </transition>
        <form v-on:submit.prevent="submitEditColumn()">
          <div class="content">
              <label>Column Name</label>
              <input type="text" v-model="editColumnPopup.name" placeholder="Column Name . . ">
              <label>Column Type</label>
              <select v-model="editColumnPopup.type" @on:change="coltype()" :disabled='!editColumnPopup.typeeditable'>
                <option value=""> -- Select Column Type --</option>
                <option value="1">Numeric</option>
                <option value="2">Text</option>
                <option value="3">Date</option>
                <option value="4">Time</option>
                <option value="5">Image</option>
                <option value="6">Audio</option>
                <option value="7">Video</option>
                <option value="8">Lookup</option>
              </select>
          </div>
          <div class="content" v-if="editColumnPopup.type == '8'">
              <label>Lookup Table</label>
            <select v-model="editColumnPopup.lookuptable" @on:change="assignlookupfields()">
              <option value="null"> -- Lookup Table -- </option>
              <option v-for="lookuptable in classdetails.lookuptables" :value="lookuptable.idclass" v-if="classdetails.classdetails.idclass != lookuptable.idclass && lookuptable.Status=='1'" >{{lookuptable.classname}}</option>
            </select><br>
            <label>Lookup Field</label>
            <select v-model="editColumnPopup.lookupfield">
              <option value="null"> -- Lookup Field -- </option>
              <option v-for="lookupfield in classdetails.lookupfields" :value="lookupfield.idclassproperty">{{lookupfield.propertyname}}</option>
            </select>
          </div>
          <div class="content">
              <label>Order</label>
              <input type="text" v-model="editColumnPopup.order">
          </div>
          <br>
          <center>
            <button type="submit"  class="waves-effect waves-light btn blue darken-3">Edit Column</button>
          </center>
        </form>
      </div>
    </div>
  </transition>
  
  
  <div class="popupblackout" v-if="addColumnPopup.visible && classdetails"></div>
  <transition name="slidefade" mode="out-in">
    <div class="popupbox z-depth-4" v-if="addColumnPopup.visible && classdetails">
      <div class="inner-popup">
        <header class="blue darken-3">
          <br>
            <div class="header-text">Add Column</div>
            <button @click="addColumnPopup.visible = false"><i class="material-icons">cancel</i></button>
        </header>
        <transition name="slidefadesmall" mode="out-in">
          <div class="progress" v-if="addColumnPopup.loading">
              <div class="indeterminate"></div>
          </div>
        </transition>
        <form v-on:submit.prevent="addColumn()">
          <div class="content">
              <input type="text" v-model="addColumnPopup.name" placeholder="Column Name . . ">
              <select v-model="addColumnPopup.type" @on:change="coltype()">
                <option value=""> -- Select Column Type --</option>
                <option value="1">Numeric</option>
                <option value="2">Text</option>
                <option value="3">Date</option>
                <option value="4">Time</option>
                <option value="5">Image</option>
                <option value="6">Audio</option>
                <option value="7">Video</option>
                <option value="8">Lookup</option>
              </select>
          </div>
          <div class="content" v-if="addColumnPopup.type == '8'">
            <select v-model="addColumnPopup.lookuptable" @on:change="assignlookupfields()">
              <option value="null"> -- Lookup Table -- </option>
              <option v-for="lookuptable in classdetails.lookuptables" :value="lookuptable.idclass" v-if="classdetails.classdetails.idclass != lookuptable.idclass && lookuptable.Status=='1'">{{lookuptable.classname}}</option>
            </select><br>
            <select v-model="addColumnPopup.lookupfield">
              <option value="null"> -- Lookup Field -- </option>
              <option v-for="lookupfield in classdetails.lookupfields" :value="lookupfield.idclassproperty" v-if="lookupfield.status == '1'">{{lookupfield.propertyname}}</option>
            </select>
          </div>
          <div class="content">
              <input type="text" v-model="addColumnPopup.order">
          </div>
          <br>
          <center>
            <button type="submit"  class="waves-effect waves-light btn blue darken-3">Add Column</button>
          </center>
        </form>
      </div>
    </div>
  </transition>

  <div class="popupblackout" v-if="addRowPopup.visible && classdetails"></div>
  <transition name="slidefade" mode="out-in">
    <div class="popupbox z-depth-4" v-if="addRowPopup.visible && classdetails">
      <div class="inner-popup">
        <header class="blue darken-3">
          <br>
            <div class="header-text">Add Row</div>
            <button @click="addRowPopup.visible = false"><i class="material-icons">cancel</i></button>
        </header>
        <transition name="slidefadesmall" mode="out-in">
          <div class="progress" v-if="addRowPopup.loading">
              <div class="indeterminate"></div>
          </div>
        </transition>
        <form v-on:submit.prevent="addRow()">
          <div class="content">
            <div v-for="prop in addRowPopup.props">
              <input type="number" :placeholder="'Please Enter '+prop.propertyname" v-model="prop.value" v-if="prop.propertytype == '1'" >
              <input type="text" :placeholder="'Please Enter '+prop.propertyname" v-model="prop.value" v-if="prop.propertytype == '2'" >
              <input type="date" :placeholder="'Please Select '+prop.propertyname"  v-model="prop.value" v-if="prop.propertytype == '3'">
              <input type="time" :placeholder="'Please Select '+prop.propertyname"  v-model="prop.value" v-if="prop.propertytype == '4'">
              <div v-if="prop.propertytype == '5'">
                Image :<br>
                <input id="imageupload" accept="image/*" type="file" v-on:change="fileInputChange('image')" :placeholder="'Please Select '+prop.propertyname">
                <hr>
              </div>
              <div v-if="prop.propertytype == '6'">
                Audio : <br>
                <input id="audioupload" accept="audio/*" type="file" v-on:change="fileInputChange('audio')" :placeholder="'Please Select '+prop.propertyname" >
                <hr>
              </div>
              <input type="text" placeholder="Please Enter Video Link"  v-model="prop.value" v-if="prop.propertytype == '7'">
              <select v-if="prop.propertytype == '8'" v-model="prop.value">
                <option value=""> -- Select {{prop.propertyname}} --</option>
                <option v-for="feild in classdetails.lookupvalues.filter(val => val.idclassproperty == prop.propertylookupfield)" :value="feild.value">{{feild.value}}</option>
              </select>
              <br>
            </div>
          </div>
          <br>
          <center>
            <button type="submit"  class="waves-effect waves-light btn blue darken-3">Add Row</button>
          </center>
        </form>
      </div>
    </div>
  </transition>



  <div class="popupblackout" v-if="editRowPopup.visible && classdetails"></div>
  <transition name="slidefade" mode="out-in">
    <div class="popupbox z-depth-4" v-if="editRowPopup.visible && classdetails">
      <div class="inner-popup">
        <header class="blue darken-3">
          <br>
          <div class="header-text">Edit Row</div>
          <button @click="editRowPopup.visible = false"><i class="material-icons">cancel</i></button>
        </header>
        <transition name="slidefadesmall" mode="out-in">
          <div class="progress" v-if="editRowPopup.loading">
            <div class="indeterminate"></div>
          </div>
        </transition>
        <form v-on:submit.prevent="submitEditRow()">
          <div class="content">
            <div v-for="prop in editRowPopup.props">
              <input type="number" :placeholder="'Please Enter '+prop.propertyname" v-model="prop.value" v-if="prop.propertytype == '1'" >
              <input type="text" :placeholder="'Please Enter '+prop.propertyname" v-model="prop.value" v-if="prop.propertytype == '2'" >
              <input type="date" :placeholder="'Please Select '+prop.propertyname"  v-model="prop.value" v-if="prop.propertytype == '3'">
              <input type="time" :placeholder="'Please Select '+prop.propertyname"  v-model="prop.value" v-if="prop.propertytype == '4'">
              <div v-if="prop.propertytype == '5'">
                Image :<br>
                <input id="imageupload" accept="image/*" type="file" v-on:change="fileInputChange('image')" :placeholder="'Please Select '+prop.propertyname">
                <hr>
              </div>
              <div v-if="prop.propertytype == '6'">
                Audio : <br>
                <input id="audioupload" accept="audio/*" type="file" v-on:change="fileInputChange('audio')" :placeholder="'Please Select '+prop.propertyname" >
                <hr>
              </div>
              <input type="text" placeholder="Please Enter Video Link"  v-model="prop.value" v-if="prop.propertytype == '7'">
              <select v-if="prop.propertytype == '8'" v-model="prop.value">
                <option value=""> -- Select {{prop.propertyname}} --</option>
                <option v-for="feild in classdetails.lookupvalues.filter(val => val.idclassproperty == prop.propertylookupfield)" :value="feild.value">{{feild.value}}</option>
              </select>
              <br>
            </div>
          </div>
          <br>
          <center>
            <button type="submit"  class="waves-effect waves-light btn blue darken-3">Save Data</button>
          </center>
        </form>
      </div>
    </div>
  </transition>
</div>
