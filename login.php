<div class="section">
  <div class="container" align='center'>
      <div  class="login-box" style="height:590px;margin-top:15px">
        <transition name="slidefadesmall" mode="out-in">
          <div class="progress" v-if="loading">
              <div class="indeterminate"></div>
          </div>
        </transition>
        <div  class="login-logo">
          Indexopedia
        </div>
        <div  class="login-txt">
          Login Below
        </div>
        <br >
        <form v-on:submit.prevent="login()">
            <div class="input-field dirty">
              <i  class="material-icons prefix" style="margin-top:11px;"></i>
              <input  mz-input="" v-model="email" name="email" placeholder="Enter Your Email / Username. . " type="email" >
            <label for="undefined" class="active"></label>
          </div>
            <div class="input-field dirty">
              <i  class="material-icons prefix" style="margin-top:11px;"></i>
              <input  mz-input="" v-model="password" name="password" placeholder="Enter Your Password . . " type="password" >
            <label for="undefined" class="active"></label>
          </div>
            <div  class="row">
              <div  class="col s12 m12">
                <button  mz-button="" style="width:100%;" type="submit" class="btn btn-large waves-effect waves-light">
                  <i  class="material-icons" style="float:left;"></i>
                  Login
                </button>
                
                <button onClick="login()" mz-button="" style="width:100%;margin-top:5px;" type="button" class="btn btn-large waves-effect waves-light">
                  <i  class="material-icons" style="float:left;"></i>
                  Login Using Facebook
                </button>
              </div>
            </div>
        </form>
        <div align="center" style="margin-top:10px">
            <router-link to="/signup"><b>Register</b></router-link>
            <br>or<br>
            <router-link to="/"><b>Continue Without Login</b></router-link>
        </div>
      </div>
  </div>
</div>
