<div>
  <div class="container">
    <br>
    <nav class="blue darken-3 breadcrumbhead">
      <div class="nav-wrapper p10-left breadcrumbhead">
        <div class="col s12">
          <a class="breadcrumb" @click="$router.push('/')">CATEGORIES</a>
          <a class="breadcrumb" @click="$router.push('/audittrials')">AUDIT TRAIL</a>
        </div>
      </div>
    </nav>
    <br>
    <div class="card full-width teal darken-4">
      <div class="card-content">
        <span class="card-title txt-capitalize white-text">Audit Trail</span>
      </div>
    </div>
    <div style="width:100%">
        <div>
            <button class="btn" @click="page='classes'">Classes</button>
            <button class="btn" @click="page='properties'">Properties</button>
            <button class="btn" @click="page='propertyvalues'">Property Values</button>
        </div>
        <!-- Classes Page Start-->
        <div v-if="page=='classes'">
            <br>
            <button class="btn" @click="selected_class = ''" v-if="classesaudit.length > 0"> < </button>
            <table v-if="classesaudit.length == 0 && audit">
                <thead>
                    <tr>
                        <th>Class Name</th>
                        <th>Updated Date</th>
                        <th>Updated By</th>
                        <th>Details</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="clas in audit.classes">
                        <td><router-link :to="'/details/'+clas.idclass">{{clas.classname}}</router-link></td>
                        <td>{{clas.change_date}}</td>
                        <td>{{clas.firstname}}</td>
                        <td><a @click="selected_class = clas.idclass">...</a></td>
                    </tr>
                </tbody>
            </table>
            <table v-if="classesaudit.length > 0">
                <thead>
                    <tr>
                        <th>Class Name</th>
                        <th>Class Description</th>
                        <th>Updated By</th>
                        <th>Action</th>
                        <th>Updated Date</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(clasaudit, key) in classesaudit">
                        <td><router-link :to="'/details/'+clasaudit.idclass">{{clasaudit.classname}}</router-link></td>
                        <td>{{clasaudit.classdescription}}</td>
                        <td>{{clasaudit.firstname}}</td>
                        <td>{{clasaudit.action}}</td>
                        <td>{{clasaudit.change_date}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- Classes Page END-->
        
        <!---->
        <div v-if="page=='properties'">
            <br>
            <button class="btn" @click="selected_classprop = ''" v-if="propertyaudit.length > 0"> < </button>
            <table v-if="propertyaudit.length == 0 && audit">
                <thead>
                    <tr>
                        <th>Class Name</th>
                        <th>Field Name</th>
                        <th>Updated By</th>
                        <th>Updated Date</th>
                        <th>...</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="classprop in audit.classproperties">
                        <td><router-link :to="'/details/'+classprop.idclass">{{classprop.classname}}</router-link></td>
                        <td>{{classprop.propertyname}}</td>
                        <td>{{classprop.username}}</td>
                        <td>{{classprop.change_date}}</td>
                        <td><a @click="selected_classprop=classprop.idclassproperty">...</a></td>
                    </tr>
                </tbody>
            </table>
            <table v-if="propertyaudit.length > 0">
                <thead>
                    <tr>
                        <th>class name</th>
                        <th>field name</th>
                        <th>field type</th>
                        <th>field order</th>
                        <th>lookup table</th>
                        <th>lookup field</th>
                        <th>updated by</th>
                        <th>updated date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(classprop, key) in propertyaudit">
                        <td><router-link :to="'/details/'+classprop.idclass">{{classprop.classname}}</router-link></td>
                        
                        <td>{{classprop.propertyname}}</td>
                        
                        <td>{{classprop.propertytype}}</td>
                        
                        <td>{{classprop.propertyorder}}</td>
                        
                        <td>{{classprop.propertylookupclass}}</td>
                        <td>{{classprop.propertylookupfield}}</td>
                        <td>{{classprop.username}}</td>
                        <td>{{classprop.change_date}}</td>
                        <td>{{classprop.action}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
        
        
        
        
        
        
        <div v-if="page=='propertyvalues'">
            <br>
            <button class="btn" @click="selected_classpropvalue = null; show_propvaluesaudit = false" v-if="show_propvaluesaudit"> < </button>
            <table v-if="!show_propvaluesaudit && audit">
                <thead>
                    <tr>
                        <th>Class Name</th>
                        <th>Field Name</th>
                        <th>Value</th>
                        <th>Updated By</th>
                        <th>Updated Date</th>
                        <th>...</th>
                    </tr>
                </thead>
                <tbody>
                        <tr v-for="propvalue in audit.propertyvalues">
                            <td><router-link :to="'/details/'+propvalue.idclass">{{propvalue.classname}}</router-link></td>
                            <td>{{propvalue.propname}}</td>
                            <td>{{propvalue.value}}</td>
                            <td>{{propvalue.username}}</td>
                            <td>{{propvalue.change_date}}</td>
                            <td><a @click="selected_classpropvalue = propvalue;show_propvaluesaudit = true;">...</a></td>
                        </tr>
                </tbody>
            </table>
            <table v-if="show_propvaluesaudit">
                <thead>
                    <tr>
                        <th>Class Name</th>
                        <th>Field Name</th>
                        <th>Value</th>
                        <th>Updated By</th>
                        <th>Updated Date</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                        <tr v-for="propvalue in propertyvaluesaudit">
                            <td><router-link :to="'/details/'+propvalue.idclass">{{propvalue.classname}}</router-link></td>
                            <td>{{propvalue.propname}}</td>
                            <td>{{propvalue.value}}</td>
                            <td>{{propvalue.username}}</td>
                            <td>{{propvalue.change_date}}</td>
                            <td>{{propvalue.action}}</td>
                        </tr>
                </tbody>
            </table>
        </div>
    </div>
  <!-- Loader -->

    <transition name="fade" mode="out-in">
      <div class="greyout" v-if="loading"></div>
    </transition>
    <transition name="slidefade" mode="out-in">
      <div class="loader" v-if="loading">
        <div class="preloader-wrapper big active">
          <div class="spinner-layer spinner-blue-only">
            <div class="circle-clipper left">
              <div class="circle"></div>
            </div><div class="gap-patch">
              <div class="circle"></div>
            </div><div class="circle-clipper right">
              <div class="circle"></div>
            </div>
          </div>
        </div>
      </div>
    </transition>
  </div>
</div>
