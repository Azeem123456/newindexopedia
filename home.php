<div>
  <div class="container">
    <br>
    <div class="card-panel blue darken-3 white-text">
      <h4 class="canhover" style="font-weight:black;" @click="$router.push('maincategories')">Categories</h4>
    </div>
    <div class="row">
      <div class="col s12 m6">
        <div class="card-panel grey lighten-4 full-width">
          <h5>Favorites</h5>
          <ul class="collection">
            <li class="collection-item avatar">
              <i class="material-icons circle">&#xE3E7;</i>
              <span class="title" style="cursor:hand;cursor:pointer" @click="$router.push('/class/1')"><b>Electronics</b></span>
              <p>
                Stuff that needs electric current to turn on
              </p>
              <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
            </li>
          </ul>
        </div>
      </div>
      <div class="col s12 m6">
        <div class="card-panel grey lighten-4 full-width">
          <h5>Recent Visited</h5>
          <ul class="collection">
            <li class="collection-item avatar">
              <i class="material-icons circle">&#xE3E7;</i>
              <span class="title" style="cursor:hand;cursor:pointer;" @click="$router.push('/class/1')"><b>Electronics</b></span>
              <p>
                Stuff that needs electric current to turn on
              </p>
              <a href="#!" class="secondary-content"><i class="material-icons">grade</i></a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
