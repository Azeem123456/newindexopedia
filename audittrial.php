<div>
  <div class="container">
    <br>
    <nav class="blue darken-3 breadcrumbhead">
      <div class="nav-wrapper p10-left breadcrumbhead">
        <div class="col s12">
          <a class="breadcrumb" @click="$router.push('/')">HOME</a>
          <a class="breadcrumb" @click="$router.push('/notifications')">NOTIFICATIONS</a>
        </div>
      </div>
    </nav>
    <br>
    <div class="card full-width teal darken-4">
      <div class="card-content">
        <span class="card-title txt-capitalize white-text">Notifications</span>
      </div>
    </div>
    <table class="striped responsive-table">
      <tr v-if="!notifications">
        <td align="center">You Have No Notifications</td>
      </tr>
      <tr v-if="notifications" v-for="notification in notifications">
        <td v-html="notification.notification"></td>
      </tr>
    </table>
  <!-- Loader -->

    <transition name="fade" mode="out-in">
      <div class="greyout" v-if="loading"></div>
    </transition>
    <transition name="slidefade" mode="out-in">
      <div class="loader" v-if="loading">
        <div class="preloader-wrapper big active">
          <div class="spinner-layer spinner-blue-only">
            <div class="circle-clipper left">
              <div class="circle"></div>
            </div><div class="gap-patch">
              <div class="circle"></div>
            </div><div class="circle-clipper right">
              <div class="circle"></div>
            </div>
          </div>
        </div>
      </div>
    </transition>
  </div>
</div>
