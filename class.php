<div>
  <div class="container">
    <transition name="slidefade" mode="out-in">
      <div class="progress" v-if="loading" style="margin-top:15px !important;">
          <div class="indeterminate"></div>
      </div>
    </transition>
    <transition name="fade" mode="out-in">
      <div class="greyout" v-if="!classes"></div>
    </transition>
    <transition name="slidefade" mode="out-in">
      <div class="loader" v-if="!classes">
        <div class="preloader-wrapper big active">
          <div class="spinner-layer spinner-blue-only">
            <div class="circle-clipper left">
              <div class="circle"></div>
            </div><div class="gap-patch">
              <div class="circle"></div>
            </div><div class="circle-clipper right">
              <div class="circle"></div>
            </div>
          </div>
        </div>
      </div>
    </transition>
    <br>
    <nav class="blue darken-3 breadcrumbhead" v-if="classes">
      <div class="nav-wrapper p10-left breadcrumbhead">
        <div class="col s12">
          <a class="breadcrumb" @click="$router.push('/maincategories')">CATEGORIES</a>
          <a class="breadcrumb" v-if="classes.subparent" @click="$router.push('/category/'+classes.subparent.catid)">{{classes.subparent.catname.toUpperCase()}}</a>
          <a class="breadcrumb" v-if="!classes.subparent" @click="$router.push('/class/'+classes.parent.catid)">{{classes.parent.catname.toUpperCase()}}</a>
          <a class="breadcrumb" v-if="classes.subparent" @click="$router.push('/class/'+classes.parent.catid)">{{classes.parent.catname.toUpperCase()}}</a>
        </div>
      </div>
    </nav>
    <br>
    <ul class="collection main-cat" v-if="classes">
        <li class="collection-item" v-for="clas of classes.cats" v-if="clas.Status=='1'">
          <div class="txt-capitalize"><a @click="$router.push('/details/'+clas.idclass)"><b>{{clas.classname}} </b>({{clas.objectscount}})</a>
          <a style="float:right;" v-if="userLogin && userLogin.level=='admin'" class="dropdown-button secondary-content" :data-activates='"sub-cat-dropdown"+clas.idclass'><i class="material-icons">&#xE5D3;</i></a>
          <i v-if="clas.locked == 1" style="font-size:15px;margin-right:10px;float:right;margin-top:5px;" class="material-icons">&#xE897;</i>
          <i v-if="clas.isfollowing > 0" style="font-size:15px;margin-right:10px;float:right;margin-top:5px;" class="material-icons">rss_feed</i>
            <ul :id='"sub-cat-dropdown"+clas.idclass' class='dropdown-content'>
              <li v-if="userLogin && clas.idowner == userLogin.id" >
                  <a v-if="clas.locked == 0" @click="lock(clas.idclass)"><i class="material-icons bi">&#xE897;</i> Lock</a>
                  <a v-if="clas.locked == 1" @click="unlock(clas.idclass)"><i class="material-icons bi">&#xE898;</i> Unlock</a>
              </li>
              <li><a @click="editCategory(clas)"><i class="material-icons bi">&#xE254;</i>Edit</a></li>
              <li v-if="clas.objectscount == 0"><a @click="deleteCategory(clas)"><i class="material-icons bi">&#xE872;</i>Delete</a></li>
            </ul>
            <br>
            {{clas.classdescription}}
          </div>
        </li>
    </ul>
    <div class="fixed-action-btn horizontal" v-if="userLogin">
      <a class="btn-floating btn-large red tooltipped" @click="addCategoryPopup.visible = true" data-position="top" data-delay="50" data-tooltip="Add Class">
        <i class="material-icons">&#xE145;</i>
      </a>
    </div>
  </div>




  <!-- Pop Up Box -->
    <div class="popupblackout" v-if="addCategoryPopup.visible"></div>
    <transition name="slidefade" mode="out-in">
      <div class="popupbox z-depth-4" v-if="addCategoryPopup.visible">
        <div class="inner-popup">
          <header class="blue darken-3">
            <br>
              <div class="header-text">Add Class</div>
              <button @click="addCategoryPopup.visible = false"><i class="material-icons">cancel</i></button>
          </header>
          <transition name="slidefadesmall" mode="out-in">
            <div class="progress" v-if="addCategoryPopup.loading">
                <div class="indeterminate"></div>
            </div>
          </transition>
          <form v-on:submit.prevent="addCategory()">
            <div class="content">
                <input type="text" v-model="addCategoryPopup.name" placeholder="Class Name . . ">
                <select class="browser-default" v-if="classes" v-model="addCategoryPopup.category">
                    <option v-for="cat of classes.maincategories" :value="cat.catid"
                    :selected="cat.catid == $route.params.id"
                    >{{cat.catname}}</option>
                </select>
                <input type="text" v-model="addCategoryPopup.description" placeholder="Class Description . . ">
            </div>
            <br>
            <center>
              <button type="submit"  class="waves-effect waves-light btn blue darken-3">Add Class</button>
            </center>
          </form>
        </div>
      </div>
    </transition>

    <div class="popupblackout" v-if="editCategoryPopup.visible"></div>
    <transition name="slidefade" mode="out-in">
      <div class="popupbox z-depth-4" v-if="editCategoryPopup.visible">
        <div class="inner-popup">
          <header class="blue darken-3">
            <br>
              <div class="header-text">Edit Class</div>
              <button @click="editCategoryPopup.visible = false"><i class="material-icons">cancel</i></button>
          </header>
          <transition name="slidefadesmall" mode="out-in">
            <div class="progress" v-if="editCategoryPopup.loading">
                <div class="indeterminate"></div>
            </div>
          </transition>
          <form v-on:submit.prevent="editCategorySubmit()">
            <div class="content">
                <label>Class Name</label>
                <input type="text" v-model="editCategoryPopup.name" placeholder="Class Name . . ">
                <label>Category</label>
                <select class="browser-default" v-if="classes" v-model="editCategoryPopup.category">
                    <option v-for="cat of classes.maincategories" :value="cat.catid"
                    :selected="cat.catid == $route.params.id"
                    >{{cat.catname}}</option>
                </select>
                <label>Class Description</label>
                <input type="text" v-model="editCategoryPopup.description" placeholder="Class Description . . ">
            </div>
            <br>
            <center>
              <button type="submit"  class="waves-effect waves-light btn blue darken-3">Edit Class</button>
            </center>
          </form>
        </div>
      </div>
    </transition>
  <!-- /Popup box -->
</div>
