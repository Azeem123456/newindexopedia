<div class="section">
  <div class="container" align='center'>
      <div  class="login-box" style="height:auto;margin-top:-5px;">
        <transition name="slidefadesmall" mode="out-in">
          <div class="progress" v-if="loading">
              <div class="indeterminate"></div>
          </div>
        </transition>
        <div  class="login-logo">
          Indexopedia
        </div>
        <div  class="login-txt">
          Sign Up Below
        </div>
        <br >
        <form v-on:submit.prevent="login()">
            <div classs="input-feild">
                <input type="text" placeholder="First Name" v-model="fname">
            </div>
            <div classs="input-feild">
                <input type="text" placeholder="Last Name" v-model="lname">
            </div>
            <div classs="input-feild">
                <input type="text" placeholder="Email" v-model="email">
            </div>
            <div classs="input-feild">
                <input type="password" placeholder="Password" v-model="pass">
            </div>
            <div classs="input-feild">
                <input type="password" placeholder="Repeat Password" v-model="rpass">
            </div>
            <div  class="row">
              <div  class="col s12 m12">
                <button  mz-button="" style="width:100%;" type="submit" class="btn btn-large waves-effect waves-light">
                  <i  class="material-icons" style="float:left;"></i>
                  Sign Up
                </button>
              </div>
            </div>
        </form>
        <div align="center" style="margin-top:10px">
            <router-link to="/login"><b>back to login</b></router-link>
        </div>
      </div>
  </div>
</div>
