<div>
  <div class="container">
    <transition name="fade" mode="out-in">
      <div class="greyout" v-if="!categories"></div>
    </transition>
    <transition name="slidefade" mode="out-in">
      <div class="loader" v-if="!categories">
        <div class="preloader-wrapper big active">
          <div class="spinner-layer spinner-blue-only">
            <div class="circle-clipper left">
              <div class="circle"></div>
            </div><div class="gap-patch">
              <div class="circle"></div>
            </div><div class="circle-clipper right">
              <div class="circle"></div>
            </div>
          </div>
        </div>
      </div>
    </transition>
    <br>
    <nav class="blue darken-3 breadcrumbhead">
      <div class="nav-wrapper p10-left breadcrumbhead">
        <div class="col s12">
          <a class="breadcrumb" @click="$router.push('maincategories')">CATEGORIES</a>
        </div>
      </div>
    </nav>
    <br>
    <ul class="collection main-cat" v-if="categories">
        <li class="collection-item" v-for="category of categories">
          <div>
          <a class="txt-capitalize" @click="$router.push('/class/'+category.category.catid)"><b>{{category.category.catname}}</b> ({{category.category.classcount}})</a>
          <a v-if="userLogin && userLogin.level=='admin'" class="dropdown-button secondary-content" :data-activates='"sub-cat-dropdown"+category.category.catid'><i class="material-icons">&#xE5D3;</i></a>
          <br>
          {{category.category.catdescription}}
          <ul :id='"sub-cat-dropdown"+category.category.catid' class='dropdown-content'>
            <li><a @click="editCategory(category.category)"><i class="material-icons bi">&#xE254;</i>Edit</a></li>
            <li v-if="category.category.classcount == 0"><a @click="deleteCategory(category.category)" ><i class="material-icons bi">&#xE872;</i>Delete</a></li>
          </ul>
        </div>
        </li>
    </ul>
    <div class="fixed-action-btn horizontal" v-if="userLogin && userLogin.level=='admin'">
      <a class="btn-floating btn-large red tooltipped" @click="addCategoryPopup.visible = true" data-position="top" data-delay="50" data-tooltip="Add Category">
        <i class="material-icons">&#xE145;</i>
      </a>
    </div>
  </div>



  <!-- Pop Up Box -->
  <div class="popupblackout" v-if="addCategoryPopup.visible"></div>
  <transition name="slidefade" mode="out-in">
    <div class="popupbox z-depth-4" v-if="addCategoryPopup.visible">
      <div class="inner-popup">
        <header class="blue darken-3">
          <br>
            <div class="header-text">Add Category</div>
            <button @click="addCategoryPopup.visible = false"><i class="material-icons">cancel</i></button>
        </header>
        <transition name="slidefadesmall" mode="out-in">
          <div class="progress" v-if="addCategoryPopup.loading">
              <div class="indeterminate"></div>
          </div>
        </transition>
        <form v-on:submit.prevent="addCategory()">
          <div class="content">
              <input type="text" v-model="addCategoryPopup.name" placeholder="Category Name . . ">
              <input type="text" v-model="addCategoryPopup.description" placeholder="Category Description . . ">
          </div>
          <br>
          <center>
            <button type="submit"  class="waves-effect waves-light btn blue darken-3">Add Category</button>
          </center>
        </form>
      </div>
    </div>
  </transition>

  <div class="popupblackout" v-if="editCategoryPopup.visible"></div>
  <transition name="slidefade" mode="out-in">
    <div class="popupbox z-depth-4" v-if="editCategoryPopup.visible">
      <div class="inner-popup">
        <header class="blue darken-3">
          <br>
            <div class="header-text">Edit Category</div>
            <button @click="editCategoryPopup.visible = false"><i class="material-icons">cancel</i></button>
        </header>
        <transition name="slidefadesmall" mode="out-in">
          <div class="progress" v-if="editCategoryPopup.loading">
              <div class="indeterminate"></div>
          </div>
        </transition>
        <form v-on:submit.prevent="editCategorySubmit()">
          <div class="content">
              <label>Category Name</label>
              <input type="text" v-model="editCategoryPopup.name" placeholder="Category Name . . ">
              <label>Category Description</label>
              <input type="text" v-model="editCategoryPopup.description" placeholder="Category Description . . ">
          </div>
          <br>
          <center>
            <button type="submit"  class="waves-effect waves-light btn blue darken-3">Edit Category</button>
          </center>
        </form>
      </div>
    </div>
  </transition>
<!-- /Popup box -->
</div>
