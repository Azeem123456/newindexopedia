
<div>
  <div class="container">
    <br>
    <nav class="blue darken-3 breadcrumbhead">
      <div class="nav-wrapper p10-left breadcrumbhead">
        <div class="col s12">
          <a class="breadcrumb" @click="$router.push('/')">CATEGORIES</a>
          <a class="breadcrumb" @click="$router.push('/settings')">SETTINGS</a>
        </div>
      </div>
    </nav>
    <br>
    <div class="card full-width">
      <div class="card-content">
        <span class="card-title txt-capitalize">Account Settings</span>
        <form v-on:submit.prevent="changeAccountSettings()">
          <div class="row">
            <div class="col m6">
              <label>First Name </label>
              <input type="text" :placeholder="userLogin ? userLogin.fname : ''" v-model="accounts.fname">
            </div>
            <div class="col m6">
              <label>Last Name </label>
              <input type="text" :placeholder="userLogin ? userLogin.lname : ''" v-model="accounts.lname">
            </div>
            <div class="col m6">
              <label>Old Password </label>
              <input type="password" placeholder="Please Enter Your Old Password" v-model="accounts.oldpassword">
            </div>
            <div class="col m6">
              <label>New Password </label>
              <input type="password" placeholder="Please Enter Your New Password" v-model="accounts.password">
            </div>

              <div class="col m12">
                  <div class="img-relative">
                      <!-- Loading image -->
                      <div class="overlay uploadProcess" style="display: none;">
                          <div class="overlay-content"><img src="uploads/images/loading.gif"/></div>
                      </div>
                      <!-- Hidden upload form -->
                      <input id="imageupload" accept="image/*" type="file" v-on:change="fileInputChange('image')" >


                      <!-- Profile image -->
                  </div>
              </div>
            <div align="center">
              <button type="submit" class="btn blue darken-3">Submit</button>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="card full-width">
      <div class="card-content">
        <span class="card-title txt-capitalize">Assign KOL</span>
        <form v-on:submit.prevent="assignKOL()">
          <div class="row">
              <div class="col m6">
                <select class="browser-default" v-model="assign_kol.table">
                  <option value="">Select Table</option>
                  <option v-for="table in kol_settings.classes" v-if="table.idowner==userLogin.id" :value="table.idclass">{{table.classname}}</option>
                </select>
              </div>
              <div class="col m6">
                <select class="browser-default" v-model="assign_kol.kol">
                  <option value="">Select KOL</option>
                  <option v-for="kol in kol_settings.users" :value="kol.id">{{kol.fname+' '+kol.lname+' ('+kol.uname+')'}}</option>
                </select>
              </div>
            </div>
            <div align="center">
              <button type="submit" class="btn blue darken-3">Assign KOL</button>
            </div>
        </form>
      </div>
    </div>

    <div class="card full-width">
      <div class="card-content">
        <span class="card-title txt-capitalize">Manage KOL</span>
        <table class="responsive-table" v-if="kol_settings.kols && kol_settings.kols.length > 0">
          <thead>
            <tr>
              <th>Table</th>
              <th>User</th>
              <th>Access Level</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="kol in kol_settings.kols">
              <td>{{kol.classname}}</td>
              <td>{{kol.fname+' '+kol.lname+' ('+kol.uname+')'}}</td>
              <td>{{kol.access_level}}</td>
              <td><a @click="deleteKOL(kol.id)">Delete</a></td>
            </tr>
          </tbody>
        </table>
        <div align="center" v-else>You Have Not Assigned Any KOL</div>
      </div>
    </div>

    <div class="card full-width">
      <div class="card-content">
        <span class="card-title txt-capitalize">Manage Tables</span>
        <table class="responsive-table" v-if="kol_settings.classes && kol_settings.classes.length > 0">
          <thead>
            <tr>
              <th>Table</th>
              <th>Role</th>
              <th>Lock / Unlock</th>
            </tr>
          </thead>
          <tbody>
            <tr v-for="table in kol_settings.classes">
              <td>{{table.classname}}</td>
              <td>{{table.idowner == userLogin.id ? 'List Owner' : 'KOL'}}</td>
              <td>
                <a v-if="table.locked == '0'" @click="lockTable(table.idclass)">Lock</a>
                <a v-else @click="unlockTable(table.idclass)">Unlock</a>
              </td>
            </tr>
          </tbody>
        </table>
        <div align="center" v-else>You Don't Have Any Table</div>
      </div>
    </div>
  </div>

  <!-- Loader -->

  <transition name="fade" mode="out-in">
    <div class="greyout" v-if="loading"></div>
  </transition>
  <transition name="slidefade" mode="out-in">
    <div class="loader" v-if="loading">
      <div class="preloader-wrapper big active">
        <div class="spinner-layer spinner-blue-only">
          <div class="circle-clipper left">
            <div class="circle"></div>
          </div><div class="gap-patch">
            <div class="circle"></div>
          </div><div class="circle-clipper right">
            <div class="circle"></div>
          </div>
        </div>
      </div>
    </div>
  </transition>
</div>
