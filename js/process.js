function showModal(header, body){
  $('.modal').modal();
  $("#modal-header").html(header);
  $("#modal-body").html(body);
  $('.modal').modal('open');
}
function showOtherModal(header, body){
  $('.modal').modal();
  $("#modal-header").html(header);
  $("#modal-body").html(body);
  $("#modal-footer a").html("close");
  $('.modal').modal('open');
}

function hideModal(){
  $("#modal-body").html("");
  $('.modal').modal('close');
}
function isEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email.toLowerCase());
}
function isLoggedIn(){
  return localStorage.getItem("_logindetails") ? true : false;
}
$('body').on('DOMNodeInserted', 'div.popupbox', function () {
  _this = $(this)
  setTimeout(function(){
    _this.animate({height: _this.find(".inner-popup").height()+20, maxHeight: 600}, 500, function () {
      if(_this.height() == 600)
      {
        _this.css({height: "600px", overflowY: "scroll", paddingBottom: "15px"});
      }
    });
  },500);
});

//  =============================================================================

// ==== File Uploading ==





var Upload = function (file) {
  this.file = file;
};

Upload.prototype.getType = function() {
  return this.file.type;
};
Upload.prototype.getSize = function() {
  return this.file.size;
};
Upload.prototype.getName = function() {
  return this.file.name;
};
Upload.prototype.doUpload = function () {
  var that = this;
  var formData = new FormData();

  // add assoc key values, this will be posts values
  formData.append("file", this.file, this.getName());
  formData.append("upload_file", true);

  $.ajax({
    type: "POST",
    url: "./ajax/upload.php",
    xhr: function () {
      var myXhr = $.ajaxSettings.xhr();
      if (myXhr.upload) {
        myXhr.upload.addEventListener('progress', that.progressHandling, false);
      }
      return myXhr;
    },
    success: function (data) {
      // your callback here
    },
    error: function (error) {
      // handle error
    },
    async: true,
    data: formData,
    cache: false,
    contentType: false,
    processData: false,
    timeout: 60000
  });
};

Upload.prototype.progressHandling = function (event) {
  var percent = 0;
  var position = event.loaded || event.position;
  var total = event.total;
  var progress_bar_id = "#progress-wrp";
  if (event.lengthComputable) {
    percent = Math.ceil(position / total * 100);
  }
  // update progressbars classes so it fits your code
  $(progress_bar_id + " .progress-bar").css("width", +percent + "%");
  $(progress_bar_id + " .status").text(percent + "%");
};





// == // File Uploading
headerComponent = Vue.component('header-template', {
  template: "#header-template",
  data: function(){
    return {
      categories: null,
      userLogin: null,
      imageLink:null,
      searchtext: "",
      searchsuggestions: ""
    };
  },
  methods:{
    getSearchSuggestions: function(){
      this.searchsuggestions = "";
      this.$http.post("./ajax/search_suggestions.php", JSON.stringify({keyword: this.searchtext}))
          .then(response => {
            this.searchsuggestions = response.body;
          });
    },
    checkLogin: function(){
      if(isLoggedIn()){
          this.userLogin = JSON.parse(localStorage.getItem("_logindetails"));
          this.imageLink='/uploads/images/'+this.userLogin.profilepic;
          // alert(this.imageLink);
      }
      else
        this.userLogin = null;
    },
    isLoggedIn: isLoggedIn,
    logout: function(){
      localStorage.removeItem("_logindetails");
      this.$router.push("/login");
    },
    init: function(){
      setTimeout(function(){$('.tooltipped').tooltip({delay: 10});},500);
      this.$http.get("./ajax/sidebar_getall.php")
                .then(response => {
                  this.categories = response.body;
                });
      $("#side-menu").sideNav();
      $(".side-nav li").on('click', 'a[href]', function(){
        $("#side-menu").sideNav('hide');
      });

      $("#toggle-search-nav").click(function(){
        $(".search-nav").toggle();
      });
      $('.collapsible').collapsible();
    }
  },
  mounted: function(){
    this.init();
    this.checkLogin();
  }
});



// Home Component Start





HomeComponent = Vue.component('home-template', {
    template: "#home-template",
    methods:{
      init: function(){
        $(".search-nav").fadeIn();
        $(".dropdown-button").dropdown();
        setTimeout(function(){$('.tooltipped').tooltip({delay: 10});},500);
      }
    },
    mounted: function(){
      this.init();
    }
});





// MainCategory Component Start





MainCategoryComponent = Vue.component('maincategory-template', {
    template: "#maincategory-template",
    data: function(){
      return {
        categories: null,
        userLogin: null,
        addCategoryPopup: {
          visible: false,
          loading: false,
          name: '',
          description: ''
        },
        editCategoryPopup: {
          visible: false,
          loading: false,
          catid: null,
          name: '',
          description: ''
        }
      }
    },
    methods:{
      checkLogin: function(){
        if(isLoggedIn())
          this.userLogin = JSON.parse(localStorage.getItem("_logindetails"));
        else
          this.userLogin = null;
      },
      addCategory: function(){
        if(this.addCategoryPopup.name == "" || this.addCategoryPopup.description == ""){
          showModal("Error !", "Please Fill In All The Feilds");
        }else{
          this.addCategoryPopup.loading = true;
          this.$http.post("./ajax/maincategory_addcat.php", JSON.stringify(this.addCategoryPopup))
                    .then(response => {
                      this.addCategoryPopup.loading = false;
                      this.addCategoryPopup.visible = false;
                      if(response.body == "exist"){
                        showModal("We're Sorry !", "Category You Want To Add Already Exists In Our Database");
                      }else if(response.body == "success"){
                        this.init();
                      }
                    });
        }
      },
      editCategory(category){
        this.editCategoryPopup.name = category.catname;
        this.editCategoryPopup.description = category.catdescription;
        this.editCategoryPopup.catid = category.catid;
        this.editCategoryPopup.visible = true;
      },
      editCategorySubmit(){
        this.editCategoryPopup.loading = true;
        this.$http.post("./ajax/maincategory_editcat.php", JSON.stringify(this.editCategoryPopup))
                  .then(response => {
                    this.editCategoryPopup.loading = false;
                    this.editCategoryPopup.visible = false;
                    if(response.body == "success"){
                      this.init();
                    }
                  });
      },
      deleteCategory(category){
        this.$http.post("./ajax/maincategory_deletecat.php", JSON.stringify({catid: category.catid}))
                  .then(response => {
                    if(response.body == "success"){
                      this.init();
                    }
                  });
      },
      init: function(){
        setTimeout(function(){$('.tooltipped').tooltip({delay: 10});},500);
        this.$http.get("./ajax/sidebar_getall.php")
                  .then(response => {
                    this.categories = response.body;
                    setTimeout(function(){$(".dropdown-button").dropdown();},500);
                  });
      }
    },
    mounted: function(){
      this.init();
      this.checkLogin();
    }
});





// Categor Component Start





CategoryComponent = Vue.component('category-template', {
    template: "#category-template",
    data: function(){
      return {
          categories: null,
          userLogin: null,
          loading: false,
          addCategoryPopup: {
            visible: false,
            loading: false,
            parentcatid: this.$route.params.id,
            name: '',
            description: ''
          },
          editCategoryPopup: {
            visible: false,
            loading: false,
            catid: null,
            name: '',
            description: ''
          }
        }
    },
    methods:{
      checkLogin: function(){
        if(isLoggedIn())
          this.userLogin = JSON.parse(localStorage.getItem("_logindetails"));
        else
          this.userLogin = null;
      },
      addCategory: function(){
        if(this.addCategoryPopup.name == "" || this.addCategoryPopup.description == ""){
          showModal("Error !", "Please Fill In All The Feilds");
        }else{
          this.addCategoryPopup.loading = true;
          this.$http.post("./ajax/category_addcat.php", JSON.stringify(this.addCategoryPopup))
                    .then(response => {
                      this.addCategoryPopup.loading = false;
                      if(response.body == "exist"){
                        showModal("We're Sorry !", "Category You Want To Add Already Exists In Our Database");
                      }else if(response.body == "success"){
                        this.loading = true;
                        this.init();
                      }
                    });
        }
      },
      editCategory(category){
        this.editCategoryPopup.name = category.catname;
        this.editCategoryPopup.description = category.catdescription;
        this.editCategoryPopup.catid = category.catid;
        this.editCategoryPopup.visible = true;
      },
      editCategorySubmit(){
        this.editCategoryPopup.loading = true;
        this.$http.post("./ajax/maincategory_editcat.php", JSON.stringify(this.editCategoryPopup))
                  .then(response => {
                    this.editCategoryPopup.loading = false;
                    this.editCategoryPopup.visible = false;
                    if(response.body == "success"){
                      this.loading = true;
                      this.init();
                    }
                  });
      },
      deleteCategory(category){
        this.loading = true;
        this.$http.post("./ajax/maincategory_deletecat.php", JSON.stringify({catid: category.catid}))
                  .then(response => {
                    if(response.body == "success"){
                      this.init();
                    }
                  });
      },
      init: function(){
        setTimeout(function(){$('.tooltipped').tooltip({delay: 10});},500);
        this.$http.get("./ajax/category_getcats.php?id="+this.$route.params.id)
                  .then(response => {
                    this.categories = response.body;
                    if(this.categories.cats.length < 1)
                      this.$router.push("/maincategories");
                    setTimeout(function(){$(".dropdown-button").dropdown();},500);
                    this.loading = false;
                  });
      }
    },
    mounted: function(){
      this.init();
      this.checkLogin();
    }
});



// Class Component Start




ClassComponent = Vue.component('class-template', {
    template: "#class-template",
    data: function(){
      return {
        classes: null,
        loading: false,
        maincats: [],
        addCategoryPopup: {
          visible: false,
          loading: false,
          user: null,
          parentcatid: this.$route.params.id,
          name: '',
          category: this.$route.params.id,
          description: ''
        },
        editCategoryPopup: {
          visible: false,
          loading: false,
          catid: null,
          user: null,
          name: '',
          category: this.$route.params.id,
          description: ''
        }
      }
    },
    methods:{
      clearFeilds: function(){
        this.addCategoryPopup.visible = false;
        this.addCategoryPopup.loading = false;
        this.addCategoryPopup.name = '';
        this.addCategoryPopup.description = '';
      },
      checkLogin: function(){
        if(isLoggedIn())
          this.userLogin = JSON.parse(localStorage.getItem("_logindetails"));
        else
          this.userLogin = null;
      },
      lock: function(id){
        this.loading = true;
        this.$http.post("./ajax/settings_assignkol.php", JSON.stringify({type: "locktable", id: id}))
                  .then(response => {
                    this.loading = false;
                    if(response.body == "success"){}
                    else {
                      showModal("failed !", response.body);
                    }
                      this.init();
                  });
      },
      unlock: function(id){
        this.loading = true;
        this.$http.post("./ajax/settings_assignkol.php", JSON.stringify({type: "unlocktable", id: id}))
                  .then(response => {
                    this.loading = false;
                    if(response.body == "success"){
                        
                    }
                      else {
                        showModal("failed !", response.body);
                      }
                      this.init();
                  });
      },
      addCategory: function(){
        if(this.addCategoryPopup.name == "" || this.addCategoryPopup.description == ""){
          showModal("Error !", "Please Fill In All The Feilds");
        }else{
          this.addCategoryPopup.loading = true;
          this.$http.post("./ajax/classes_addclass.php", JSON.stringify(this.addCategoryPopup))
                    .then(response => {
                      this.addCategoryPopup.loading = false;
                      this.clearFeilds();
                      if(response.body == "exist"){
                        showModal("We're Sorry !", "Class You Want To Add Already Exists In Our Database");
                      }else if(response.body == "success"){
                        this.init();
                        this.loading = true;
                      }
                    });
        }
      },
      editCategory(category){
        this.editCategoryPopup.name = category.classname;
        this.editCategoryPopup.description = category.classdescription;
        this.editCategoryPopup.catid = category.idclass;
        this.editCategoryPopup.visible = true;
      },
      editCategorySubmit(){
        this.editCategoryPopup.loading = true;
        this.$http.post("./ajax/classes_editclass.php", JSON.stringify(this.editCategoryPopup))
                  .then(response => {
                    this.editCategoryPopup.loading = false;
                    this.editCategoryPopup.visible = false;
                    if(response.body == "success"){
                      this.loading = true;
                      this.init();
                    }
                  });
      },
      deleteCategory(category){
        this.loading = true;
        this.$http.post("./ajax/classes_deleteclass.php", JSON.stringify({catid: category.idclass}))
                  .then(response => {
                      this.loading = false;
                    if(response.body == "islookup"){
                        showModal("Error !", "Cannot delete class as it contains lookup columns");
                    }
                    if(response.body == "contains"){
                        showModal("Error!", "Cannot delete class as it contains data");
                    }
                    if(response.body == "success"){
                      this.init();
                    }
                  });
      },
      init: function(){
        if(this.userLogin){
          this.addCategoryPopup.user = this.userLogin.id;
          this.editCategoryPopup.user = this.userLogin.id;
        }else{
          if(isLoggedIn()){
              this.userLogin = JSON.parse(localStorage.getItem("_logindetails"));
              this.addCategoryPopup.user = this.userLogin.id;
              this.editCategoryPopup.user = this.userLogin.id;
          }
        }
        setTimeout(function(){$('.tooltipped').tooltip({delay: 10});},500);
        var senduserid = 'no';
        if(this.userLogin)
            senduserid = this.userLogin.id;
        this.$http.get("./ajax/classes_getclasses.php?id="+this.$route.params.id+"&uid="+senduserid)
                  .then(response => {
                    this.classes = response.body;
                    setTimeout(function(){$(".dropdown-button").dropdown();},500);
                      this.loading = false;
                  });
      }
    },
    mounted: function(){
      this.checkLogin();
      this.init();
    }
});








// Details Component







DetailsComponent = Vue.component('details-template', {
    template: "#details-template",
    data: function(){
      return {
        classdetails: null,
        userLogin: null,
        loading: false,
        addColumnPopup: {
          visible: false,
          loading: false,
          idclass: this.$route.params.id,
          user: null,
          name: '',
          order: '',
          type: "",
          lookuptable: null,
          lookupfield: null
        },
        editColumnPopup: {
          id: '',
          visible: false,
          loading: false,
          idclass: this.$route.params.id,
          user: null,
          name: '',
          order: '',
          type: "",
          typeeditable: false,
          lookuptable: null,
          lookupfield: null
        },
        addRowPopup: {
          visible: false,
          loading: false,
          idclass: this.$route.params.id,
          user: null,
          props: []
        },
        editRowPopup: {
          visible: false,
          loading: false,
          idclass: this.$route.params.id,
          user: null,
          idobject: null,
          props: []
        },
        audioupload: null,
        imageupload: null,
        search_query: "",
        search_column: "",
        col_delete: "",
      }
    },
    computed: {
        searched_objects: function(){
            var _this = this;
            var found_objects = [];
            if(this.classdetails && this.search_column == ""){
                found_objects = [];
                this.classdetails.table.objects.forEach(obj => {
                    var inserted = false;
                    _this.classdetails.table.classprops.forEach(classprop => {
                        if(classprop.propertytype != '7' && classprop.propertytype != '5' && classprop.propertytype != '6'){
                            if(_this.classdetails.table.propvalues.filter(propvalue => propvalue.idobject==obj.idobject && propvalue.idclassproperty==classprop.idclassproperty)[0])
                                if(_this.classdetails.table.propvalues.filter(propvalue => propvalue.idobject==obj.idobject && propvalue.idclassproperty==classprop.idclassproperty)[0].value.toLowerCase().indexOf(_this.search_query.toLowerCase()) > -1){
                                    if(!inserted){
                                        found_objects.push(obj);
                                        inserted = true;
                                    }
                                }
                        }
                    });
                });
            }else if(this.classdetails && this.search_column != ""){
                found_objects = [];
                this.classdetails.table.objects.forEach(obj => {
                    var inserted = false;
                    _this.classdetails.table.classprops.forEach(classprop => {
                        if(classprop.idclassproperty == _this.search_column){
                            if(_this.classdetails.table.propvalues.filter(propvalue => propvalue.idobject==obj.idobject && propvalue.idclassproperty==classprop.idclassproperty)[0])
                                if(_this.classdetails.table.propvalues.filter(propvalue => propvalue.idobject==obj.idobject && propvalue.idclassproperty==classprop.idclassproperty)[0].value.toLowerCase().indexOf(_this.search_query.toLowerCase()) > -1){
                                    if(!inserted){
                                        found_objects.push(obj);
                                        inserted = true;
                                    }
                                }
                        }
                    });
                });
            }
            return found_objects;
        }
    },
    watch: {
      'addColumnPopup.lookuptable': function () {
          if(this.addColumnPopup.lookuptable){
              this.classdetails.lookupfields = this.classdetails.lookupprops.filter(prop => prop.idclass == this.addColumnPopup.lookuptable);
          }
      },
      'addColumnPopup.visible': function(){
          this.addColumnPopup.order = parseInt(this.classdetails.table.classprops[this.classdetails.table.classprops.length - 1].propertyorder) + 1;
      },
      'editColumnPopup.lookuptable': function () {
          if(this.editColumnPopup.lookuptable){
              this.classdetails.lookupfields = this.classdetails.lookupprops.filter(prop => prop.idclass == this.editColumnPopup.lookuptable);
          }
      }
    },
    methods:{
        // File Upload
        editColumn: function(){
            if(this.col_delete == ""){
                showModal("Error !", "Please Select A Column To Edit");
            }else{
                var editclm = this.classdetails.table.classprops.filter(prop => prop.idclassproperty == this.col_delete)[0];
                
                this.editColumnPopup.id = editclm.idclassproperty;
                this.editColumnPopup.name = editclm.propertyname;
                this.editColumnPopup.type = editclm.propertytype;
                this.editColumnPopup.order = editclm.propertyorder;
                this.editColumnPopup.lookuptable = editclm.propertylookupclass;
                var _this = this;
                setTimeout(function(){
                    _this.editColumnPopup.lookupfield = editclm.propertylookupfield;
                },100);
                this.editColumnPopup.typeeditable = this.classdetails.table.propvalues.filter(propvalue => propvalue.idclassproperty==this.editColumnPopup.id)[0] ? false : true;
                this.editColumnPopup.visible = true;
            }
        },
        submitEditColumn: function(){
            this.loading = true;
            this.editColumnPopup.user = this.userLogin.id;
            this.$http.post("./ajax/details_edit_column.php", JSON.stringify(this.editColumnPopup))
                      .then(res => {
                        this.init();
                        this.loading = false;
                        this.editColumnPopup.visible = false;
                        window.location.href='/details/'+this.userLogin.id;
                      });
        },
        deleteColumn: function(){
            this.loading = true;
            if(this.col_delete == ""){
                showModal("Error !", "Please Select A Column To Delete");
                this.loading = false;
            }else if(this.classdetails.table.classprops.filter(prop => prop.idclassproperty == this.col_delete)[0].type == 8){
                showModal("Error !", "This column can't be deleted, it's a lookup field");
                this.loading = false;
            }else{
                this.$http.post("./ajax/details_delete_column.php", JSON.stringify({id: this.col_delete, user: this.userLogin.id}))
                          .then(response => {
                              
                            this.loading = false;
                              if(response.body == "islookup"){
                                showModal("error !", "This record is referenced by other table and cannot be deleted");
                            }
                            
                            if(response.body == "hasdata"){
                                showModal("Error!", "Cannot delete column as it contains data");
                            }
                              if(response.body == "success"){
                                  this.init();
                                  this.col_delete = "";
                              }
                          });
            }
        },
        sortTable: function(n) {
          var table, rows, switching, i, x, y, shouldSwitch, dir, switchcount = 0;
          table = document.getElementById("myTable");
          switching = true;
          dir = "asc"; 
          while (switching) {
            switching = false;
            rows = table.getElementsByTagName("TR");
            for (i = 1; i < (rows.length - 1); i++) {
              shouldSwitch = false;
              x = rows[i].getElementsByTagName("TD")[n];
              y = rows[i + 1].getElementsByTagName("TD")[n];
              if (dir == "asc") {
                if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
                  shouldSwitch= true;
                  break;
                }
              } else if (dir == "desc") {
                if (x.innerHTML.toLowerCase() < y.innerHTML.toLowerCase()) {
                  shouldSwitch= true;
                  break;
                }
              }
            }
            if (shouldSwitch) {
              rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
              switching = true;
              switchcount ++; 
            } else {
              if (switchcount == 0 && dir == "asc") {
                dir = "desc";
                switching = true;
              }
            }
          }
        },
        clearColumnFeilds: function(){
          this.addColumnPopup.visible = false;
          this.addColumnPopup.loading = false;
          this.addColumnPopup.name = "";
          this.addColumnPopup.type = "";
          this.addColumnPopup.lookuptable = null;
          this.addColumnPopup.lookupfield = null;
        },
        fileInputChange: function(type) {
          if(type == "audio")
            this.audioupload = document.getElementById("audioupload").files[0];
          else
            this.imageupload = document.getElementById("imageupload").files[0];
        },
        // -- / file upload
        showImagePopup: function(url){
          var imgtag = '<img src="'+url+'" style="width:100%" >';
          showOtherModal('Image', imgtag);
        },
        showVideoPopup: function(url){
          var videotag = '<object data="'+url+'" style="width:100%;height:350px;"></object>';
          showOtherModal('Video', videotag);
        },
        editRow: function(idobject){
          this.editRowPopup.idobject = idobject;
          this.editRowPopup.props = this.addRowPopup.props;
          this.editRowPopup.props.forEach(prop => {
            prop.value = this.classdetails.table.propvalues.filter(propvalue => propvalue.idobject==idobject && propvalue.idclassproperty==prop.idclassproperty)[0] ?
              this.classdetails.table.propvalues.filter(propvalue => propvalue.idobject==idobject && propvalue.idclassproperty==prop.idclassproperty)[0].value : '';
          });
          this.editRowPopup.visible = true;
        },
        checkLogin: function(){
            if(isLoggedIn())
                this.userLogin = JSON.parse(localStorage.getItem("_logindetails"));
            else
                this.userLogin = null;
        },
        lockobj: function(id){
            this.loading = true;
            this.$http.post("./ajax/details_lock_unlock_obj.php", JSON.stringify({idobject: id, type: "lock"}))
                .then(response => {
                    if(response.body == "success"){
                        this.loading = false;
                        this.init();
                    }
                });
        },
        lockTable: function(id){
            this.loading = true;
            this.$http.post("./ajax/settings_assignkol.php", JSON.stringify({type: "locktable", id: id}))
                .then(response => {
                    this.loading = false;
                    if(response.body == "success"){}
                    else {
                        showModal("failed !", response.body);
                    }
                    this.init();
                });
        },
        unlockTable: function(id){
            this.loading = true;
            this.$http.post("./ajax/settings_assignkol.php", JSON.stringify({type: "unlocktable", id: id}))
                .then(response => {
                    this.loading = false;
                    if(response.body == "success"){

                    }
                    else {
                        showModal("failed !", response.body);
                    }
                    this.init();
                });
        },
        unlockobj: function (id) {
            this.loading = true;
            this.$http.post("./ajax/details_lock_unlock_obj.php", JSON.stringify({idobject: id, type: "unlock"}))
                    .then(response => {
                    if(response.body == "success"){
                    this.loading = false;
                    this.init();
                }
            });
        },
        follow: function (idclass, iduser) {
                this.loading = true;
                this.$http.post("./ajax/details_lock_unlock_obj.php", JSON.stringify({idclass: idclass, iduser: iduser, type: "follow"}))
                    .then(response => {
                    if(response.body == "success"){
                    this.loading = false;
                    this.init();
                }
            });
        },
        unfollow: function(idclass, iduser){
                this.loading = true;
                this.$http.post("./ajax/details_lock_unlock_obj.php", JSON.stringify({idclass: idclass, iduser: iduser, type: "unfollow"}))
                    .then(response => {
                    if(response.body == "success"){
                    this.loading = false;
                    this.init();
                }
            });
        },
        coltype: function () {
            $("div.popupbox").animate({height: $("div.popupbox").find(".inner-popup").height()+20}, 500);
        },
        addRow: function(){
          this.addRowPopup.loading = true;
        
          _this = this;
          var formData = new FormData();
          if(this.imageupload){
            formData = null;
            formData = new FormData();
            formData.append('file', this.imageupload);
            $.ajax({
                    url : "./ajax/upload.php",
                    type : 'POST',
                    data : formData,
                    processData: false,
                    contentType: false,
                    async:false,
                    success: function(data) {
                    _this.addRowPopup.props.filter(prop => prop.propertytype == '5')[0].value = data;
                    }
            });
          }
          if(this.audioupload){
            formData = null;
            formData = new FormData();
            formData.append('file', this.audioupload);
            $.ajax({
              url : "./ajax/upload.php",
              type : 'POST',
              data : formData,
              processData: false,
              contentType: false,
              async: false,
              success: function(data) {
                _this.addRowPopup.props.filter(prop => prop.propertytype == '6')[0].value = data;
              }
            });
          }
        
          this.addRowPopup.userfullname = this.userLogin.fname+" "+this.userLogin.lname;
          this.addRowPopup.classname = this.classdetails.classdetails.classname;
          this.addRowPopup.user = this.userLogin.id;
          this.$http.post("./ajax/details_addrow.php", JSON.stringify({type: "add", row: this.addRowPopup}))
                    .then(response => {
                      this.addRowPopup.visible = false;
                      this.addRowPopup.loading = false;
                      if(response.body == "exist"){
                        showModal("We're Sorry !", "A Similar Row Already Exists In Our Database");
                      }else if(response.body == "success"){
                        this.init();
                      }
                    });
        },
        submitEditRow: function(){
          this.editRowPopup.loading = true;
          this.editRowPopup.user = this.userLogin.id;
          _this = this;
          
          var formData = new FormData();
          if(this.imageupload){
            formData = null;
            formData = new FormData();
            formData.append('file', this.imageupload);
            $.ajax({
              url : "./ajax/upload.php",
              type : 'POST',
              data : formData,
              processData: false,
              contentType: false,
              async:false,
              success: function(data) {
                _this.editRowPopup.props.filter(prop => prop.propertytype == '5')[0].value = data;
              }
            });
          }
          if(this.audioupload){
            formData = null;
            formData = new FormData();
            formData.append('file', this.audioupload);
            $.ajax({
              url : "./ajax/upload.php",
              type : 'POST',
              data : formData,
              processData: false,
              contentType: false,
              async: false,
              success: function(data) {
                _this.editRowPopup.props.filter(prop => prop.propertytype == '6')[0].value = data;
              }
            });
          }

          this.editRowPopup.userfullname = this.userLogin.fname+" "+this.userLogin.lname;
          this.editRowPopup.classname = this.classdetails.classdetails.classname;
          this.$http.post("./ajax/details_addrow.php", JSON.stringify({type: 'edit', row: this.editRowPopup}))
                    .then(response => {
                        this.editRowPopup.loading = false;
                        if(response.body == "success"){
                            this.editRowPopup.visible = false;
                          this.init();
                        }
                      });
        },
        deleteRow: function(idobject){
          var ufname = this.userLogin.fname+" "+this.userLogin.lname;
          var dcname = this.classdetails.classdetails.classname;
          this.$http.post("./ajax/details_addrow.php", JSON.stringify({type: 'delete', row: {idobject: idobject, idclass: this.$route.params.id, userfullname: ufname, classname: dcname}}))
              .then(response => {
                  this.addRowPopup.loading = false;
                  if(response.body == "islookup"){
                        showModal("error !", "This record is referenced by other table and cannot be deleted");
                    }
                  if(response.body == "success"){
                    this.init();
                  }
              });
        },
        addColumn: function(){
          this.addColumnPopup.loading = true;
          this.addColumnPopup.user = this.userLogin.id;
          if(this.addColumnPopup.order == ''){
              this.addColumnPopup.order = 1;
          }
          
          if(this.addColumnPopup.type == ""){
            showModal("Error !", "Please Fill Column Type Field");  
          }
          else if(this.addColumnPopup.name == ""){
            showModal("Error !", "Please Fill In The Required Fields");
          }else if(this.classdetails.table.classprops.filter(prop => prop.propertyname == this.addColumnPopup.name)[0]){
           showModal("Error !", "Column name should be unique");
          }else{
            this.$http.post("./ajax/details_addcolumn.php", JSON.stringify(this.addColumnPopup))
                      .then(response => {
                        this.clearColumnFeilds();
                        if(response.body == "exist"){
                          showModal("column already exist for the selected class");
                        }else if(response.body == "success"){
                          this.addColumnPopup.visible = false;
                          this.init();
                        }
                      });

          }
        },
        init: function(){
          setTimeout(function(){$('.tooltipped').tooltip({delay: 10});},500);
          this.$http.get("./ajax/details_getdetails.php?id="+this.$route.params.id)
                    .then(response => {
                      this.classdetails = response.body;
                      this.addRowPopup.props = response.body.table.classprops;
                      this.addRowPopup.props.forEach(prop => {
                        prop.value = "";
                      });
                      if(!this.classdetails)
                        this.$router.push("/maincategories");
                      setTimeout(function(){
                          $(".dropdown-button").dropdown();
                           $(".dropdown-trigger").dropdown();
                          if($(".mediPlayer").length > 0)
                          $('.mediPlayer').mediaPlayer();
                      },1000);
                    });
          $(".dropdown-button").dropdown();
        }
    },
    mounted: function(){
        this.checkLogin();
        this.init();
    }
});



// Login Component Start




LoginComponent = Vue.component('login-template', {
    template: "#login-template",
    data: function(){
      return {
        email: '',
        password: '',
        loading: false
      }
    },
    methods:{
      init: function(){
        setTimeout(function(){$('.tooltipped').tooltip({delay: 10});},500);
        $(".dropdown-button").dropdown();
        $("#sidenav-overlay").fadeOut(500);
        $(".drag-target").css("width", "0px");
      },
      login: function(){

        this.loading = true;
        if(this.email == "" || this.password == ""){
          showModal("Error !", "Please Fill In The Form Correctly !");
          this.loading = false;
        }else if(!isEmail(this.email)){
          showModal("Error !", "The Email You Entered Is Not Valid !");
          this.loading = false;
        }else{
          this.$http.post("./ajax/login_userlogin.php", JSON.stringify({email: this.email, password: this.password}))
                    .then(response => {
                      this.loading = false;
                      if(response.body == "notfound"){
                        showModal("Can't Login !", "No User Name Exists With Email And Password You Provided");
                      }else if(response.body == "notactivated"){
                        showModal("Can't Login !", "Your Account Is Not Activated Yet! Please Activate Your Account");
                      }else{
                        localStorage.setItem("_logindetails", JSON.stringify(response.body));
                        this.$router.push("/");
                      }
                    });
        }
      }
    },
    mounted: function(){
        if(isLoggedIn()){
            this.$router.push("/");
        }
      this.init();
    }
});



SignUpComponent = Vue.component('signup-template', {
    template: "#signup-template",
    data: function(){
      return {
        fname: '',
        lname: '',
        email: '',
        pass: '',
        rpass: '',
        loading: false
      }
    },
    methods:{
      login: function(){
        this.loading = true;
        if(this.fname == "" || this.lname == "" || this.email == "" || this.pass == "" || this.rpass == ""){
          showModal("Error !", "Please Fill In The Form Correctly !");
          this.loading = false;
        }else if(!isEmail(this.email)){
          showModal("Error !", "The Email You Entered Is Not Valid !");
          this.loading = false;
        }else if(this.pass != this.rpass){
          showModal("Error !", "Passwords you entered do not match");   
        }else{
          this.$http.post("./ajax/signup.php", JSON.stringify({fname: this.fname, lname: this.lname, email: this.email, pass: this.pass}))
                    .then(response => {
                      this.loading = false;
                      if(response.body == "exist"){
                          showModal("Error !", "The Email You Entered Is Already Associated With An Account");  
                      }else{
                          showModal("Error !", "Your Account Has Been Created Please Login !");
                        this.$router.push("/login");
                      }
                    });
        }
      }
    },
    mounted: function(){
        if(isLoggedIn()){
            this.$router.push("/");
        }
    }
});


// Settings Component Start




SettingsComponent = Vue.component('settings-template', {
    template: "#settings-template",
    data: function(){
      return {
        loading: false,
        userLogin: null,
        imageupload:null,
        imageLink:null,
        accounts: {
          type: "account",
          uid: null,
          fname: '',
          lname: '',
          oldpassword: '',
          password: '',
          profilepic:'',
        },
        assign_kol: {
          type: "assign",
          table: "",
          kol: "",
          access_level: "all",
          by: null
        },
        kol_settings: {
          classes: null,
          users: null,
          kols: null
        }
      }
    },
    methods:{
      checkLogin: function(){
        if(isLoggedIn()){
            this.userLogin = JSON.parse(localStorage.getItem("_logindetails"));
            this.imageLink="/uploads/images/"+this.userLogin.profilepic;
        }

        else
          this.userLogin = null;
      },
      lockTable: function(id){
        this.loading = true;
        this.$http.post("./ajax/settings_assignkol.php", JSON.stringify({type: "locktable", id: id}))
                  .then(response => {
                    this.loading = false;
                    if(response.body == "success"){}
                    else {
                      showModal("failed !", response.body);
                    }
                      this.init();
                  });
      },
      unlockTable: function(id){
        this.loading = true;
        this.$http.post("./ajax/settings_assignkol.php", JSON.stringify({type: "unlocktable", id: id}))
                  .then(response => {
                    this.loading = false;
                    if(response.body == "success"){
                        
                    }
                      else {
                        showModal("failed !", response.body);
                      }
                      this.init();
                  });
      },
      fileInputChange: function(type) {
        if(type == "image")
            this.imageupload = document.getElementById("imageupload").files[0];
      },
      changeAccountSettings: function(){
        if((this.accounts.fname == "" && this.accounts.lname == "" && this.accounts.profilepic == "")   &&
          (this.accounts.oldpassword == "" || this.accounts.password == "" )){
          showModal("Error !", "Nothing To Change");
        }else{


            _this = this;
            var formData = new FormData();
            if(this.imageupload){
                formData = null;
                formData = new FormData();
                formData.append('file', this.imageupload);
                formData.append('id', this.accounts.uid);
                $.ajax({
                    url : "./ajax/upload_image.php",
                    type : 'POST',
                    data : formData,
                    processData: false,
                    contentType: false,
                    async:false,
                    success: function(data) {
                        showModal("Success !", "Image has been successfully updated");
                    }
                });
            }

          this.loading = true;
          this.$http.post("./ajax/settings_assignkol.php", JSON.stringify(this.accounts))
                    .then(response => {
                      this.loading = false;
                      if(response.body == "success")
                        showModal("Success !", "Your Account Has Successfully Been Updated Logout And Login Again For Change");
                      else if(response.body == "wrongpass")
                        showModal("Error !", "Old password You Entered is Incorrect");
                        this.init();
                    });
        }
      },
      deleteKOL: function(id){
        this.loading = true;
        this.$http.post("./ajax/settings_assignkol.php", JSON.stringify({type: "delete", id: id}))
                  .then(response => {
                    this.loading = false;
                    if(response.body == "success"){
                      this.init();
                    }
                  });
      },
      assignKOL: function(){
        this.loading = true;
        this.assign_kol.classname = this.kol_settings.classes.filter(clas => clas.idclass == this.assign_kol.table)[0]?
                                         this.kol_settings.classes.filter(clas => clas.idclass == this.assign_kol.table)[0].classname : '';
        this.assign_kol.userfullname = this.userLogin.fname+" "+this.userLogin.lname;
        this.$http.post("./ajax/settings_assignkol.php", JSON.stringify(this.assign_kol))
                  .then(response => {
                    this.loading = false;
                    if(response.body == "exist"){
                      showModal("We're Sorry !", "The Kol You Tried To Assign Is Already Assigned");
                    }else if(response.body == "success"){
                      this.init();
                    }
                  });
      },
      init: function(){
        this.loading = true;
        if(this.userLogin){
          this.assign_kol.by = this.userLogin.id;
          this.accounts.uid = this.userLogin.id;
        }else{
          if(isLoggedIn()){
            this.userLogin = JSON.parse(localStorage.getItem("_logindetails"));
            this.assign_kol.by = this.userLogin.id;
          }
        }
        setTimeout(function(){$('.tooltipped').tooltip({delay: 10});},500);
        $(".dropdown-button").dropdown();
        this.$http.get("./ajax/settings_getkoldetails.php?uid="+this.userLogin.id)
                  .then(response => {
                    this.loading = false;
                    this.kol_settings = response.body;
                  });
      }
    },
    mounted: function(){
      if(!isLoggedIn)
          this.$router.push("/");
      this.checkLogin();
      this.init();
    }
});

NotificationComponent = Vue.component('notification-template', {
    template: "#notification-template",
    data: function(){
      return {
        loading: false,
        userLogin: null,
        audit: null,
        page: 'classes',
        selected_class: '',
        selected_classprop: '',
        selected_classpropvalue: null,
        show_propvaluesaudit: false,
      }
    },
    computed: {
        classesaudit: function(){
            if(this.audit == null)
                return [];
            else
                return this.audit.classes_audit.filter(clas => clas.idclass == this.selected_class);
        },
        propertyaudit: function(){
            if(this.audit == null)
                return [];
            else
                return this.audit.classproperties_audit.filter(prop => prop.idclassproperty == this.selected_classprop);
        },
        propertyvaluesaudit: function(){
            if(this.audit == null)
                return [];
            else if(this.selected_classpropvalue == null)
                return [];
            else
                return this.audit.propertyvaluesaudit.filter(
                    pva => pva.idobject == this.selected_classpropvalue.idobject && pva.idclass == this.selected_classpropvalue.idclass && pva.idclassproperty == this.selected_classpropvalue.idclassproperty
                    );
        }
    },
    methods:{
      checkLogin: function(){
        if(isLoggedIn())
          this.userLogin = JSON.parse(localStorage.getItem("_logindetails"));
        else
          this.userLogin = null;
      },
      init: function(){
        this.loading = true;
        if(!this.userLogin){
          if(isLoggedIn())
            this.userLogin = JSON.parse(localStorage.getItem("_logindetails"));
        }
        this.$http.get("./ajax/notifications.php?uid="+this.userLogin.id)
                  .then(response => {
                     this.audit = response.body;
                    this.loading = false;
                  });
      }
    },
    mounted: function(){
      if(!isLoggedIn)
          this.$router.push("/");
      this.checkLogin();
      this.init();
    }
});

AboutUs = Vue.component('aboutus-template', {
    template: "#aboutus-template",
    data: function(){
      return {
      }
    },
    methods:{
    },
});

TermsAndConditions = Vue.component('termsandconds-template', {
    template: "#termsandconds-template",
    data: function(){
      return {
      }
    },
    methods:{
    },
});

PrivacyPolicy = Vue.component('privacypolicy-template', {
    template: "#privacypolicy-template",
    data: function(){
      return {
      }
    },
    methods:{
    },
});
var router = new VueRouter(
    {
        mode: 'history',
        routes: [
            { path: '/login', component: LoginComponent},
            { path: '/signup', component: SignUpComponent},
            { path: '', component: headerComponent,
              children: [
                { path: '', component: MainCategoryComponent},
                { path: '/maincategories', component: MainCategoryComponent},
                { path: '/category/:id', component: CategoryComponent },
                { path: '/class/:id', component: ClassComponent},
                { path: '/details/:id', component: DetailsComponent},
                { path: '/settings', component: SettingsComponent},
                { path: '/audittrial', component: NotificationComponent},
                { path: '/about-us', component: AboutUs},
                { path: '/terms-and-conditions', component: TermsAndConditions},
                { path: '/privacy-policy', component: PrivacyPolicy}
              ]
            }
        ]
    }
)

var app = new Vue({
    el: '#indexopedia-main',
    router: router,
    data: {
    },
    methods:{
      init: function(){
        setTimeout(function(){$('.tooltipped').tooltip({delay: 10});},500);
        $(".dropdown-button").dropdown();
      }
    },
    mounted: function(){
    
      this.init();
    }
})
