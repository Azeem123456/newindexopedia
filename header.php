<div>
    <header>
        <nav>
            <div class="nav-wrapper white">
                <ul class="left">
                    <li><a id="side-menu" data-activates="slide-out"><i class="material-icons">&#xE5D2;</i></a></li>
                </ul>
                <a href="" class="brand-logo">
                    <img src="./img/Indexopedialogo.png" style="height:50px;margin-top:8px">
                </a>

                <ul class="center ">
                    <li style=" padding-left: 565px;">
                        <input :autofocus="'autofocus'" id="search" type="search" v-model="searchtext"
                               autocomplete="off" v-on:keyup="getSearchSuggestions()" required placeholder="Search.."
                               style="      width: 114%;
                                box-sizing: border-box;
                                color:#000;
                                border: 2px solid #ccc;
                                border-radius: 20px;
                                font-size: 16px;
                                background-color: white;
                                background-image: url('uploads/images/searchicon.png');
                                background-position: 10px 10px;
                                background-repeat: no-repeat;
                                padding: 12px 20px 12px 40px;
                                -webkit-transition: width 0.4s ease-in-out;
                                transition: width 0.4s ease-in-out;
                            }">
                    </li>
                    <li> <label class="label-icon" for="search" style="margin-top:-10px;"><i class="material-icons">&#xE8B6;</i></label></li>


                    <li v-if="userLogin" style="padding-left: 395px;padding-right:7px;cursor: grab"
                        @click="$router.push('/settings')"><font style="font-size:20px; color:#94ba33;">Welcome
                            {{userLogin.fname}}</font></li>
                    <li v-if="userLogin"><a @click="logout()">Logout</a></li>
                    <li v-if="!userLogin" style="    padding-left: 545px;">
                        <router-link to="/login">Login</router-link>
                    </li>
                    <!--
                                  </ul>




                                <ul class="right">

                                      <li><a id="toggle-search-nav" onClick="setTimeout(function(){$('#search').focus()},100);"><i class="material-icons">&#xE8B6;</i></a></li>-->
                </ul>
            </div>
        </nav>
<!--        <nav class="search-nav white">-->
<!--            <div class="nav-wrapper white">-->
<!--                <form>-->
<!--                    <div class="input-field">-->
<!--                        <input :autofocus="'autofocus'" id="search" type="search" v-model="searchtext"-->
<!--                               autocomplete="off" v-on:keyup="getSearchSuggestions()" required>-->
<!--                        <label class="label-icon" for="search" style="margin-top:-10px;"><i class="material-icons">&#xE8B6;</i></label>-->
<!--                        <i class="material-icons">close</i>-->
<!--                    </div>-->
<!--                </form>-->
<!--            </div>-->
<!--        </nav>-->
        <transition name="slidefadesmall" mode="out-in">
            <div class="z-depth-3"
                 style="padding:15px;padding-top:0px;width:100%;position:absolute;z-index:1;background:white;max-height:400px;overflow-y:scroll;"
                 v-if="searchsuggestions != '' && searchtext != ''">
                <ul style="list-style-type:none;" v-html="searchsuggestions">

                </ul>
            </div>
        </transition>
        <ul id="slide-out" class="side-nav">
            <li>
                <div class="user-view">
                    <div class="background">
                    </div>

                    <a><img class="circle" @click="$router.push('/settings')" :src="imageLink"></a>
                    <a><span class="black-text name" v-if="!isLoggedIn()">Guest</span></a>
                    <a><span class="black-text name"
                             v-if="userLogin">{{userLogin.fname+' '+userLogin.lname }}</span></a>
                    <a><span class="black-text email" v-if="userLogin">{{userLogin.email}}</span></a>
                </div>
            </li>
            <li v-if="isLoggedIn()">
                <router-link to="/audittrial"><i class="material-icons">&#xE7F4;</i> Audit Trail</router-link>
            </li>
            <li v-if="isLoggedIn()">
                <router-link to="/settings"><i class="material-icons">&#xE7FD;</i>Settings</router-link>
            </li>
            <li v-if="isLoggedIn()"><a @click="logout()"><i class="material-icons">&#xE879;</i>Logout</a></li>
            <li v-if="!isLoggedIn()">
                <router-link to="/login"><i class="material-icons">&#xE7FD;</i>Login</router-link>
            </li>
            <li>
                <div class="divider"></div>
            </li>
            <li>
                <router-link to="/about-us">About Us</router-link>
            </li>
            <li>
                <router-link to="/privacy-policy">Privacy Policy</router-link>
            </li>
            <li>
                <router-link to="/terms-and-conditions">Terms & Conditions</router-link>
            </li>
        </ul>
    </header>
    <transition name="fade" mode="out-in">
        <router-view :key="$route.fullPath"></router-view>
    </transition>
</div>
