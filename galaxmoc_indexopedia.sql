-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 12, 2019 at 10:35 AM
-- Server version: 10.2.22-MariaDB-cll-lve
-- PHP Version: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `galaxmoc_indexopedia`
--

-- --------------------------------------------------------

--
-- Table structure for table `classes`
--

CREATE TABLE `classes` (
  `idclass` int(11) NOT NULL,
  `classname` varchar(45) NOT NULL,
  `idowner` int(11) DEFAULT NULL,
  `classdescription` text DEFAULT NULL,
  `CatId` int(11) NOT NULL,
  `Status` varchar(20) NOT NULL,
  `locked` enum('0','1') NOT NULL,
  `change_by` int(11) NOT NULL,
  `change_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classes`
--

INSERT INTO `classes` (`idclass`, `classname`, `idowner`, `classdescription`, `CatId`, `Status`, `locked`, `change_by`, `change_date`) VALUES
(1, 'Shahid testing Class', 8, 'Shahid Testing Class description', 1, '0', '0', 8, '2018-07-31 12:48:50'),
(2, 'FK Table Test 1', 11, 'test', 2, '1', '0', 11, '2018-06-19 16:32:22'),
(13, 'Test0810', 1, 'Test0810', 6, '1', '0', 1, '2018-08-10 15:14:07'),
(4, 'FK Test Table 2', 11, 'Test 2', 2, '1', '0', 11, '2018-06-22 07:51:25'),
(5, 'FK Test 2 LU', 11, 'Lookup', 2, '0', '0', 11, '2018-06-22 08:00:25'),
(6, 'shahid test lookup', 3, 'description lookup', 4, '1', '0', 3, '2018-06-23 08:08:28'),
(7, 'shahid test has lookup value', 3, 'description', 4, '1', '0', 3, '2018-06-23 08:12:24'),
(8, 'CPU', 1, 'Processors', 6, '0', '0', 1, '2018-06-26 17:50:45'),
(9, 'Manufacturers', 1, 'Manufacturers', 6, '1', '1', 1, '2018-06-26 17:41:59'),
(10, 'shahidtestingclass', 14, 'shahidtestingclass', 1, '0', '0', 14, '2018-07-31 12:49:24'),
(11, 'Yousaf Class 1', 14, 'Des', 7, '0', '0', 14, '2018-07-31 14:02:55'),
(12, 'Yousaf Class', 14, 'Desctio', 7, '1', '0', 14, '2018-07-31 14:03:06'),
(14, 'My Class', 7, 'My Description', 2, '1', '0', 7, '2018-09-07 07:14:43'),
(15, 'Flowers', 2, 'Flowers', 10, '1', '0', 2, '2019-01-15 12:03:37'),
(16, 'Test 1202019', 1, 'Test category', 6, '0', '1', 1, '2019-01-20 18:30:47'),
(17, 'Mountains', 1, 'Mountains', 6, '1', '0', 1, '2019-01-20 18:38:45'),
(18, 'Countries', 1, 'List of countries', 6, '1', '0', 1, '2019-01-20 18:40:12'),
(19, 'Farrakh Class', 1, 'Farrakh', 6, '1', '0', 1, '2019-02-03 17:11:40'),
(20, 'Test123', 1, 'Test123', 6, '1', '0', 1, '2019-02-03 17:12:14'),
(21, 'FK Test 4', 4, 'Testing', 2, '1', '0', 4, '2019-03-12 08:08:14'),
(22, 'Laptops', 10, 'List of Laptios', 7, '1', '0', 10, '2019-03-12 10:08:35'),
(23, 'test2', 10, 'new class to be deleted', 7, '1', '1', 10, '2019-03-12 10:21:45');

--
-- Triggers `classes`
--
DELIMITER $$
CREATE TRIGGER `audit_Classes_insert` AFTER INSERT ON `classes` FOR EACH ROW INSERT into `classes_audit` (`idclass`, `classname`, `idowner`, `classdescription`, `CatId`, `Status`, `locked`, `change_by`, `change_date`, `action`)
VALUES (NEW.idclass, NEW.classname, NEW.idowner, NEW.classdescription, NEW.CatId, NEW.Status, NEW.locked, NEW.change_by, NOW(), 'created')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_classes_update` AFTER UPDATE ON `classes` FOR EACH ROW BEGIN
    INSERT INTO classes_audit
    SET
    	idclass = NEW.idclass,
     	classname = NEW.classname,
        idowner = NEW.idowner,
        classdescription = NEW.classdescription,
        CatId = NEW.CatId,
        Status = NEW.Status,
        locked = NEW.locked,
        change_by = NEW.change_by,
        change_date = NOW(),
        action = IF(NEW.Status='0', 'deleted', 'updated');
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `classes_audit`
--

CREATE TABLE `classes_audit` (
  `id` int(11) NOT NULL,
  `idclass` int(11) NOT NULL,
  `classname` varchar(45) NOT NULL,
  `idowner` int(11) DEFAULT NULL,
  `classdescription` text DEFAULT NULL,
  `CatId` int(11) NOT NULL,
  `Status` varchar(20) NOT NULL,
  `locked` enum('0','1') NOT NULL,
  `change_by` int(11) NOT NULL,
  `change_date` datetime NOT NULL,
  `action` enum('created','updated','deleted') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classes_audit`
--

INSERT INTO `classes_audit` (`id`, `idclass`, `classname`, `idowner`, `classdescription`, `CatId`, `Status`, `locked`, `change_by`, `change_date`, `action`) VALUES
(1, 1, 'Shahid testing Class', 8, 'Shahid Testing Class descriptionn', 1, '1', '0', 8, '2018-06-11 05:39:48', 'created'),
(2, 1, 'Shahid testing Class', 8, 'Shahid Testing Class descriptionn', 1, '1', '1', 8, '2018-06-11 05:39:53', 'updated'),
(3, 1, 'Shahid testing Class', 8, 'Shahid Testing Class descriptionn', 1, '1', '0', 8, '2018-06-11 05:39:56', 'updated'),
(4, 1, 'Shahid testing Class', 8, 'Shahid Testing Class description', 1, '1', '0', 8, '2018-06-11 05:40:01', 'updated'),
(5, 2, 'FK Table Test 1', 11, 'test', 2, '1', '0', 11, '2018-06-19 16:32:22', 'created'),
(6, 3, 'Look Up table', 11, 'look up', 3, '1', '0', 11, '2018-06-19 16:35:16', 'created'),
(7, 3, 'Look Up table', 11, 'look up', 3, '0', '0', 11, '2018-06-19 16:44:49', 'deleted'),
(8, 4, 'FK Test Table 2', 11, 'Test 2', 2, '1', '0', 11, '2018-06-22 07:51:25', 'created'),
(9, 5, 'FK Test 2 LU', 11, 'Lookup', 2, '1', '0', 11, '2018-06-22 07:51:45', 'created'),
(10, 5, 'FK Test 2 LU', 11, 'Lookup', 2, '0', '0', 11, '2018-06-22 08:00:25', 'deleted'),
(11, 6, 'shahid test lookup', 3, 'description lookup', 4, '1', '0', 3, '2018-06-23 08:08:28', 'created'),
(12, 7, 'shahid test has lookup value', 3, 'description', 4, '1', '0', 3, '2018-06-23 08:11:39', 'created'),
(13, 7, 'shahid test has lookup value', 3, 'description', 4, '0', '0', 3, '2018-06-23 08:12:24', 'deleted'),
(14, 7, 'shahid test has lookup value', 3, 'description', 4, '1', '0', 3, '2018-06-23 08:12:47', 'updated'),
(15, 8, 'CPU', 1, 'Processors', 6, '1', '0', 1, '2018-06-26 17:38:53', 'created'),
(16, 9, 'Manufacturers', 1, 'Manufacturers', 6, '1', '0', 1, '2018-06-26 17:41:59', 'created'),
(17, 8, 'CPU', 1, 'Processors', 6, '0', '0', 1, '2018-06-26 17:50:45', 'deleted'),
(18, 1, 'Shahid testing Class', 8, 'Shahid Testing Class description', 1, '0', '0', 8, '2018-07-31 12:48:50', 'deleted'),
(19, 10, 'shahidtestingclass', 14, 'shahidtestingclass', 1, '1', '0', 14, '2018-07-31 12:49:15', 'created'),
(20, 10, 'shahidtestingclass', 14, 'shahidtestingclass', 1, '0', '0', 14, '2018-07-31 12:49:24', 'deleted'),
(21, 11, 'Yousaf Class 1', 14, 'Des', 7, '1', '0', 14, '2018-07-31 14:02:40', 'created'),
(22, 11, 'Yousaf Class 1', 14, 'Des', 7, '0', '0', 14, '2018-07-31 14:02:55', 'deleted'),
(23, 12, 'Yousaf Class', 14, 'Desctio', 7, '1', '0', 14, '2018-07-31 14:03:06', 'created'),
(24, 13, 'Test0810', 1, 'Test0810', 6, '1', '0', 1, '2018-08-10 15:14:07', 'created'),
(25, 14, 'My Class', 7, 'My Description', 2, '1', '0', 7, '2018-09-07 07:14:43', 'created'),
(26, 15, 'Flowers', 2, 'Flowers', 10, '1', '0', 2, '2019-01-15 12:03:37', 'created'),
(27, 16, 'Test 1202019', 1, 'Test category', 6, '1', '0', 1, '2019-01-20 18:29:37', 'created'),
(28, 16, 'Test 1202019', 1, 'Test category', 6, '1', '1', 1, '2019-01-20 18:30:34', 'updated'),
(29, 16, 'Test 1202019', 1, 'Test category', 6, '0', '1', 1, '2019-01-20 18:30:47', 'deleted'),
(30, 17, 'Mountains', 1, 'Mountains', 6, '1', '0', 1, '2019-01-20 18:38:45', 'created'),
(31, 18, 'Countries', 1, 'List of countries', 6, '1', '0', 1, '2019-01-20 18:40:12', 'created'),
(32, 19, 'Farrakh Class', 1, 'Farrakh', 6, '1', '0', 1, '2019-02-03 17:11:36', 'created'),
(33, 19, 'Farrakh Class', 1, 'Farrakh', 6, '0', '0', 1, '2019-02-03 17:11:40', 'deleted'),
(34, 19, 'Farrakh Class', 1, 'Farrakh', 6, '1', '0', 1, '2019-02-03 17:11:54', 'updated'),
(35, 20, 'Test123', 1, 'Test123', 6, '1', '0', 1, '2019-02-03 17:12:11', 'created'),
(36, 20, 'Test123', 1, 'Test123', 6, '0', '0', 1, '2019-02-03 17:12:14', 'deleted'),
(37, 20, 'Test123', 1, 'Test123', 6, '1', '0', 1, '2019-02-03 17:12:23', 'updated'),
(38, 9, 'Manufacturers', 1, 'Manufacturers', 6, '1', '1', 1, '2019-02-03 17:52:16', 'updated'),
(39, 21, 'FK Test 4', 4, 'Testing', 2, '1', '0', 4, '2019-03-12 08:08:14', 'created'),
(40, 21, 'FK Test 4', 4, 'Testing', 2, '1', '1', 4, '2019-03-12 08:08:24', 'updated'),
(41, 21, 'FK Test 4', 4, 'Testing', 2, '1', '0', 4, '2019-03-12 08:16:42', 'updated'),
(42, 21, 'FK Test 4', 4, 'Testing', 2, '1', '1', 4, '2019-03-12 08:32:47', 'updated'),
(43, 21, 'FK Test 4', 4, 'Testing', 2, '1', '0', 4, '2019-03-12 08:32:48', 'updated'),
(44, 22, 'Laptops', 10, 'List of Laptios', 7, '1', '0', 10, '2019-03-12 10:08:35', 'created'),
(45, 23, 'test2', 10, 'new class to be deleted', 7, '1', '0', 10, '2019-03-12 10:21:45', 'created'),
(46, 23, 'test2', 10, 'new class to be deleted', 7, '1', '1', 10, '2019-03-12 10:23:39', 'updated');

-- --------------------------------------------------------

--
-- Table structure for table `classproperties`
--

CREATE TABLE `classproperties` (
  `idclassproperty` int(11) NOT NULL,
  `idclass` int(11) NOT NULL,
  `propertyname` varchar(45) DEFAULT NULL,
  `propertytype` int(11) NOT NULL DEFAULT 0,
  `propertyorder` int(11) NOT NULL DEFAULT 0,
  `propertylookupclass` varchar(45) DEFAULT NULL,
  `propertylookupfield` varchar(45) DEFAULT NULL,
  `propertylookupquery` varchar(255) DEFAULT NULL,
  `propertydisplayname` varchar(45) NOT NULL,
  `status` enum('1','0') NOT NULL,
  `change_by` int(11) NOT NULL,
  `change_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classproperties`
--

INSERT INTO `classproperties` (`idclassproperty`, `idclass`, `propertyname`, `propertytype`, `propertyorder`, `propertylookupclass`, `propertylookupfield`, `propertylookupquery`, `propertydisplayname`, `status`, `change_by`, `change_date`) VALUES
(1, 1, 'Name', 2, 7, '', '', NULL, 'Name', '1', 8, '2018-06-11 05:40:40'),
(2, 1, 'Second Column', 2, 5, '', '', NULL, 'Second Column', '1', 8, '2018-06-11 05:41:13'),
(3, 1, 'After 1st row', 2, 5, '', '', NULL, 'After 1st row', '1', 8, '2018-06-11 05:43:32'),
(4, 2, 'FK tbl1 Col1', 1, 3, '', '', NULL, 'FK tbl1 Col1', '1', 8, '2018-12-21 10:38:23'),
(5, 2, 'fk tbl1 col2', 2, 1, '', '', NULL, 'fk tbl1 col2', '1', 11, '2018-06-19 16:34:13'),
(32, 13, 'Col2', 2, 6, '', '', NULL, 'Col2', '1', 3, '2018-08-13 12:44:47'),
(31, 13, 'Col1', 2, 1, '', '', NULL, 'Col1', '1', 1, '2018-08-10 15:14:25'),
(8, 2, 'fk Tbl1 col 3LU', 8, 6, '3', '6', NULL, 'fk Tbl1 col 3LU', '1', 4, '2018-06-26 21:25:43'),
(9, 2, 'FK Tbl1 Col 4 LU #', 8, 1, '3', '6', NULL, 'FK Tbl1 Col 4 LU #', '0', 11, '2018-06-19 16:37:56'),
(10, 5, 'Country Name', 2, 1, '', '', NULL, 'Country Name', '1', 11, '2018-06-22 07:52:00'),
(11, 5, 'ISO Code', 2, 1, '', '', NULL, 'ISO Code', '1', 11, '2018-06-22 07:52:12'),
(12, 5, 'Population', 1, 3, '', '', NULL, 'Population', '1', 11, '2018-06-22 07:52:28'),
(13, 4, 'Name', 2, 1, '', '', NULL, 'Name', '1', 11, '2018-06-22 07:53:36'),
(14, 4, 'ISO Code LU', 8, 1, '5', '11', NULL, 'ISO Code LU', '1', 5, '2018-12-30 18:06:33'),
(15, 4, 'Country Name', 8, 3, '5', '10', NULL, 'Country Name', '1', 11, '2018-06-22 07:54:19'),
(16, 6, 'lookup column', 2, 1, '', '', NULL, 'lookup column', '1', 3, '2018-06-23 08:11:03'),
(17, 7, 'look shahid', 8, 1, '6', '16', NULL, 'look shahid', '0', 3, '2018-06-23 08:12:01'),
(18, 8, 'Code name', 2, 1, '', '', NULL, 'Code name', '1', 1, '2018-06-26 17:39:43'),
(19, 8, 'Cores', 1, 3, '', '', NULL, 'Cores', '1', 1, '2018-06-26 17:45:28'),
(20, 9, 'Name', 2, 1, '', '', NULL, 'Name', '1', 1, '2018-06-26 17:42:14'),
(21, 8, 'Manufacturer', 8, 1, '9', '20', NULL, 'Manufacturer', '1', 1, '2018-06-26 17:44:58'),
(22, 8, 'L1 Cache', 2, 3, '', '', NULL, 'L1 Cache', '1', 1, '2018-06-26 17:48:57'),
(23, 9, 'Country', 2, 5, '', '', NULL, 'Country', '0', 1, '2018-06-26 17:57:01'),
(24, 4, 'fktest', 2, 3, '', '', NULL, 'fktest', '0', 4, '2018-07-18 19:48:36'),
(25, 2, 'Col4', 1, 3, '', '', NULL, 'Col4', '0', 4, '2018-07-31 12:41:40'),
(26, 12, 'Name', 2, 1, '', '', NULL, 'Name', '1', 14, '2018-07-31 14:03:36'),
(27, 12, 'Price', 1, 4, '', '', NULL, 'Price', '0', 14, '2018-07-31 14:03:48'),
(28, 12, 'model', 1, 6, '', '', NULL, 'model', '0', 14, '2018-07-31 14:19:00'),
(29, 4, 'Col4', 2, 4, '', '', NULL, 'Col4', '0', 4, '2018-07-31 14:50:05'),
(30, 9, 'Model', 2, 6, '', '', NULL, 'Model', '1', 14, '2018-07-31 14:56:50'),
(33, 13, 'Col3', 1, 5, '', '', NULL, 'Col3', '1', 4, '2018-08-10 15:16:25'),
(34, 2, 'Col1', 1, 4, '', '', NULL, 'Col1', '1', 4, '2018-08-10 15:17:53'),
(35, 13, 'Col4', 2, 7, '', '', NULL, 'Col4', '1', 3, '2018-08-13 12:45:19'),
(36, 13, 'col6', 1, 7, '', '', NULL, 'col6', '1', 14, '2018-08-13 12:45:48'),
(37, 12, 'Father Name', 2, 1, '', '', NULL, 'Father Name', '1', 6, '2018-08-15 05:41:06'),
(38, 12, 'Another Father', 2, 1, '', '', NULL, 'Another Father', '1', 6, '2018-08-15 05:41:41'),
(39, 14, 'Name', 2, 3, '', '', NULL, 'Name', '1', 7, '2018-09-07 07:19:26'),
(40, 14, 'Father  Name', 2, 3, '', '', NULL, 'Father  Name', '1', 7, '2018-09-07 07:15:28'),
(41, 14, 'Roll Num', 1, 3, '', '', NULL, 'Roll Num', '1', 7, '2018-09-07 07:15:47'),
(42, 14, 'New Column', 3, 4, '', '', NULL, 'New Column', '1', 7, '2018-09-07 07:16:41'),
(43, 14, 'New Column 1', 5, 5, '', '', NULL, 'New Column 1', '1', 7, '2018-09-07 07:17:25'),
(44, 2, 'shahidcolumn', 5, 7, '', '', NULL, 'shahidcolumn', '1', 8, '2018-12-21 10:39:17'),
(45, 4, 'Another Country', 8, 4, '9', '30', NULL, 'Another Country', '1', 5, '2018-12-30 18:49:14'),
(46, 9, 'Version', 2, 7, '', '', NULL, 'Version', '1', 2, '2019-01-15 11:51:45'),
(47, 17, 'Name', 2, 0, '', '', NULL, 'Name', '1', 1, '2019-01-20 18:42:11'),
(48, 17, 'Height', 1, 1, '', '', NULL, 'Height', '1', 1, '2019-01-20 18:39:29'),
(50, 17, 'Country', 8, 3, '18', '49', NULL, 'Country', '1', 1, '2019-01-20 18:43:26'),
(49, 18, 'Country Name', 2, 1, '', '', NULL, 'Country Name', '1', 1, '2019-01-20 18:40:35'),
(51, 18, 'Country Name2', 1, 3, '', '', NULL, 'Country Name2', '0', 1, '2019-01-20 18:46:29'),
(52, 18, 'Country Name3', 1, 4, '', '', NULL, 'Country Name3', '0', 1, '2019-01-20 18:48:28'),
(53, 18, 'ABC', 2, 5, '', '', NULL, 'ABC', '0', 1, '2019-01-20 18:50:26'),
(54, 18, 'ABCD', 2, 6, '', '', NULL, 'ABCD', '0', 1, '2019-01-20 18:52:59'),
(55, 18, 'Continent', 2, 7, '', '', NULL, 'Continent', '1', 1, '2019-01-29 14:18:31'),
(56, 18, 'ISOCODE', 2, 8, '', '', NULL, 'ISOCODE', '0', 1, '2019-02-03 17:37:17'),
(57, 21, 'Name', 2, 1, '', '', NULL, 'Name', '1', 4, '2019-03-12 08:16:55'),
(58, 21, 'name 1', 2, 3, '', '', NULL, 'name 1', '1', 4, '2019-03-12 08:17:04'),
(59, 22, 'Name', 2, 1, '', '', NULL, 'Name', '1', 10, '2019-03-12 10:09:04'),
(60, 22, 'Price', 1, 3, '', '', NULL, 'Price', '1', 10, '2019-03-12 10:09:17'),
(61, 22, 'Model', 2, 7, '', '', NULL, 'Model', '1', 10, '2019-03-12 10:09:31'),
(62, 12, 'Models', 8, 2, '22', '61', NULL, 'Models', '1', 10, '2019-03-12 10:18:19');

--
-- Triggers `classproperties`
--
DELIMITER $$
CREATE TRIGGER `audit_classproperties_insert` AFTER INSERT ON `classproperties` FOR EACH ROW INSERT into `classproperties_audit` (`idclassproperty`,`idclass`, `propertyname`, `propertytype`, `propertyorder`, `propertylookupclass`, `propertylookupfield`, `propertylookupquery`, `propertydisplayname`, `status`, `change_by`, `change_date`, `action`)
VALUES (NEW.idclassproperty, NEW.idclass, NEW.propertyname, NEW.propertytype, NEW.propertyorder, NEW.propertylookupclass, NEW.propertylookupfield, NEW.propertylookupquery, NEW.propertydisplayname, NEW.status, NEW.change_by, NOW(), 'created')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_classproperties_update` AFTER UPDATE ON `classproperties` FOR EACH ROW BEGIN
    INSERT INTO classproperties_audit
    SET
    	idclassproperty = OLD.idclassproperty,
     	idclass = NEW.idclass,
        propertyname = NEW.propertyname,
        propertytype = NEW.propertytype,
        propertyorder = NEW.propertyorder,
        propertylookupclass = NEW.propertylookupclass,
        propertylookupfield = NEW.propertylookupfield,
        propertylookupquery = NEW.propertylookupquery,
        propertydisplayname = NEW.propertydisplayname,
        status = NEW.status,
        change_by = NEW.change_by,
        change_date = NOW(),
        action = IF(NEW.status = '1', 'updated', 'deleted');
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `classproperties_audit`
--

CREATE TABLE `classproperties_audit` (
  `id` int(11) NOT NULL,
  `idclassproperty` int(11) NOT NULL,
  `idclass` int(11) NOT NULL,
  `propertyname` varchar(45) DEFAULT NULL,
  `propertytype` int(11) NOT NULL DEFAULT 0,
  `propertyorder` int(11) NOT NULL DEFAULT 0,
  `propertylookupclass` varchar(45) DEFAULT NULL,
  `propertylookupfield` varchar(45) DEFAULT NULL,
  `propertylookupquery` varchar(255) DEFAULT NULL,
  `propertydisplayname` varchar(45) NOT NULL,
  `status` enum('1','0') NOT NULL,
  `change_by` int(11) NOT NULL,
  `change_date` datetime NOT NULL,
  `action` enum('created','updated','deleted') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `classproperties_audit`
--

INSERT INTO `classproperties_audit` (`id`, `idclassproperty`, `idclass`, `propertyname`, `propertytype`, `propertyorder`, `propertylookupclass`, `propertylookupfield`, `propertylookupquery`, `propertydisplayname`, `status`, `change_by`, `change_date`, `action`) VALUES
(1, 1, 1, 'Name', 2, 1, '', '', NULL, 'Name', '1', 8, '2018-06-11 05:40:40', 'created'),
(2, 2, 1, 'Second Column', 2, 2, '', '', NULL, 'Second Column', '1', 8, '2018-06-11 05:41:13', 'created'),
(3, 3, 1, 'After 1st row', 2, 3, '', '', NULL, 'After 1st row', '1', 8, '2018-06-11 05:43:32', 'created'),
(4, 4, 2, 'FK tbl1 Col1', 2, 0, '', '', NULL, 'FK tbl1 Col1', '1', 11, '2018-06-19 16:32:54', 'created'),
(5, 1, 1, 'Name', 2, 1, '', '', NULL, 'Name', '1', 8, '2018-06-19 16:33:11', 'updated'),
(6, 5, 2, 'fk tbl1 col2', 1, 1, '', '', NULL, 'fk tbl1 col2', '1', 11, '2018-06-19 16:33:11', 'created'),
(7, 1, 1, 'Name', 2, 1, '', '', NULL, 'Name', '1', 8, '2018-06-19 16:34:13', 'updated'),
(8, 5, 2, 'fk tbl1 col2', 2, 1, '', '', NULL, 'fk tbl1 col2', '1', 11, '2018-06-19 16:34:13', 'updated'),
(9, 4, 2, 'FK tbl1 Col1', 2, 0, '', '', NULL, 'FK tbl1 Col1', '1', 11, '2018-06-19 16:34:21', 'updated'),
(10, 4, 2, 'FK tbl1 Col1', 1, 0, '', '', NULL, 'FK tbl1 Col1', '1', 11, '2018-06-19 16:34:21', 'updated'),
(11, 1, 1, 'Name', 2, 1, '', '', NULL, 'Name', '1', 8, '2018-06-19 16:35:32', 'updated'),
(12, 6, 3, 'LU Col 1 #', 1, 1, '', '', NULL, 'LU Col 1 #', '1', 11, '2018-06-19 16:35:32', 'created'),
(13, 2, 1, 'Second Column', 2, 2, '', '', NULL, 'Second Column', '1', 8, '2018-06-19 16:35:53', 'updated'),
(14, 7, 3, 'LU Col 2 Text', 2, 2, '', '', NULL, 'LU Col 2 Text', '1', 11, '2018-06-19 16:35:53', 'created'),
(15, 2, 1, 'Second Column', 2, 2, '', '', NULL, 'Second Column', '1', 8, '2018-06-19 16:37:36', 'updated'),
(16, 8, 2, 'fk Tbl1 col 3LU', 8, 2, '3', '6', NULL, 'fk Tbl1 col 3LU', '1', 11, '2018-06-19 16:37:36', 'created'),
(17, 3, 1, 'After 1st row', 2, 3, '', '', NULL, 'After 1st row', '1', 8, '2018-06-19 16:37:56', 'updated'),
(18, 9, 2, 'FK Tbl1 Col 4 LU #', 8, 3, '3', '6', NULL, 'FK Tbl1 Col 4 LU #', '1', 11, '2018-06-19 16:37:56', 'created'),
(19, 2, 1, 'Second Column', 2, 2, '', '', NULL, 'Second Column', '1', 8, '2018-06-19 16:42:40', 'updated'),
(20, 8, 2, 'fk Tbl1 col 3LU', 8, 2, '3', '6', NULL, 'fk Tbl1 col 3LU', '1', 11, '2018-06-19 16:42:40', 'updated'),
(21, 4, 2, 'FK tbl1 Col1', 1, 1, '', '', NULL, 'FK tbl1 Col1', '1', 11, '2018-06-22 07:52:00', 'updated'),
(22, 10, 5, 'Country Name', 2, 0, '', '', NULL, 'Country Name', '1', 11, '2018-06-22 07:52:00', 'created'),
(23, 1, 1, 'Name', 2, 1, '', '', NULL, 'Name', '1', 8, '2018-06-22 07:52:12', 'updated'),
(24, 11, 5, 'ISO Code', 2, 1, '', '', NULL, 'ISO Code', '1', 11, '2018-06-22 07:52:12', 'created'),
(25, 2, 1, 'Second Column', 2, 2, '', '', NULL, 'Second Column', '1', 8, '2018-06-22 07:52:28', 'updated'),
(26, 12, 5, 'Population', 1, 2, '', '', NULL, 'Population', '1', 11, '2018-06-22 07:52:28', 'created'),
(27, 10, 5, 'Country Name', 2, 1, '', '', NULL, 'Country Name', '1', 11, '2018-06-22 07:53:36', 'updated'),
(28, 13, 4, 'Name', 2, 0, '', '', NULL, 'Name', '1', 11, '2018-06-22 07:53:36', 'created'),
(29, 1, 1, 'Name', 2, 1, '', '', NULL, 'Name', '1', 8, '2018-06-22 07:54:00', 'updated'),
(30, 14, 4, 'ISO Code LU', 8, 1, '5', '11', NULL, 'ISO Code LU', '1', 11, '2018-06-22 07:54:00', 'created'),
(31, 2, 1, 'Second Column', 2, 2, '', '', NULL, 'Second Column', '1', 8, '2018-06-22 07:54:19', 'updated'),
(32, 15, 4, 'Country Name', 8, 2, '5', '10', NULL, 'Country Name', '1', 11, '2018-06-22 07:54:19', 'created'),
(33, 13, 4, 'Name', 2, 1, '', '', NULL, 'Name', '1', 11, '2018-06-23 08:11:03', 'updated'),
(34, 16, 6, 'lookup column', 2, 0, '', '', NULL, 'lookup column', '1', 3, '2018-06-23 08:11:03', 'created'),
(35, 1, 1, 'Name', 2, 1, '', '', NULL, 'Name', '1', 8, '2018-06-23 08:12:01', 'updated'),
(36, 17, 7, 'look shahid', 8, 1, '6', '16', NULL, 'look shahid', '1', 3, '2018-06-23 08:12:01', 'created'),
(37, 16, 6, 'lookup column', 2, 1, '', '', NULL, 'lookup column', '1', 3, '2018-06-26 17:39:43', 'updated'),
(38, 18, 8, 'Code name', 2, 0, '', '', NULL, 'Code name', '1', 1, '2018-06-26 17:39:43', 'created'),
(39, 1, 1, 'Name', 2, 1, '', '', NULL, 'Name', '1', 8, '2018-06-26 17:41:13', 'updated'),
(40, 19, 8, 'Cores', 1, 1, '', '', NULL, 'Cores', '1', 1, '2018-06-26 17:41:13', 'created'),
(41, 1, 1, 'Name', 2, 1, '', '', NULL, 'Name', '1', 8, '2018-06-26 17:42:14', 'updated'),
(42, 20, 9, 'Name', 2, 1, '', '', NULL, 'Name', '1', 1, '2018-06-26 17:42:14', 'created'),
(43, 1, 1, 'Name', 2, 2, '', '', NULL, 'Name', '1', 8, '2018-06-26 17:44:58', 'updated'),
(44, 21, 8, 'Manufacturer', 8, 1, '9', '20', NULL, 'Manufacturer', '1', 1, '2018-06-26 17:44:58', 'created'),
(45, 1, 1, 'Name', 2, 1, '', '', NULL, 'Name', '1', 8, '2018-06-26 17:45:28', 'updated'),
(46, 19, 8, 'Cores', 1, 2, '', '', NULL, 'Cores', '1', 1, '2018-06-26 17:45:28', 'updated'),
(47, 3, 1, 'After 1st row', 2, 3, '', '', NULL, 'After 1st row', '1', 8, '2018-06-26 17:48:57', 'updated'),
(48, 22, 8, 'L1 Cache', 2, 3, '', '', NULL, 'L1 Cache', '1', 1, '2018-06-26 17:48:57', 'created'),
(49, 2, 1, 'Second Column', 2, 2, '', '', NULL, 'Second Column', '1', 8, '2018-06-26 17:57:01', 'updated'),
(50, 23, 9, 'Country', 2, 2, '', '', NULL, 'Country', '1', 1, '2018-06-26 17:57:01', 'created'),
(51, 23, 9, 'Country', 2, 2, '', '', NULL, 'Country', '0', 1, '2018-06-26 17:57:07', 'deleted'),
(52, 1, 1, 'Name', 2, 1, '', '', NULL, 'Name', '1', 8, '2018-06-26 21:20:23', 'updated'),
(53, 14, 4, 'ISO Code LU', 8, 1, '5', '11', NULL, 'ISO Code LU', '1', 4, '2018-06-26 21:20:23', 'updated'),
(54, 2, 1, 'Second Column', 2, 2, '', '', NULL, 'Second Column', '1', 8, '2018-06-26 21:25:21', 'updated'),
(55, 8, 2, 'fk Tbl1 col 3LU', 8, 2, '3', '6', NULL, 'fk Tbl1 col 3LU', '1', 4, '2018-06-26 21:25:21', 'updated'),
(56, 2, 1, 'Second Column', 2, 2, '', '', NULL, 'Second Column', '1', 8, '2018-06-26 21:25:43', 'updated'),
(57, 8, 2, 'fk Tbl1 col 3LU', 8, 2, '3', '6', NULL, 'fk Tbl1 col 3LU', '1', 4, '2018-06-26 21:25:43', 'updated'),
(58, 3, 1, 'After 1st row', 2, 3, '', '', NULL, 'After 1st row', '1', 8, '2018-07-18 19:48:36', 'updated'),
(59, 24, 4, 'fktest', 2, 3, '', '', NULL, 'fktest', '1', 4, '2018-07-18 19:48:36', 'created'),
(60, 24, 4, 'fktest', 2, 3, '', '', NULL, 'fktest', '0', 4, '2018-07-18 19:48:44', 'deleted'),
(61, 9, 2, 'FK Tbl1 Col 4 LU #', 8, 3, '3', '6', NULL, 'FK Tbl1 Col 4 LU #', '0', 11, '2018-07-18 19:51:45', 'deleted'),
(62, 17, 7, 'look shahid', 8, 1, '6', '16', NULL, 'look shahid', '0', 3, '2018-07-18 19:52:19', 'deleted'),
(63, 3, 1, 'After 1st row', 2, 4, '', '', NULL, 'After 1st row', '1', 8, '2018-07-31 12:41:40', 'updated'),
(64, 25, 2, 'Col4', 1, 3, '', '', NULL, 'Col4', '1', 4, '2018-07-31 12:41:40', 'created'),
(65, 25, 2, 'Col4', 1, 3, '', '', NULL, 'Col4', '0', 4, '2018-07-31 12:41:49', 'deleted'),
(66, 1, 1, 'Name', 2, 1, '', '', NULL, 'Name', '1', 8, '2018-07-31 14:03:36', 'updated'),
(67, 26, 12, 'Name', 2, 1, '', '', NULL, 'Name', '1', 14, '2018-07-31 14:03:36', 'created'),
(68, 2, 1, 'Second Column', 2, 2, '', '', NULL, 'Second Column', '1', 8, '2018-07-31 14:03:48', 'updated'),
(69, 27, 12, 'Price', 1, 2, '', '', NULL, 'Price', '1', 14, '2018-07-31 14:03:48', 'created'),
(70, 27, 12, 'Price', 1, 2, '', '', NULL, 'Price', '0', 14, '2018-07-31 14:07:16', 'deleted'),
(71, 2, 1, 'Second Column', 2, 3, '', '', NULL, 'Second Column', '1', 8, '2018-07-31 14:19:00', 'updated'),
(72, 28, 12, 'model', 1, 2, '', '', NULL, 'model', '1', 14, '2018-07-31 14:19:00', 'created'),
(73, 28, 12, 'model', 1, 2, '', '', NULL, 'model', '0', 14, '2018-07-31 14:19:12', 'deleted'),
(74, 7, 3, 'LU Col 2 Text', 2, 3, '', '', NULL, 'LU Col 2 Text', '1', 11, '2018-07-31 14:25:01', 'updated'),
(75, 28, 12, 'model', 1, 2, '', '', NULL, 'model', '1', 14, '2018-07-31 14:25:01', 'updated'),
(76, 28, 12, 'model', 1, 2, '', '', NULL, 'model', '0', 14, '2018-07-31 14:25:10', 'deleted'),
(77, 8, 2, 'fk Tbl1 col 3LU', 8, 3, '3', '6', NULL, 'fk Tbl1 col 3LU', '1', 4, '2018-07-31 14:25:26', 'updated'),
(78, 12, 5, 'Population', 1, 3, '', '', NULL, 'Population', '1', 11, '2018-07-31 14:25:40', 'updated'),
(79, 28, 12, 'model', 1, 2, '', '', NULL, 'model', '1', 14, '2018-07-31 14:25:40', 'updated'),
(80, 28, 12, 'model', 1, 2, '', '', NULL, 'model', '0', 14, '2018-07-31 14:26:13', 'deleted'),
(81, 15, 4, 'Country Name', 8, 3, '5', '10', NULL, 'Country Name', '1', 11, '2018-07-31 14:26:37', 'updated'),
(82, 28, 12, 'model', 1, 2, '', '', NULL, 'model', '1', 14, '2018-07-31 14:26:37', 'updated'),
(83, 3, 1, 'After 1st row', 2, 4, '', '', NULL, 'After 1st row', '1', 8, '2018-07-31 14:49:37', 'updated'),
(84, 29, 4, 'Col4', 1, 4, '', '', NULL, 'Col4', '1', 4, '2018-07-31 14:49:37', 'created'),
(85, 29, 4, 'Col4', 1, 4, '', '', NULL, 'Col4', '0', 4, '2018-07-31 14:49:43', 'deleted'),
(86, 3, 1, 'After 1st row', 2, 5, '', '', NULL, 'After 1st row', '1', 8, '2018-07-31 14:49:53', 'updated'),
(87, 29, 4, 'Col4', 1, 4, '', '', NULL, 'Col4', '1', 4, '2018-07-31 14:49:53', 'updated'),
(88, 29, 4, 'Col4', 1, 4, '', '', NULL, 'Col4', '1', 4, '2018-07-31 14:50:05', 'updated'),
(89, 29, 4, 'Col4', 2, 4, '', '', NULL, 'Col4', '1', 4, '2018-07-31 14:50:05', 'updated'),
(90, 19, 8, 'Cores', 1, 3, '', '', NULL, 'Cores', '1', 1, '2018-07-31 14:56:50', 'updated'),
(91, 30, 9, 'Model', 2, 2, '', '', NULL, 'Model', '1', 14, '2018-07-31 14:56:50', 'created'),
(92, 29, 4, 'Col4', 2, 4, '', '', NULL, 'Col4', '0', 4, '2018-07-31 14:57:13', 'deleted'),
(93, 28, 12, 'model', 1, 2, '', '', NULL, 'model', '0', 14, '2018-07-31 14:57:27', 'deleted'),
(94, 1, 1, 'Name', 2, 1, '', '', NULL, 'Name', '1', 8, '2018-08-10 15:14:25', 'updated'),
(95, 31, 13, 'Col1', 2, 1, '', '', NULL, 'Col1', '1', 1, '2018-08-10 15:14:25', 'created'),
(96, 23, 9, 'Country', 2, 2, '', '', NULL, 'Country', '0', 1, '2018-08-10 15:15:13', 'deleted'),
(97, 32, 13, 'Col2', 2, 2, '', '', NULL, 'Col2', '1', 1, '2018-08-10 15:15:13', 'created'),
(98, 32, 13, 'Col2', 2, 2, '', '', NULL, 'Col2', '0', 1, '2018-08-10 15:15:19', 'deleted'),
(99, 32, 13, 'Col2', 2, 3, '', '', NULL, 'Col2', '0', 1, '2018-08-10 15:15:29', 'deleted'),
(100, 23, 9, 'Country', 2, 4, '', '', NULL, 'Country', '0', 1, '2018-08-10 15:16:09', 'deleted'),
(101, 27, 12, 'Price', 1, 4, '', '', NULL, 'Price', '0', 14, '2018-08-10 15:16:25', 'deleted'),
(102, 33, 13, 'Col3', 1, 2, '', '', NULL, 'Col3', '1', 4, '2018-08-10 15:16:25', 'created'),
(103, 2, 1, 'Second Column', 2, 4, '', '', NULL, 'Second Column', '1', 8, '2018-08-10 15:16:36', 'updated'),
(104, 32, 13, 'Col2', 2, 4, '', '', NULL, 'Col2', '0', 1, '2018-08-10 15:17:37', 'deleted'),
(105, 2, 1, 'Second Column', 2, 4, '', '', NULL, 'Second Column', '1', 8, '2018-08-10 15:17:53', 'updated'),
(106, 34, 2, 'Col1', 1, 4, '', '', NULL, 'Col1', '1', 4, '2018-08-10 15:17:53', 'created'),
(107, 34, 2, 'Col1', 1, 4, '', '', NULL, 'Col1', '0', 4, '2018-08-10 15:17:58', 'deleted'),
(108, 2, 1, 'Second Column', 2, 5, '', '', NULL, 'Second Column', '1', 8, '2018-08-10 15:18:05', 'updated'),
(109, 34, 2, 'Col1', 1, 4, '', '', NULL, 'Col1', '1', 4, '2018-08-10 15:18:05', 'updated'),
(110, 34, 2, 'Col1', 1, 4, '', '', NULL, 'Col1', '0', 4, '2018-08-10 15:18:14', 'deleted'),
(111, 32, 13, 'Col2', 2, 5, '', '', NULL, 'Col2', '0', 1, '2018-08-10 15:18:22', 'deleted'),
(112, 34, 2, 'Col1', 1, 4, '', '', NULL, 'Col1', '1', 4, '2018-08-10 15:18:22', 'updated'),
(113, 8, 2, 'fk Tbl1 col 3LU', 8, 6, '3', '6', NULL, 'fk Tbl1 col 3LU', '1', 4, '2018-08-10 15:21:09', 'updated'),
(114, 28, 12, 'model', 1, 6, '', '', NULL, 'model', '0', 14, '2018-08-13 12:43:13', 'deleted'),
(115, 32, 13, 'Col2', 2, 5, '', '', NULL, 'Col2', '1', 1, '2018-08-13 12:43:13', 'updated'),
(116, 32, 13, 'Col2', 2, 5, '', '', NULL, 'Col2', '0', 1, '2018-08-13 12:43:36', 'deleted'),
(117, 30, 9, 'Model', 2, 6, '', '', NULL, 'Model', '1', 14, '2018-08-13 12:43:52', 'updated'),
(118, 32, 13, 'Col2', 2, 5, '', '', NULL, 'Col2', '1', 1, '2018-08-13 12:43:52', 'updated'),
(119, 33, 13, 'Col3', 1, 5, '', '', NULL, 'Col3', '1', 4, '2018-08-13 12:44:47', 'updated'),
(120, 32, 13, 'Col2', 2, 2, '', '', NULL, 'Col2', '1', 3, '2018-08-13 12:44:47', 'updated'),
(121, 32, 13, 'Col2', 2, 6, '', '', NULL, 'Col2', '1', 3, '2018-08-13 12:45:19', 'updated'),
(122, 35, 13, 'Col4', 2, 2, '', '', NULL, 'Col4', '1', 3, '2018-08-13 12:45:19', 'created'),
(123, 35, 13, 'Col4', 2, 7, '', '', NULL, 'Col4', '1', 3, '2018-08-13 12:45:48', 'updated'),
(124, 36, 13, 'col6', 1, 2, '', '', NULL, 'col6', '1', 14, '2018-08-13 12:45:48', 'created'),
(125, 36, 13, 'col6', 1, 7, '', '', NULL, 'col6', '1', 14, '2018-08-15 05:41:06', 'updated'),
(126, 37, 12, 'Father Name', 2, 2, '', '', NULL, 'Father Name', '1', 6, '2018-08-15 05:41:06', 'created'),
(127, 1, 1, 'Name', 2, 7, '', '', NULL, 'Name', '1', 8, '2018-08-15 05:41:41', 'updated'),
(128, 38, 12, 'Another Father', 2, 1, '', '', NULL, 'Another Father', '1', 6, '2018-08-15 05:41:41', 'created'),
(129, 4, 2, 'FK tbl1 Col1', 1, 1, '', '', NULL, 'FK tbl1 Col1', '1', 11, '2018-09-07 07:15:12', 'updated'),
(130, 39, 14, 'Name', 2, 1, '', '', NULL, 'Name', '1', 7, '2018-09-07 07:15:12', 'created'),
(131, 37, 12, 'Father Name', 2, 2, '', '', NULL, 'Father Name', '1', 6, '2018-09-07 07:15:28', 'updated'),
(132, 40, 14, 'Father  Name', 2, 2, '', '', NULL, 'Father  Name', '1', 7, '2018-09-07 07:15:28', 'created'),
(133, 9, 2, 'FK Tbl1 Col 4 LU #', 8, 3, '3', '6', NULL, 'FK Tbl1 Col 4 LU #', '0', 11, '2018-09-07 07:15:47', 'deleted'),
(134, 41, 14, 'Roll Num', 1, 3, '', '', NULL, 'Roll Num', '1', 7, '2018-09-07 07:15:47', 'created'),
(135, 23, 9, 'Country', 2, 4, '', '', NULL, 'Country', '0', 1, '2018-09-07 07:16:41', 'deleted'),
(136, 42, 14, 'New Column', 3, 4, '', '', NULL, 'New Column', '1', 7, '2018-09-07 07:16:41', 'created'),
(137, 2, 1, 'Second Column', 2, 5, '', '', NULL, 'Second Column', '1', 8, '2018-09-07 07:17:25', 'updated'),
(138, 43, 14, 'New Column 1', 5, 5, '', '', NULL, 'New Column 1', '1', 7, '2018-09-07 07:17:25', 'created'),
(139, 9, 2, 'FK Tbl1 Col 4 LU #', 8, 1, '3', '6', NULL, 'FK Tbl1 Col 4 LU #', '0', 11, '2018-09-07 07:19:26', 'deleted'),
(140, 39, 14, 'Name', 2, 3, '', '', NULL, 'Name', '1', 7, '2018-09-07 07:19:26', 'updated'),
(141, 37, 12, 'Father Name', 2, 1, '', '', NULL, 'Father Name', '1', 6, '2018-12-21 10:38:23', 'updated'),
(142, 4, 2, 'FK tbl1 Col1', 1, 2, '', '', NULL, 'FK tbl1 Col1', '1', 8, '2018-12-21 10:38:23', 'updated'),
(143, 1, 1, 'Name', 2, 7, '', '', NULL, 'Name', '1', 8, '2018-12-21 10:39:17', 'updated'),
(144, 44, 2, 'shahidcolumn', 5, 7, '', '', NULL, 'shahidcolumn', '1', 8, '2018-12-21 10:39:17', 'created'),
(145, 5, 2, 'fk tbl1 col2', 2, 1, '', '', NULL, 'fk tbl1 col2', '1', 11, '2018-12-30 18:04:59', 'updated'),
(146, 14, 4, 'ISO Code LU', 8, 1, '5', '11', NULL, 'ISO Code LU', '1', 5, '2018-12-30 18:04:59', 'updated'),
(147, 5, 2, 'fk tbl1 col2', 2, 1, '', '', NULL, 'fk tbl1 col2', '1', 11, '2018-12-30 18:06:14', 'updated'),
(148, 14, 4, 'ISO Code LU', 8, 1, '5', '11', NULL, 'ISO Code LU', '1', 5, '2018-12-30 18:06:14', 'updated'),
(149, 5, 2, 'fk tbl1 col2', 2, 1, '', '', NULL, 'fk tbl1 col2', '1', 11, '2018-12-30 18:06:33', 'updated'),
(150, 14, 4, 'ISO Code LU', 8, 1, '5', '11', NULL, 'ISO Code LU', '1', 5, '2018-12-30 18:06:33', 'updated'),
(151, 23, 9, 'Country', 2, 5, '', '', NULL, 'Country', '0', 1, '2018-12-30 18:49:14', 'deleted'),
(152, 45, 4, 'Another Country', 8, 4, '9', '30', NULL, 'Another Country', '1', 5, '2018-12-30 18:49:14', 'created'),
(153, 1, 1, 'Name', 2, 7, '', '', NULL, 'Name', '1', 8, '2019-01-15 11:51:45', 'updated'),
(154, 46, 9, 'Version', 2, 7, '', '', NULL, 'Version', '1', 2, '2019-01-15 11:51:45', 'created'),
(155, 18, 8, 'Code name', 2, 1, '', '', NULL, 'Code name', '1', 1, '2019-01-20 18:39:15', 'updated'),
(156, 47, 17, 'Name', 2, 0, '', '', NULL, 'Name', '1', 1, '2019-01-20 18:39:15', 'created'),
(157, 5, 2, 'fk tbl1 col2', 2, 1, '', '', NULL, 'fk tbl1 col2', '1', 11, '2019-01-20 18:39:29', 'updated'),
(158, 48, 17, 'Height', 1, 1, '', '', NULL, 'Height', '1', 1, '2019-01-20 18:39:29', 'created'),
(159, 47, 17, 'Name', 2, 1, '', '', NULL, 'Name', '1', 1, '2019-01-20 18:40:35', 'updated'),
(160, 49, 18, 'Country Name', 2, 0, '', '', NULL, 'Country Name', '1', 1, '2019-01-20 18:40:35', 'created'),
(161, 49, 18, 'Country Name', 2, 1, '', '', NULL, 'Country Name', '1', 1, '2019-01-20 18:42:11', 'updated'),
(162, 47, 17, 'Name', 2, 0, '', '', NULL, 'Name', '1', 1, '2019-01-20 18:42:11', 'updated'),
(163, 4, 2, 'FK tbl1 Col1', 1, 2, '', '', NULL, 'FK tbl1 Col1', '1', 8, '2019-01-20 18:43:26', 'updated'),
(164, 50, 17, 'Country', 8, 2, '18', '49', NULL, 'Country', '1', 1, '2019-01-20 18:43:26', 'created'),
(165, 4, 2, 'FK tbl1 Col1', 1, 2, '', '', NULL, 'FK tbl1 Col1', '1', 8, '2019-01-20 18:46:29', 'updated'),
(166, 51, 18, 'Country Name2', 1, 2, '', '', NULL, 'Country Name2', '1', 1, '2019-01-20 18:46:29', 'created'),
(167, 51, 18, 'Country Name2', 1, 2, '', '', NULL, 'Country Name2', '0', 1, '2019-01-20 18:46:55', 'deleted'),
(168, 4, 2, 'FK tbl1 Col1', 1, 3, '', '', NULL, 'FK tbl1 Col1', '1', 8, '2019-01-20 18:47:08', 'updated'),
(169, 51, 18, 'Country Name2', 1, 2, '', '', NULL, 'Country Name2', '1', 1, '2019-01-20 18:47:08', 'updated'),
(170, 51, 18, 'Country Name2', 1, 2, '', '', NULL, 'Country Name2', '0', 1, '2019-01-20 18:47:23', 'deleted'),
(171, 40, 14, 'Father  Name', 2, 3, '', '', NULL, 'Father  Name', '1', 7, '2019-01-20 18:47:53', 'updated'),
(172, 50, 17, 'Country', 8, 3, '18', '49', NULL, 'Country', '1', 1, '2019-01-20 18:48:28', 'updated'),
(173, 52, 18, 'Country Name3', 1, 2, '', '', NULL, 'Country Name3', '1', 1, '2019-01-20 18:48:28', 'created'),
(174, 52, 18, 'Country Name3', 1, 2, '', '', NULL, 'Country Name3', '0', 1, '2019-01-20 18:48:33', 'deleted'),
(175, 51, 18, 'Country Name2', 1, 3, '', '', NULL, 'Country Name2', '0', 1, '2019-01-20 18:48:45', 'deleted'),
(176, 52, 18, 'Country Name3', 1, 4, '', '', NULL, 'Country Name3', '0', 1, '2019-01-20 18:50:26', 'deleted'),
(177, 53, 18, 'ABC', 2, 2, '', '', NULL, 'ABC', '1', 1, '2019-01-20 18:50:26', 'created'),
(178, 53, 18, 'ABC', 2, 2, '', '', NULL, 'ABC', '0', 1, '2019-01-20 18:52:10', 'deleted'),
(179, 53, 18, 'ABC', 2, 5, '', '', NULL, 'ABC', '0', 1, '2019-01-20 18:52:59', 'deleted'),
(180, 54, 18, 'ABCD', 2, 2, '', '', NULL, 'ABCD', '1', 1, '2019-01-20 18:52:59', 'created'),
(181, 54, 18, 'ABCD', 2, 2, '', '', NULL, 'ABCD', '0', 1, '2019-01-20 18:53:33', 'deleted'),
(182, 54, 18, 'ABCD', 2, 6, '', '', NULL, 'ABCD', '0', 1, '2019-01-29 14:18:31', 'deleted'),
(183, 55, 18, 'Continent', 2, 2, '', '', NULL, 'Continent', '1', 1, '2019-01-29 14:18:31', 'created'),
(184, 55, 18, 'Continent', 2, 2, '', '', NULL, 'Continent', '0', 1, '2019-02-03 17:17:35', 'deleted'),
(185, 55, 18, 'Continent', 2, 7, '', '', NULL, 'Continent', '0', 1, '2019-02-03 17:18:14', 'deleted'),
(186, 55, 18, 'Continent', 2, 7, '', '', NULL, 'Continent', '1', 1, '2019-02-03 17:18:14', 'updated'),
(187, 55, 18, 'Continent', 2, 7, '', '', NULL, 'Continent', '0', 1, '2019-02-03 17:36:09', 'deleted'),
(188, 55, 18, 'Continent', 2, 7, '', '', NULL, 'Continent', '1', 1, '2019-02-03 17:36:33', 'updated'),
(189, 56, 18, 'ISOCODE', 2, 8, '', '', NULL, 'ISOCODE', '1', 1, '2019-02-03 17:37:17', 'created'),
(190, 56, 18, 'ISOCODE', 2, 8, '', '', NULL, 'ISOCODE', '0', 1, '2019-02-03 17:37:33', 'deleted'),
(191, 5, 2, 'fk tbl1 col2', 2, 1, '', '', NULL, 'fk tbl1 col2', '1', 11, '2019-03-12 08:16:55', 'updated'),
(192, 57, 21, 'Name', 2, 1, '', '', NULL, 'Name', '1', 4, '2019-03-12 08:16:55', 'created'),
(193, 58, 21, 'name 1', 2, 2, '', '', NULL, 'name 1', '1', 4, '2019-03-12 08:17:04', 'created'),
(194, 58, 21, 'name 1', 2, 2, '', '', NULL, 'name 1', '0', 4, '2019-03-12 08:17:08', 'deleted'),
(195, 58, 21, 'name 1', 2, 3, '', '', NULL, 'name 1', '0', 4, '2019-03-12 08:17:23', 'deleted'),
(196, 58, 21, 'name 1', 2, 3, '', '', NULL, 'name 1', '1', 4, '2019-03-12 08:17:38', 'updated'),
(197, 58, 21, 'name 1', 2, 3, '', '', NULL, 'name 1', '0', 4, '2019-03-12 08:17:44', 'deleted'),
(198, 58, 21, 'name 1', 2, 3, '', '', NULL, 'name 1', '1', 4, '2019-03-12 08:18:56', 'updated'),
(199, 58, 21, 'name 1', 2, 3, '', '', NULL, 'name 1', '0', 4, '2019-03-12 08:19:19', 'deleted'),
(200, 58, 21, 'name 1', 2, 3, '', '', NULL, 'name 1', '1', 4, '2019-03-12 08:19:57', 'updated'),
(201, 5, 2, 'fk tbl1 col2', 2, 1, '', '', NULL, 'fk tbl1 col2', '1', 11, '2019-03-12 10:09:04', 'updated'),
(202, 59, 22, 'Name', 2, 1, '', '', NULL, 'Name', '1', 10, '2019-03-12 10:09:04', 'created'),
(203, 60, 22, 'Price', 1, 2, '', '', NULL, 'Price', '1', 10, '2019-03-12 10:09:17', 'created'),
(204, 60, 22, 'Price', 1, 3, '', '', NULL, 'Price', '1', 10, '2019-03-12 10:09:31', 'updated'),
(205, 61, 22, 'Model', 2, 2, '', '', NULL, 'Model', '1', 10, '2019-03-12 10:09:31', 'created'),
(206, 61, 22, 'Model', 2, 7, '', '', NULL, 'Model', '1', 10, '2019-03-12 10:14:41', 'updated'),
(207, 62, 12, 'Models', 8, 2, '22', '61', NULL, 'Models', '1', 10, '2019-03-12 10:14:41', 'created'),
(208, 62, 12, 'Models', 8, 2, '22', '61', NULL, 'Models', '1', 10, '2019-03-12 10:16:51', 'updated'),
(209, 62, 12, 'Models', 8, 2, '22', '61', NULL, 'Models', '1', 10, '2019-03-12 10:16:51', 'updated'),
(210, 62, 12, 'Models', 8, 2, '22', '61', NULL, 'Models', '1', 10, '2019-03-12 10:17:24', 'updated'),
(211, 62, 12, 'Models', 8, 2, '22', '61', NULL, 'Models', '1', 10, '2019-03-12 10:17:24', 'updated'),
(212, 62, 12, 'Models', 8, 2, '22', '61', NULL, 'Models', '1', 10, '2019-03-12 10:18:19', 'updated'),
(213, 62, 12, 'Models', 8, 2, '22', '61', NULL, 'Models', '1', 10, '2019-03-12 10:18:19', 'updated');

-- --------------------------------------------------------

--
-- Table structure for table `followers`
--

CREATE TABLE `followers` (
  `id` int(11) NOT NULL,
  `iduser` int(11) DEFAULT NULL,
  `idclass` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kol`
--

CREATE TABLE `kol` (
  `id` int(11) NOT NULL,
  `idclass` int(11) NOT NULL,
  `iduser` int(11) NOT NULL,
  `access_level` varchar(11) NOT NULL,
  `granted_date` date NOT NULL,
  `granted_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `maincategory`
--

CREATE TABLE `maincategory` (
  `catid` int(11) NOT NULL,
  `catname` varchar(255) NOT NULL,
  `catdescription` varchar(255) NOT NULL,
  `parentcatid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `maincategory`
--

INSERT INTO `maincategory` (`catid`, `catname`, `catdescription`, `parentcatid`) VALUES
(1, 'shahidtesting', 'shahidtesting description', 0),
(2, 'Farrakh Test', 'Testing New Changes', 0),
(4, 'shahid new test', 'description', 0),
(6, 'Olivier Testing', 'Olivier Testing category', 0),
(7, 'Yousaf Testing', 'Description', 0),
(10, 'Marianne Testing', 'Testing V5', 0);

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(4) NOT NULL,
  `username` varchar(65) NOT NULL DEFAULT '',
  `password` varchar(65) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `firstname` varchar(65) NOT NULL,
  `lastname` varchar(65) NOT NULL,
  `status` int(11) NOT NULL,
  `profilepic` varchar(150) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `user_by` int(11) NOT NULL,
  `user_to` int(11) NOT NULL,
  `notification` text NOT NULL,
  `date` datetime NOT NULL,
  `seen` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `objectpropertyvalues`
--

CREATE TABLE `objectpropertyvalues` (
  `idobject` int(11) NOT NULL,
  `idclass` int(11) NOT NULL,
  `idclassproperty` int(11) NOT NULL,
  `value` varchar(45) DEFAULT NULL,
  `change_by` int(11) NOT NULL,
  `change_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `objectpropertyvalues`
--

INSERT INTO `objectpropertyvalues` (`idobject`, `idclass`, `idclassproperty`, `value`, `change_by`, `change_date`) VALUES
(1, 1, 1, 'test row 1 column 1', 8, '2018-06-11 05:43:06'),
(1, 1, 2, 'test row 2 column 2', 8, '2018-06-11 05:43:06'),
(1, 1, 3, 'Edited After inserting 3rd column', 8, '2018-06-11 05:43:51'),
(2, 1, 1, 'Added r2 f1', 8, '2018-06-11 05:44:16'),
(2, 1, 2, 'Added r2 f2', 8, '2018-06-11 05:44:16'),
(2, 1, 3, 'Added r2 f3', 8, '2018-06-11 05:44:16'),
(3, 2, 4, '1aaaa', 11, '2018-06-19 16:33:32'),
(3, 2, 5, '3', 11, '2018-06-19 16:33:32'),
(4, 2, 4, 'aaaa', 11, '2018-06-19 16:33:44'),
(4, 2, 5, '2', 11, '2018-06-19 16:33:49'),
(5, 2, 4, '1', 11, '2018-06-19 16:34:39'),
(5, 2, 5, '1bbbb', 11, '2018-06-19 16:34:39'),
(29, 14, 40, 'asdf', 7, '2018-09-07 07:17:09'),
(29, 14, 39, 'asd', 7, '2018-09-07 07:19:15'),
(27, 12, 37, 'ABC', 6, '2018-08-15 05:41:16'),
(28, 13, 31, 'Test1', 1, '2018-08-10 15:14:36'),
(8, 2, 4, '2', 11, '2018-06-19 16:43:27'),
(8, 2, 5, '2bbbb', 11, '2018-06-19 16:43:27'),
(8, 2, 8, '', 8, '2018-12-21 10:39:44'),
(8, 2, 9, '43', 11, '2018-06-19 16:43:27'),
(9, 5, 10, 'United Kingdom', 11, '2018-06-22 07:52:47'),
(9, 5, 11, 'UK', 11, '2018-06-22 07:52:47'),
(9, 5, 12, '52000000', 11, '2018-06-22 07:52:47'),
(10, 5, 10, 'United States', 11, '2018-06-22 07:53:02'),
(10, 5, 11, 'USA', 11, '2018-06-22 07:53:02'),
(10, 5, 12, '380000000', 11, '2018-06-22 07:53:02'),
(11, 4, 13, 'UK', 11, '2018-06-22 07:54:32'),
(11, 4, 14, 'UK', 4, '2018-06-26 21:32:38'),
(11, 4, 15, 'United Kingdom', 11, '2018-06-22 07:54:32'),
(12, 6, 16, 'first', 3, '2018-06-23 08:11:11'),
(13, 6, 16, 'second', 3, '2018-06-23 08:11:19'),
(14, 7, 17, 'first', 3, '2018-06-23 08:12:09'),
(15, 8, 18, '4308u', 1, '2018-06-26 17:40:23'),
(16, 8, 18, '4300u', 1, '2018-06-26 17:40:33'),
(17, 8, 18, '2510m', 1, '2018-06-26 17:40:46'),
(17, 8, 19, '2', 1, '2018-06-26 17:41:25'),
(16, 8, 19, '2', 1, '2018-06-26 17:41:33'),
(18, 9, 20, 'Intel', 1, '2018-06-26 17:42:23'),
(19, 9, 20, 'AMD', 1, '2018-06-26 17:42:31'),
(20, 9, 20, 'ARM', 1, '2018-06-26 17:42:43'),
(21, 9, 20, 'Qualcomm', 1, '2018-06-26 17:43:32'),
(22, 9, 20, 'Samsung', 1, '2018-06-26 17:43:41'),
(23, 9, 20, 'Toshiba', 1, '2018-06-26 17:43:50'),
(24, 9, 20, 'Apple', 1, '2018-06-26 17:43:57'),
(15, 8, 21, 'Intel', 1, '2018-06-26 17:45:44'),
(15, 8, 19, '2', 1, '2018-06-26 17:45:51'),
(17, 8, 21, 'Intel', 1, '2018-06-26 17:46:15'),
(25, 8, 18, 'A11', 1, '2018-06-26 17:47:14'),
(25, 8, 21, 'Apple', 1, '2018-06-26 17:47:14'),
(25, 8, 19, '6', 1, '2018-06-26 17:47:14'),
(5, 2, 8, '43', 4, '2018-06-26 21:25:11'),
(5, 2, 9, '23', 4, '2018-06-26 21:25:11'),
(26, 12, 26, 'Samsung', 14, '2018-07-31 14:04:01'),
(26, 12, 27, '199', 14, '2018-07-31 14:04:01'),
(27, 12, 26, 'Nokia', 14, '2018-07-31 14:25:56'),
(27, 12, 28, '3019', 14, '2018-07-31 14:25:56'),
(26, 12, 28, '222', 14, '2018-07-31 14:26:08'),
(18, 9, 30, '2018', 14, '2018-07-31 14:56:58'),
(29, 14, 41, '2123', 7, '2018-09-07 07:17:09'),
(29, 14, 42, '2018-09-13', 7, '2018-09-07 07:17:09'),
(29, 14, 43, '7554905554_1536322754.png', 7, '2018-09-07 07:19:15'),
(8, 2, 34, '', 8, '2018-12-21 10:39:44'),
(8, 2, 44, '5051381159_1545410384.jpg', 8, '2018-12-21 10:39:44'),
(30, 4, 14, 'USA', 5, '2018-12-30 18:02:32'),
(30, 4, 13, 'United States of America', 5, '2018-12-30 18:02:32'),
(30, 4, 15, 'United States', 5, '2018-12-30 18:02:32'),
(30, 4, 45, '', 4, '2019-03-12 08:15:43'),
(18, 9, 46, '', 2, '2019-01-15 11:52:38'),
(31, 18, 49, 'Belgium', 1, '2019-01-20 18:40:50'),
(32, 18, 49, 'France', 1, '2019-01-20 18:40:56'),
(33, 18, 49, 'USA', 1, '2019-01-20 18:41:04'),
(34, 18, 49, 'China', 1, '2019-01-20 18:41:15'),
(35, 17, 47, 'Mont Blanc', 1, '2019-01-20 18:42:47'),
(35, 17, 48, '4800', 1, '2019-01-20 18:42:47'),
(35, 17, 50, 'France', 1, '2019-01-20 18:43:33'),
(31, 18, 53, 'dfgdsfgsd', 1, '2019-01-20 18:52:02'),
(31, 18, 54, 'sdfsdf', 1, '2019-01-20 18:53:07'),
(32, 18, 54, 'sdfsdfsdfsdf', 1, '2019-01-20 18:53:14'),
(31, 18, 55, '', 2, '2019-01-29 14:23:22'),
(32, 18, 55, 'Europe', 1, '2019-02-03 17:17:31'),
(32, 18, 56, 'FR', 1, '2019-02-03 17:37:29'),
(36, 18, 49, 'Argentina', 1, '2019-02-03 18:01:16'),
(36, 18, 55, '', 1, '2019-02-03 18:01:16'),
(37, 21, 57, 'data 1', 4, '2019-03-12 08:19:10'),
(37, 21, 58, 'data 2', 4, '2019-03-12 08:19:10'),
(38, 22, 59, 'Dell', 10, '2019-03-12 10:10:02'),
(38, 22, 61, 'Latitude', 10, '2019-03-12 10:10:02'),
(38, 22, 60, '65000', 10, '2019-03-12 10:10:02'),
(39, 22, 59, 'Dell', 10, '2019-03-12 10:14:10'),
(39, 22, 61, 'Inspiron', 10, '2019-03-12 10:14:10'),
(39, 22, 60, '45000', 10, '2019-03-12 10:14:10'),
(40, 12, 38, '', 10, '2019-03-12 10:15:23'),
(40, 12, 37, '', 10, '2019-03-12 10:15:23'),
(40, 12, 26, '', 10, '2019-03-12 10:15:23'),
(40, 12, 62, 'Latitude', 10, '2019-03-12 10:15:23');

--
-- Triggers `objectpropertyvalues`
--
DELIMITER $$
CREATE TRIGGER `audit_objectpropertyvalues_insert` AFTER INSERT ON `objectpropertyvalues` FOR EACH ROW INSERT into `objectpropertyvalues_audit` (`idobject`,`idclass`, `idclassproperty`, `value`, `change_by`, `change_date`, `action`)
VALUES (NEW.idobject, NEW.idclass, NEW.idclassproperty, NEW.value, NEW.change_by, NOW(), 'created')
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `bafter_object_property_values_update` AFTER UPDATE ON `objectpropertyvalues` FOR EACH ROW BEGIN
    INSERT INTO objectpropertyvalues_audit
    SET 
    	idobject = NEW.idobject,
     	idclass = NEW.idclass,
        idclassproperty = NEW.idclassproperty,
        value = NEW.value,
        change_by = NEW.change_by,
        change_date = NOW(),
        action = 'updated';
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `objectpropertyvalues_audit`
--

CREATE TABLE `objectpropertyvalues_audit` (
  `idobject` int(11) NOT NULL,
  `idclass` int(11) NOT NULL,
  `idclassproperty` int(11) NOT NULL,
  `value` varchar(45) DEFAULT NULL,
  `change_by` int(11) NOT NULL,
  `change_date` datetime NOT NULL,
  `action` enum('created','updated','deleted') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `objectpropertyvalues_audit`
--

INSERT INTO `objectpropertyvalues_audit` (`idobject`, `idclass`, `idclassproperty`, `value`, `change_by`, `change_date`, `action`) VALUES
(1, 1, 1, 'test row 1 column 1', 8, '2018-06-11 05:43:06', 'created'),
(1, 1, 2, 'test row 2 column 2', 8, '2018-06-11 05:43:06', 'created'),
(1, 1, 3, 'Edited After inserting 3rd column', 8, '2018-06-11 05:43:51', 'created'),
(2, 1, 1, 'Added r2 f1', 8, '2018-06-11 05:44:16', 'created'),
(2, 1, 2, 'Added r2 f2', 8, '2018-06-11 05:44:16', 'created'),
(2, 1, 3, 'Added r2 f3', 8, '2018-06-11 05:44:16', 'created'),
(3, 2, 4, '1aaaa', 11, '2018-06-19 16:33:32', 'created'),
(3, 2, 5, '3', 11, '2018-06-19 16:33:32', 'created'),
(4, 2, 4, 'aaaa', 11, '2018-06-19 16:33:44', 'created'),
(4, 2, 5, '1', 11, '2018-06-19 16:33:44', 'created'),
(4, 2, 5, '2', 11, '2018-06-19 16:33:49', 'updated'),
(5, 2, 4, '1', 11, '2018-06-19 16:34:39', 'created'),
(5, 2, 5, '1bbbb', 11, '2018-06-19 16:34:39', 'created'),
(6, 3, 6, '23', 11, '2018-06-19 16:36:12', 'created'),
(6, 3, 7, 'Row 1 LU', 11, '2018-06-19 16:36:12', 'created'),
(7, 3, 6, '43', 11, '2018-06-19 16:36:25', 'created'),
(7, 3, 7, 'LU Row 2', 11, '2018-06-19 16:36:25', 'created'),
(7, 3, 7, 'Row 2 LU', 11, '2018-06-19 16:36:35', 'updated'),
(8, 2, 4, '2', 11, '2018-06-19 16:43:27', 'created'),
(8, 2, 5, '2bbbb', 11, '2018-06-19 16:43:27', 'created'),
(8, 2, 8, '23', 11, '2018-06-19 16:43:27', 'created'),
(8, 2, 9, '43', 11, '2018-06-19 16:43:27', 'created'),
(9, 5, 10, 'United Kingdom', 11, '2018-06-22 07:52:47', 'created'),
(9, 5, 11, 'UK', 11, '2018-06-22 07:52:47', 'created'),
(9, 5, 12, '52000000', 11, '2018-06-22 07:52:47', 'created'),
(10, 5, 10, 'United States', 11, '2018-06-22 07:53:02', 'created'),
(10, 5, 11, 'USA', 11, '2018-06-22 07:53:02', 'created'),
(10, 5, 12, '380000000', 11, '2018-06-22 07:53:02', 'created'),
(11, 4, 13, 'UK', 11, '2018-06-22 07:54:32', 'created'),
(11, 4, 14, 'UK', 11, '2018-06-22 07:54:32', 'created'),
(11, 4, 15, 'United Kingdom', 11, '2018-06-22 07:54:32', 'created'),
(12, 6, 16, 'first', 3, '2018-06-23 08:11:11', 'created'),
(13, 6, 16, 'second', 3, '2018-06-23 08:11:19', 'created'),
(14, 7, 17, 'first', 3, '2018-06-23 08:12:09', 'created'),
(15, 8, 18, '4308u', 1, '2018-06-26 17:40:23', 'created'),
(16, 8, 18, '4300u', 1, '2018-06-26 17:40:33', 'created'),
(17, 8, 18, '2510m', 1, '2018-06-26 17:40:46', 'created'),
(17, 8, 19, '2', 1, '2018-06-26 17:41:25', 'created'),
(16, 8, 19, '2', 1, '2018-06-26 17:41:33', 'created'),
(18, 9, 20, 'Intel', 1, '2018-06-26 17:42:23', 'created'),
(19, 9, 20, 'AMD', 1, '2018-06-26 17:42:31', 'created'),
(20, 9, 20, 'ARM', 1, '2018-06-26 17:42:43', 'created'),
(21, 9, 20, 'Qualcomm', 1, '2018-06-26 17:43:32', 'created'),
(22, 9, 20, 'Samsung', 1, '2018-06-26 17:43:41', 'created'),
(23, 9, 20, 'Toshiba', 1, '2018-06-26 17:43:50', 'created'),
(24, 9, 20, 'Apple', 1, '2018-06-26 17:43:57', 'created'),
(15, 8, 21, 'Intel', 1, '2018-06-26 17:45:44', 'created'),
(15, 8, 19, '', 1, '2018-06-26 17:45:44', 'created'),
(15, 8, 19, '2', 1, '2018-06-26 17:45:51', 'updated'),
(17, 8, 21, 'Intel', 1, '2018-06-26 17:46:15', 'created'),
(25, 8, 18, 'A11', 1, '2018-06-26 17:47:14', 'created'),
(25, 8, 21, 'Apple', 1, '2018-06-26 17:47:14', 'created'),
(25, 8, 19, '6', 1, '2018-06-26 17:47:14', 'created'),
(11, 4, 14, 'USA', 4, '2018-06-26 21:20:34', 'updated'),
(5, 2, 8, '43', 4, '2018-06-26 21:25:11', 'created'),
(5, 2, 9, '23', 4, '2018-06-26 21:25:11', 'created'),
(11, 4, 14, 'UK', 4, '2018-06-26 21:32:38', 'updated'),
(26, 12, 26, 'Samsung', 14, '2018-07-31 14:04:01', 'created'),
(26, 12, 27, '199', 14, '2018-07-31 14:04:01', 'created'),
(27, 12, 26, 'Nokia', 14, '2018-07-31 14:25:56', 'created'),
(27, 12, 28, '3019', 14, '2018-07-31 14:25:56', 'created'),
(26, 12, 28, '222', 14, '2018-07-31 14:26:08', 'created'),
(18, 9, 30, '2018', 14, '2018-07-31 14:56:58', 'created'),
(28, 13, 31, 'Test1', 1, '2018-08-10 15:14:36', 'created'),
(27, 12, 37, 'ABC', 6, '2018-08-15 05:41:16', 'created'),
(29, 14, 39, 'adsfadsf', 7, '2018-09-07 07:17:09', 'created'),
(29, 14, 40, 'asdf', 7, '2018-09-07 07:17:09', 'created'),
(29, 14, 41, '2123', 7, '2018-09-07 07:17:09', 'created'),
(29, 14, 42, '2018-09-13', 7, '2018-09-07 07:17:09', 'created'),
(29, 14, 43, '6318668064_1536322659.png', 7, '2018-09-07 07:17:40', 'created'),
(29, 14, 39, '', 7, '2018-09-07 07:18:57', 'updated'),
(29, 14, 43, '9488102739_1536322736.png', 7, '2018-09-07 07:18:57', 'updated'),
(29, 14, 39, 'asd', 7, '2018-09-07 07:19:15', 'updated'),
(29, 14, 43, '7554905554_1536322754.png', 7, '2018-09-07 07:19:15', 'updated'),
(8, 2, 34, '', 8, '2018-12-21 10:39:44', 'created'),
(8, 2, 8, '', 8, '2018-12-21 10:39:44', 'updated'),
(8, 2, 44, '5051381159_1545410384.jpg', 8, '2018-12-21 10:39:44', 'created'),
(30, 4, 14, 'USA', 5, '2018-12-30 18:02:32', 'created'),
(30, 4, 13, 'United States of America', 5, '2018-12-30 18:02:32', 'created'),
(30, 4, 15, 'United States', 5, '2018-12-30 18:02:32', 'created'),
(30, 4, 45, '2018', 5, '2018-12-30 18:49:24', 'created'),
(18, 9, 46, '2.0', 2, '2019-01-15 11:52:07', 'created'),
(18, 9, 46, '', 2, '2019-01-15 11:52:38', 'updated'),
(31, 18, 49, 'Belgium', 1, '2019-01-20 18:40:50', 'created'),
(32, 18, 49, 'France', 1, '2019-01-20 18:40:56', 'created'),
(33, 18, 49, 'USA', 1, '2019-01-20 18:41:04', 'created'),
(34, 18, 49, 'China', 1, '2019-01-20 18:41:15', 'created'),
(35, 17, 47, 'Mont Blanc', 1, '2019-01-20 18:42:47', 'created'),
(35, 17, 48, '4800', 1, '2019-01-20 18:42:47', 'created'),
(35, 17, 50, 'France', 1, '2019-01-20 18:43:33', 'created'),
(31, 18, 53, 'dfgdsfgsd', 1, '2019-01-20 18:52:02', 'created'),
(31, 18, 54, 'sdfsdf', 1, '2019-01-20 18:53:07', 'created'),
(32, 18, 54, 'sdfsdfsdfsdf', 1, '2019-01-20 18:53:14', 'created'),
(31, 18, 55, 'Europe', 2, '2019-01-29 14:22:54', 'created'),
(31, 18, 55, '', 2, '2019-01-29 14:23:22', 'updated'),
(32, 18, 55, 'Europe', 1, '2019-02-03 17:17:31', 'created'),
(32, 18, 56, 'FR', 1, '2019-02-03 17:37:29', 'created'),
(36, 18, 49, 'Argentina', 1, '2019-02-03 18:01:16', 'created'),
(36, 18, 55, '', 1, '2019-02-03 18:01:16', 'created'),
(30, 4, 45, '', 4, '2019-03-12 08:15:43', 'updated'),
(37, 21, 57, 'data 1', 4, '2019-03-12 08:19:10', 'created'),
(37, 21, 58, 'data 2', 4, '2019-03-12 08:19:10', 'created'),
(38, 22, 59, 'Dell', 10, '2019-03-12 10:10:02', 'created'),
(38, 22, 61, 'Latitude', 10, '2019-03-12 10:10:02', 'created'),
(38, 22, 60, '65000', 10, '2019-03-12 10:10:02', 'created'),
(39, 22, 59, 'Dell', 10, '2019-03-12 10:14:10', 'created'),
(39, 22, 61, 'Inspiron', 10, '2019-03-12 10:14:10', 'created'),
(39, 22, 60, '45000', 10, '2019-03-12 10:14:10', 'created'),
(40, 12, 38, '', 10, '2019-03-12 10:15:23', 'created'),
(40, 12, 37, '', 10, '2019-03-12 10:15:23', 'created'),
(40, 12, 26, '', 10, '2019-03-12 10:15:23', 'created'),
(40, 12, 62, 'Latitude', 10, '2019-03-12 10:15:23', 'created');

-- --------------------------------------------------------

--
-- Table structure for table `objects`
--

CREATE TABLE `objects` (
  `idobject` int(11) NOT NULL,
  `idclass` int(11) DEFAULT NULL,
  `Status` varchar(20) NOT NULL,
  `locked` tinyint(1) DEFAULT 0,
  `change_by` int(11) NOT NULL,
  `change_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `objects`
--

INSERT INTO `objects` (`idobject`, `idclass`, `Status`, `locked`, `change_by`, `change_date`) VALUES
(1, 1, '1', 0, 8, '0000-00-00 00:00:00'),
(2, 1, '1', 0, 8, '0000-00-00 00:00:00'),
(3, 2, '0', 0, 11, '2018-06-19 16:33:36'),
(4, 2, '0', 0, 11, '2018-06-19 16:34:06'),
(5, 2, '1', 0, 4, '0000-00-00 00:00:00'),
(29, 14, '1', 0, 7, '0000-00-00 00:00:00'),
(28, 13, '1', 0, 1, '0000-00-00 00:00:00'),
(8, 2, '0', 0, 8, '2018-12-21 10:39:53'),
(9, 5, '1', 0, 11, '0000-00-00 00:00:00'),
(10, 5, '1', 0, 11, '0000-00-00 00:00:00'),
(11, 4, '1', 0, 4, '0000-00-00 00:00:00'),
(12, 6, '1', 0, 3, '0000-00-00 00:00:00'),
(13, 6, '1', 0, 3, '0000-00-00 00:00:00'),
(14, 7, '1', 0, 3, '0000-00-00 00:00:00'),
(15, 8, '1', 0, 1, '0000-00-00 00:00:00'),
(16, 8, '1', 0, 1, '0000-00-00 00:00:00'),
(17, 8, '1', 0, 1, '0000-00-00 00:00:00'),
(18, 9, '1', 0, 2, '0000-00-00 00:00:00'),
(19, 9, '1', 0, 1, '0000-00-00 00:00:00'),
(20, 9, '1', 0, 1, '0000-00-00 00:00:00'),
(21, 9, '1', 0, 1, '0000-00-00 00:00:00'),
(22, 9, '1', 0, 1, '0000-00-00 00:00:00'),
(23, 9, '1', 0, 1, '0000-00-00 00:00:00'),
(24, 9, '1', 0, 1, '0000-00-00 00:00:00'),
(25, 8, '1', 0, 1, '0000-00-00 00:00:00'),
(26, 12, '0', 0, 14, '2018-07-31 14:29:29'),
(27, 12, '0', 0, 6, '2019-03-12 10:21:00'),
(30, 4, '1', 0, 4, '0000-00-00 00:00:00'),
(31, 18, '1', 1, 2, '0000-00-00 00:00:00'),
(32, 18, '1', 0, 1, '0000-00-00 00:00:00'),
(33, 18, '1', 0, 1, '0000-00-00 00:00:00'),
(34, 18, '1', 0, 1, '0000-00-00 00:00:00'),
(35, 17, '1', 0, 1, '0000-00-00 00:00:00'),
(36, 18, '1', 0, 1, '0000-00-00 00:00:00'),
(37, 21, '0', 0, 4, '2019-03-12 08:27:25'),
(38, 22, '1', 0, 10, '0000-00-00 00:00:00'),
(39, 22, '1', 0, 10, '0000-00-00 00:00:00'),
(40, 12, '0', 0, 10, '2019-03-12 10:20:57');

--
-- Triggers `objects`
--
DELIMITER $$
CREATE TRIGGER `audit_objects_insert` AFTER INSERT ON `objects` FOR EACH ROW INSERT into `objects_audit` (`idobject`,`idclass`, `Status`, `locked`, `change_by`, `change_date`, `action`)
VALUES (NEW.idobject, NEW.idclass, NEW.Status, NEW.locked, NEW.change_by, NOW(), `created`)
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `before_object_update` AFTER UPDATE ON `objects` FOR EACH ROW BEGIN
    INSERT INTO objects_audit
    SET
    	idobject = NEW.idobject,
     	idclass = NEW.idclass,
        Status = NEW.Status,
        locked = NEW.locked,
        change_by = NEW.change_by,
        change_date = NOW(),
        action = IF(NEW.Status = '0', 'deleted', 'updated');
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `objects_audit`
--

CREATE TABLE `objects_audit` (
  `id` int(11) NOT NULL,
  `idobject` int(11) NOT NULL,
  `idclass` int(11) DEFAULT NULL,
  `Status` varchar(20) NOT NULL,
  `locked` tinyint(1) DEFAULT 0,
  `change_by` int(11) NOT NULL,
  `change_date` datetime NOT NULL,
  `action` enum('created','updated','deleted') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `objects_audit`
--

INSERT INTO `objects_audit` (`id`, `idobject`, `idclass`, `Status`, `locked`, `change_by`, `change_date`, `action`) VALUES
(1, 1, 1, '1', 0, 8, '2018-06-11 05:43:51', 'updated'),
(2, 1, 1, '1', 0, 8, '2018-06-11 05:43:51', 'updated'),
(3, 1, 1, '1', 0, 8, '2018-06-11 05:43:51', 'updated'),
(4, 3, 2, '0', 0, 11, '2018-06-19 16:33:36', 'deleted'),
(5, 4, 2, '1', 0, 11, '2018-06-19 16:33:49', 'updated'),
(6, 4, 2, '1', 0, 11, '2018-06-19 16:33:49', 'updated'),
(7, 4, 2, '0', 0, 11, '2018-06-19 16:34:06', 'deleted'),
(8, 7, 3, '1', 0, 11, '2018-06-19 16:36:35', 'updated'),
(9, 7, 3, '1', 0, 11, '2018-06-19 16:36:35', 'updated'),
(10, 17, 8, '1', 0, 1, '2018-06-26 17:41:25', 'updated'),
(11, 17, 8, '1', 0, 1, '2018-06-26 17:41:25', 'updated'),
(12, 16, 8, '1', 0, 1, '2018-06-26 17:41:33', 'updated'),
(13, 16, 8, '1', 0, 1, '2018-06-26 17:41:33', 'updated'),
(14, 15, 8, '1', 0, 1, '2018-06-26 17:45:44', 'updated'),
(15, 15, 8, '1', 0, 1, '2018-06-26 17:45:44', 'updated'),
(16, 15, 8, '1', 0, 1, '2018-06-26 17:45:44', 'updated'),
(17, 15, 8, '1', 0, 1, '2018-06-26 17:45:51', 'updated'),
(18, 15, 8, '1', 0, 1, '2018-06-26 17:45:51', 'updated'),
(19, 15, 8, '1', 0, 1, '2018-06-26 17:45:51', 'updated'),
(20, 17, 8, '1', 0, 1, '2018-06-26 17:46:15', 'updated'),
(21, 17, 8, '1', 0, 1, '2018-06-26 17:46:15', 'updated'),
(22, 17, 8, '1', 0, 1, '2018-06-26 17:46:15', 'updated'),
(23, 11, 4, '1', 0, 4, '2018-06-26 21:20:34', 'updated'),
(24, 11, 4, '1', 0, 4, '2018-06-26 21:20:34', 'updated'),
(25, 11, 4, '1', 0, 4, '2018-06-26 21:20:35', 'updated'),
(26, 5, 2, '1', 0, 4, '2018-06-26 21:25:11', 'updated'),
(27, 5, 2, '1', 0, 4, '2018-06-26 21:25:11', 'updated'),
(28, 5, 2, '1', 0, 4, '2018-06-26 21:25:11', 'updated'),
(29, 5, 2, '1', 0, 4, '2018-06-26 21:25:11', 'updated'),
(30, 11, 4, '1', 0, 4, '2018-06-26 21:32:38', 'updated'),
(31, 11, 4, '1', 0, 4, '2018-06-26 21:32:38', 'updated'),
(32, 11, 4, '1', 0, 4, '2018-06-26 21:32:38', 'updated'),
(33, 26, 12, '1', 0, 14, '2018-07-31 14:26:08', 'updated'),
(34, 26, 12, '1', 0, 14, '2018-07-31 14:26:08', 'updated'),
(35, 26, 12, '0', 0, 14, '2018-07-31 14:29:29', 'deleted'),
(36, 18, 9, '1', 0, 14, '2018-07-31 14:56:58', 'updated'),
(37, 18, 9, '1', 0, 14, '2018-07-31 14:56:58', 'updated'),
(38, 27, 12, '1', 0, 6, '2018-08-15 05:41:16', 'updated'),
(39, 27, 12, '1', 0, 6, '2018-08-15 05:41:16', 'updated'),
(40, 29, 14, '1', 0, 7, '2018-09-07 07:17:40', 'updated'),
(41, 29, 14, '1', 0, 7, '2018-09-07 07:17:40', 'updated'),
(42, 29, 14, '1', 0, 7, '2018-09-07 07:17:40', 'updated'),
(43, 29, 14, '1', 0, 7, '2018-09-07 07:17:40', 'updated'),
(44, 29, 14, '1', 0, 7, '2018-09-07 07:17:40', 'updated'),
(45, 29, 14, '1', 0, 7, '2018-09-07 07:18:57', 'updated'),
(46, 29, 14, '1', 0, 7, '2018-09-07 07:18:57', 'updated'),
(47, 29, 14, '1', 0, 7, '2018-09-07 07:18:57', 'updated'),
(48, 29, 14, '1', 0, 7, '2018-09-07 07:18:57', 'updated'),
(49, 29, 14, '1', 0, 7, '2018-09-07 07:18:57', 'updated'),
(50, 29, 14, '1', 0, 7, '2018-09-07 07:19:15', 'updated'),
(51, 29, 14, '1', 0, 7, '2018-09-07 07:19:15', 'updated'),
(52, 29, 14, '1', 0, 7, '2018-09-07 07:19:15', 'updated'),
(53, 29, 14, '1', 0, 7, '2018-09-07 07:19:15', 'updated'),
(54, 29, 14, '1', 0, 7, '2018-09-07 07:19:15', 'updated'),
(55, 8, 2, '1', 0, 8, '2018-12-21 10:39:44', 'updated'),
(56, 8, 2, '1', 0, 8, '2018-12-21 10:39:44', 'updated'),
(57, 8, 2, '1', 0, 8, '2018-12-21 10:39:44', 'updated'),
(58, 8, 2, '1', 0, 8, '2018-12-21 10:39:44', 'updated'),
(59, 8, 2, '1', 0, 8, '2018-12-21 10:39:44', 'updated'),
(60, 8, 2, '0', 0, 8, '2018-12-21 10:39:53', 'deleted'),
(61, 30, 4, '1', 0, 5, '2018-12-30 18:49:24', 'updated'),
(62, 30, 4, '1', 0, 5, '2018-12-30 18:49:24', 'updated'),
(63, 30, 4, '1', 0, 5, '2018-12-30 18:49:24', 'updated'),
(64, 30, 4, '1', 0, 5, '2018-12-30 18:49:24', 'updated'),
(65, 18, 9, '1', 0, 2, '2019-01-15 11:52:07', 'updated'),
(66, 18, 9, '1', 0, 2, '2019-01-15 11:52:07', 'updated'),
(67, 18, 9, '1', 0, 2, '2019-01-15 11:52:07', 'updated'),
(68, 18, 9, '1', 0, 2, '2019-01-15 11:52:38', 'updated'),
(69, 18, 9, '1', 0, 2, '2019-01-15 11:52:38', 'updated'),
(70, 18, 9, '1', 0, 2, '2019-01-15 11:52:38', 'updated'),
(71, 35, 17, '1', 0, 1, '2019-01-20 18:43:33', 'updated'),
(72, 35, 17, '1', 0, 1, '2019-01-20 18:43:33', 'updated'),
(73, 35, 17, '1', 0, 1, '2019-01-20 18:43:33', 'updated'),
(74, 31, 18, '1', 0, 1, '2019-01-20 18:52:02', 'updated'),
(75, 31, 18, '1', 0, 1, '2019-01-20 18:52:02', 'updated'),
(76, 31, 18, '1', 0, 1, '2019-01-20 18:53:07', 'updated'),
(77, 31, 18, '1', 0, 1, '2019-01-20 18:53:07', 'updated'),
(78, 32, 18, '1', 0, 1, '2019-01-20 18:53:14', 'updated'),
(79, 32, 18, '1', 0, 1, '2019-01-20 18:53:14', 'updated'),
(80, 31, 18, '1', 0, 2, '2019-01-29 14:22:54', 'updated'),
(81, 31, 18, '1', 0, 2, '2019-01-29 14:22:54', 'updated'),
(82, 31, 18, '1', 0, 2, '2019-01-29 14:23:12', 'updated'),
(83, 31, 18, '1', 0, 2, '2019-01-29 14:23:12', 'updated'),
(84, 31, 18, '1', 0, 2, '2019-01-29 14:23:22', 'updated'),
(85, 31, 18, '1', 0, 2, '2019-01-29 14:23:22', 'updated'),
(86, 31, 18, '1', 1, 2, '2019-01-29 14:26:02', 'updated'),
(87, 32, 18, '1', 0, 1, '2019-02-03 17:17:31', 'updated'),
(88, 32, 18, '1', 0, 1, '2019-02-03 17:17:31', 'updated'),
(89, 32, 18, '1', 0, 1, '2019-02-03 17:34:12', 'updated'),
(90, 32, 18, '1', 0, 1, '2019-02-03 17:34:12', 'updated'),
(91, 32, 18, '1', 0, 1, '2019-02-03 17:37:29', 'updated'),
(92, 32, 18, '1', 0, 1, '2019-02-03 17:37:29', 'updated'),
(93, 32, 18, '1', 0, 1, '2019-02-03 17:37:29', 'updated'),
(94, 30, 4, '1', 0, 4, '2019-03-12 08:15:43', 'updated'),
(95, 30, 4, '1', 0, 4, '2019-03-12 08:15:43', 'updated'),
(96, 30, 4, '1', 0, 4, '2019-03-12 08:15:43', 'updated'),
(97, 30, 4, '1', 0, 4, '2019-03-12 08:15:43', 'updated'),
(98, 37, 21, '1', 1, 4, '2019-03-12 08:26:32', 'updated'),
(99, 37, 21, '1', 0, 4, '2019-03-12 08:27:23', 'updated'),
(100, 37, 21, '0', 0, 4, '2019-03-12 08:27:25', 'deleted'),
(101, 40, 12, '0', 0, 10, '2019-03-12 10:20:57', 'deleted'),
(102, 27, 12, '0', 0, 6, '2019-03-12 10:21:00', 'deleted');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `lname` varchar(255) NOT NULL,
  `uname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `status` enum('0','1') NOT NULL,
  `created` datetime NOT NULL,
  `profilepic` varchar(500) NOT NULL,
  `level` enum('admin','user') NOT NULL,
  `code` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `fname`, `lname`, `uname`, `email`, `pass`, `status`, `created`, `profilepic`, `level`, `code`) VALUES
(1, 'Olivier', 'Hustin', 'olivier.hustin@gmail.com', 'olivier.hustin@gmail.com', '98743224e6b1db3a99cb90c7bdf4ed5d', '1', '2018-06-21 12:50:47', '', 'admin', ''),
(2, 'Marianne', 'Hustin', 'marianne.hustin@gmail.com', 'marianne.hustin@gmail.com', 'abad6bb74132b7941c4ebff02f3fb651', '1', '2018-06-21 12:54:06', '', 'admin', ''),
(3, 'Shahid', 'Ullah Khan', '421985838271899', '421985838271899', '', '1', '2018-06-23 13:07:09', '', 'admin', ''),
(4, 'Farrakh', 'Kazmi', 'farrakh14@yahoo.com', 'farrakh14@yahoo.com', '6c15973b766108a4fda812110984fba5', '1', '2018-06-27 01:11:59', '', 'admin', ''),
(5, 'Franz', 'Car', 'farrakh.kazmi@gmail.com', 'farrakh.kazmi@gmail.com', '6c15973b766108a4fda812110984fba5', '1', '2018-07-31 19:56:00', '', 'user', ''),
(6, 'Arslan', 'Imran', '2132213773456740', '2132213773456740', '', '1', '2018-08-15 10:40:31', '', 'user', ''),
(7, 'shahid', 'khattak', 'shahid.khattak@gmail.com', 'shahid.khattak@gmail.com', '5f4dcc3b5aa765d61d8327deb882cf99', '1', '2018-09-07 12:13:52', '', 'user', ''),
(8, 'shahid', 'khan', 'shahid.khan@hashehouse.com', 'shahid.khan@hashehouse.com', '5f4dcc3b5aa765d61d8327deb882cf99', '1', '2018-12-21 16:37:39', '', 'user', ''),
(9, 'Olivier', 'Hustin2', 'olivier.hustin@indexopedia.com', 'olivier.hustin@indexopedia.com', '98743224e6b1db3a99cb90c7bdf4ed5d', '1', '2019-01-09 01:41:21', '', 'user', ''),
(10, 'yousaf', 'ali', 'yousaf@galaxmo.com', 'yousaf@galaxmo.com', '72bfb409ab10f1a38901327b5148cd96', '1', '2019-03-12 17:03:59', '', 'admin', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`idclass`),
  ADD UNIQUE KEY `classname_UNIQUE` (`classname`);

--
-- Indexes for table `classes_audit`
--
ALTER TABLE `classes_audit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `classproperties`
--
ALTER TABLE `classproperties`
  ADD PRIMARY KEY (`idclassproperty`),
  ADD UNIQUE KEY `Unicity1` (`idclass`,`propertyname`);

--
-- Indexes for table `classproperties_audit`
--
ALTER TABLE `classproperties_audit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `followers`
--
ALTER TABLE `followers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `followers_id_uindex` (`id`);

--
-- Indexes for table `kol`
--
ALTER TABLE `kol`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `maincategory`
--
ALTER TABLE `maincategory`
  ADD PRIMARY KEY (`catid`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `objectpropertyvalues`
--
ALTER TABLE `objectpropertyvalues`
  ADD PRIMARY KEY (`idobject`,`idclass`,`idclassproperty`);

--
-- Indexes for table `objects`
--
ALTER TABLE `objects`
  ADD PRIMARY KEY (`idobject`);

--
-- Indexes for table `objects_audit`
--
ALTER TABLE `objects_audit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `classes`
--
ALTER TABLE `classes`
  MODIFY `idclass` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `classes_audit`
--
ALTER TABLE `classes_audit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `classproperties`
--
ALTER TABLE `classproperties`
  MODIFY `idclassproperty` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=63;

--
-- AUTO_INCREMENT for table `classproperties_audit`
--
ALTER TABLE `classproperties_audit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=214;

--
-- AUTO_INCREMENT for table `followers`
--
ALTER TABLE `followers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `kol`
--
ALTER TABLE `kol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `maincategory`
--
ALTER TABLE `maincategory`
  MODIFY `catid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `objects`
--
ALTER TABLE `objects`
  MODIFY `idobject` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `objects_audit`
--
ALTER TABLE `objects_audit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
