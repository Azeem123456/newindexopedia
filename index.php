<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <base href="/">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Indexopedia - All Specifications of products in one place">
    <title>Indexopedia</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="./css/materialize.min.css">
    <link rel="stylesheet" href="./css/progress.css">
    <link rel="stylesheet" href="./css/style.css">
    <script type="text/javascript" src="./js/jquery.min.js"></script>
    <script type="text/javascript" src="./js/player.js"></script>
    <script type="text/javascript" src="./js/materialize.min.js"></script>
    <script type="text/javascript" src="./js/vue.js"></script>
    <script type="text/javascript" src="./js/vue-router.js"></script>
    <script type="text/javascript" src="./js/vue-http.js"></script>
    <script type="text/javascript" src="./js/fb_login.js"></script>

   <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

      <style>
        nav ul a{
            color:#94ba33;
        }
    </style>
  </head>
  <body>
<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '237051290177708',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.12'
    });
      
    FB.AppEvents.logPageView();   
      
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
    <div id="indexopedia-main">
      <transition name="slidefade" mode="out-in">
        <router-view></router-view>
      </transition>
      <div id="modal1" class="modal">
        <div class="modal-content">
          <h4 id="modal-header">Modal Header</h4>
          <p id="modal-body">A bunch of text</p>
        </div>
        <div class="modal-footer" id="modal-footer">
          <a onClick="hideModal()" class="waves-effect waves-green btn-flat">OK</a>
        </div>
      </div>
    </div>
    <script type="text/x-template" id="header-template">
      <?php include("./header.php"); ?>
    </script>
    <script type="text/x-template" id="home-template">
      <?php include("./home.php"); ?>
    </script>
    <script type="text/x-template" id="maincategory-template">
      <?php include("./maincategories.php"); ?>
    </script>
    <script type="text/x-template" id="category-template">
      <?php include("./category.php"); ?>
    </script>
    <script type="text/x-template" id="class-template">
      <?php include("./class.php"); ?>
    </script>
    <script type="text/x-template" id="details-template">
      <?php include("./details.php"); ?>
    </script>
    <script type="text/x-template" id="login-template">
      <?php include("./login.php"); ?>
    </script>
    <script type="text/x-template" id="signup-template">
      <?php include("./signup.php"); ?>
    </script>
    <script type="text/x-template" id="settings-template">
      <?php include("./settings.php"); ?>
    </script>
    <script type="text/x-template" id="notification-template">
      <?php include("./notifications.php"); ?>
    </script>
    <script type="text/x-template" id="aboutus-template">
      <?php include("./aboutus.php"); ?>
    </script>
    <script type="text/x-template" id="privacypolicy-template">
      <?php include("./privacypolicy.php"); ?>
    </script>
    <script type="text/x-template" id="termsandconds-template">
      <?php include("./termsandconditions.php"); ?>
    </script>
  </body>
  <script type="text/javascript" src="./js/process.js"></script>
</html>
