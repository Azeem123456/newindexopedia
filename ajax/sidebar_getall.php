<?php
  include("./mysqli.php");
  $query = $mysqli->query("SELECT maincategory.*, (SELECT count(*) FROM classes WHERE CatId=maincategory.catid AND Status='1') AS classcount FROM maincategory WHERE parentcatid='0' ORDER BY catname ASC");
  $categories = array();
  while($row = $query->fetch_assoc()){
    $subcategories = array();
    array_push($categories, array("category"=> $row, "subcategories"=>$subcategories));
  }

  echo json_encode($categories);
?>
