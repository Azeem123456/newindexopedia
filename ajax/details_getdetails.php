<?php
  include("./mysqli.php");
    $classid = $mysqli->real_escape_string(trim($_GET['id']));
    $classdetailsquery = $mysqli->query("SELECT * FROM classes WHERE idclass='$classid'");
    $classdetails = $classdetailsquery->fetch_assoc();
    $parentdq = $mysqli->query("SELECT * FROM maincategory WHERE catid='".$classdetails['CatId']."'");
    $row = $parentdq->fetch_assoc();
    $maincatdq = $mysqli->query("SELECT * FROM maincategory WHERE catid='".$row['parentcatid']."'");
    $rowd = $maincatdq->fetch_assoc();

    //  Class Properties

    $classpropertiesquery = $mysqli->query("SELECT * FROM classproperties WHERE idclass='$classid' AND status='1' order by propertyorder ASC");
    $classproperties = array();
    while($r = $classpropertiesquery->fetch_assoc()){
      array_push($classproperties, $r);
    }

    //  Objects

    $objectsquery = $mysqli->query("SELECT * FROM objects WHERE idclass='$classid' AND Status='1'");
    $objects = array();
    while($r = $objectsquery->fetch_assoc()){
      array_push($objects, $r);
    }

    // Object Property Values
    $classpropertyvalues = array();
    foreach ($objects as $object) {
      $idobject = $object['idobject'];
      $classpropertyvaluequery = $mysqli->query("SELECT * FROM objectpropertyvalues WHERE idobject='$idobject'");
      while($r = $classpropertyvaluequery->fetch_assoc()){
        array_push($classpropertyvalues, $r);
      }
    }

    $getallkols = $mysqli->query("SELECT * FROM kol WHERE idclass='$classid'");
    $kols = array();
    while($kolrow = $getallkols->fetch_assoc()){
      array_push($kols, $kolrow);
    }

    $getallfollowers = $mysqli->query("SELECT * FROM followers WHERE idclass='$classid'");
    $followers = array();
    while($followerrow = $getallfollowers->fetch_assoc()){
      array_push($followers, $followerrow);
    }

    $lookuptablesall = $mysqli->query("SELECT * FROM classes ORDER BY classname ASC");
    $lookuptables = array();
    while($lookuprow = $lookuptablesall->fetch_assoc()){
      array_push($lookuptables, $lookuprow);
    }

    $lookuppropsall = $mysqli->query("SELECT * FROM classproperties ORDER BY propertyname ASC");
    $lookupprops = array();
    while($lookuprow = $lookuppropsall->fetch_assoc()){
      array_push($lookupprops, $lookuprow);
    }

    $lookupvaluesall = $mysqli->query("SELECT * FROM objectpropertyvalues");
    $lookupvalues = array();
    while($lookupvaluerow = $lookupvaluesall->fetch_assoc()){
      array_push($lookupvalues, $lookupvaluerow);
    }
    echo json_encode(array("parent"=>$row,"subparent"=>$rowd,"classdetails"=>$classdetails,
                           "table"=>array("classprops"=>$classproperties, "objects"=>$objects, "propvalues"=>$classpropertyvalues),
                           "kols"=>$kols, "followers"=>$followers, "lookuptables"=>$lookuptables, "lookupprops"=> $lookupprops,
                           "lookupfields"=>"", "lookupvalues"=>$lookupvalues
                          ));
?>
