<?php
  include("./mysqli.php");
    $parent_id = $mysqli->real_escape_string(trim($_GET['id']));
    $userid = $mysqli->real_escape_string(trim(@$_GET['uid']));
    
    $subcatsquery = $mysqli->query(
        "SELECT c.*, GROUP_CONCAT(f.iduser), 
        (SELECT COUNT(*) FROM objects WHERE idclass=c.idclass AND Status='1') AS objectscount, 
        (SELECT COUNT(*) FROM followers WHERE iduser='$userid' AND idclass=c.idclass) AS isfollowing
        FROM classes c LEFT JOIN followers f ON f.idclass=c.idclass WHERE c.Catid='$parent_id' GROUP BY c.idclass ORDER BY c.classname ASC");
    $subcategories = array();
    while($subrow = $subcatsquery->fetch_assoc()){
      array_push($subcategories, $subrow);
    }
    $parentdq = $mysqli->query("SELECT * FROM maincategory WHERE catid='$parent_id'");
    $row = $parentdq->fetch_assoc();
    $maincatdq = $mysqli->query("SELECT * FROM maincategory WHERE catid='".$row['parentcatid']."'");
    $rowd = $maincatdq->fetch_assoc();
    
    $maincategories = $mysqli->query("SELECT * FROM maincategory");
    $maincats = array();
    while($myrow = $maincategories->fetch_assoc()){
        array_push($maincats, $myrow);
    }
    echo json_encode(array("parent"=>$row,"subparent"=>$rowd,"cats"=>$subcategories, "maincategories"=> $maincats));
?>
