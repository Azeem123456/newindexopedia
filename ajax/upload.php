<?php
    $dir = "../uploads/";
    $uniquenum = mt_rand(1000000000,9999999999);
    $uniquename = $uniquenum."_".time().".".pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);

    if (file_exists("../uploads/")) {
        move_uploaded_file($_FILES["file"]["tmp_name"], $dir.$uniquename);
    } else {
        mkdir("../uploads/");
        move_uploaded_file($_FILES["file"]["tmp_name"], $dir.$uniquename);
    }
    echo $uniquename;
?>