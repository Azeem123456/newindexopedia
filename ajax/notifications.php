<?php
require_once("mysqli.php");
$classtrailsquery = $mysqli->query("SELECT classes_audit.*, u.fname AS firstname FROM classes_audit INNER JOIN user u ON u.id=classes_audit.change_by ORDER BY classes_audit.change_date DESC ");
$classes_audit = array();
while($row = $classtrailsquery -> fetch_assoc()){
    array_push($classes_audit, $row);
}

$classesquery = $mysqli->query("SELECT classes.*, u.fname AS firstname FROM classes INNER JOIN user u ON u.id=classes.change_by ORDER BY classes.change_date DESC");
$classes = array();
while($row = $classesquery->fetch_assoc()){
    array_push($classes, $row);
}


$classpropertiestrailsquery = $mysqli->query("SELECT classproperties_audit.*, u.fname AS username, c.classname AS classname FROM classproperties_audit INNER JOIN classes c ON c.idclass=classproperties_audit.idclass INNER JOIN user u ON u.id=classproperties_audit.change_by ORDER BY classproperties_audit.change_date DESC");
$classes_properties_audit = array();
while($row = $classpropertiestrailsquery -> fetch_assoc()){
    array_push($classes_properties_audit, $row);
}

$classpropertyquery =  $mysqli->query("SELECT classproperties.*, u.fname AS username, c.classname AS classname FROM classproperties INNER JOIN classes c ON c.idclass=classproperties.idclass INNER JOIN user u ON u.id=classproperties.change_by ORDER BY classproperties.change_date DESC");
$classproperties = array();
while($row = $classpropertyquery->fetch_assoc()){
    array_push($classproperties, $row);
}



$objectpropertyvaluesquery = $mysqli->query("SELECT objectpropertyvalues.*, c.classname AS classname, cp.propertyname AS propname, u.fname AS username FROM objectpropertyvalues INNER JOIN classes c ON c.idclass=objectpropertyvalues.idclass INNER JOIN classproperties cp ON cp.idclassproperty=objectpropertyvalues.idclassproperty INNER JOIN user u ON u.id=objectpropertyvalues.change_by ORDER BY objectpropertyvalues.change_date DESC");
$object_property_values= array();
while($row = $objectpropertyvaluesquery -> fetch_assoc()){
    array_push($object_property_values, $row);
}

$objectpropertyvaluesauditquery = $mysqli->query("SELECT objectpropertyvalues_audit.*, c.classname AS classname, cp.propertyname AS propname, u.fname AS username FROM objectpropertyvalues_audit INNER JOIN classes c ON c.idclass=objectpropertyvalues_audit.idclass INNER JOIN classproperties cp ON cp.idclassproperty=objectpropertyvalues_audit.idclassproperty INNER JOIN user u ON u.id=objectpropertyvalues_audit.change_by ORDER BY objectpropertyvalues_audit.change_date DESC");
$object_property_values_audit= array();
while($row = $objectpropertyvaluesauditquery -> fetch_assoc()){
    array_push($object_property_values_audit, $row);
}



echo json_encode(
    array(
        "classes" => $classes,
        "classes_audit" => $classes_audit,
        "classproperties" => $classproperties,
        "classproperties_audit" => $classes_properties_audit,
        "propertyvalues" => $object_property_values,
        "propertyvaluesaudit" => $object_property_values_audit
        
    )
);
?>
