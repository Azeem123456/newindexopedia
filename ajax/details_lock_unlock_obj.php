<?php
require_once("mysqli.php");
$data = json_decode(file_get_contents("php://input"));
$type = trim($mysqli->real_escape_string($data->type));

if($type == "lock"){
    $idobject = trim($mysqli->real_escape_string($data->idobject));
    $mysqli->query("UPDATE objects SET locked='1' WHERE idobject='$idobject'");
}else if($type == "unlock"){
    $idobject = trim($mysqli->real_escape_string($data->idobject));
    $mysqli->query("UPDATE objects SET locked='0' WHERE idobject='$idobject'");
}else if($type == "follow"){
    $iduser = trim($mysqli->real_escape_string($data->iduser));
    $idclass = trim($mysqli->real_escape_string($data->idclass));
    $mysqli->query("INSERT INTO followers (iduser, idclass) VALUES ('$iduser', '$idclass')");
}else if($type == "unfollow"){
    $iduser = trim($mysqli->real_escape_string($data->iduser));
    $idclass = trim($mysqli->real_escape_string($data->idclass));
    $mysqli->query("DELETE FROM followers WHERE iduser='$iduser' AND idclass='$idclass'");
}
echo "success";
?>
