<?php
  include("./mysqli.php");
    $parent_id = $mysqli->real_escape_string(trim($_GET['uid']));
    $classesquery = $mysqli->query("SELECT c.idclass,c.classname,c.idowner,c.classdescription,c.CatId,c.Status, c.locked FROM classes c
                                    LEFT JOIN kol k ON k.idclass=c.idclass
                                    WHERE c.idowner='$parent_id' OR k.iduser='$parent_id'");
    $classes = array();
    while($subrow = $classesquery->fetch_assoc()){
      array_push($classes, $subrow);
    }
    $usersquery = $mysqli->query("SELECT * FROM user");
    $users = array();
    while($row = $usersquery->fetch_assoc()){
      array_push($users, $row);
    }

    $kolquery = $mysqli->query("SELECT k.id, k.idclass,k.iduser,k.access_level,k.granted_date,k.granted_by, u.fname, u.lname, u.uname, c.classname, c.classdescription, c.locked FROM kol k
                                INNER JOIN user u ON u.id=k.iduser
                                INNER JOIN classes c ON c.idclass=k.idclass
                                WHERE granted_by='$parent_id'");
    $kols = array();
    while($row = $kolquery->fetch_assoc()){
      array_push($kols, $row);
    }


    echo json_encode(array(
                    "classes" => $classes,
                    "users" => $users,
                    "kols" => $kols
                     ));
?>
