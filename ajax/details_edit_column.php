<?php
require_once("mysqli.php");
$data = json_decode(file_get_contents("php://input"));
$id = trim($mysqli->real_escape_string($data->id));
$name = trim($mysqli->real_escape_string($data->name));
$order = trim($mysqli->real_escape_string($data->order));
$type = trim($mysqli->real_escape_string($data->type));
$user = trim($mysqli->real_escape_string($data->user));

$old_order_query = $mysqli->query("SELECT * FROM classproperties WHERE idclassproperty='$id'")->fetch_assoc();
$old_order = $old_order_query['propertyorder'];

$order_check = $mysqli->query("SELECT * FROM classproperties WHERE propertyorder='$order' ");
if($order_check -> num_rows > 0){
    $order_clash_row = $order_check->fetch_assoc();
    
    $mysqli->query("UPDATE classproperties SET propertyorder='$old_order' WHERE idclassproperty='".$order_clash_row['idclassproperty']."'");
}
$mysqli->query("UPDATE classproperties SET propertyname='$name', propertyorder='$order', propertytype='$type', change_by='$user', change_date=NOW() WHERE idclassproperty='$id'");
echo "success";
?>
