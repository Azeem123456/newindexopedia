<?php
include("./mysqli.php");
$data = json_decode(file_get_contents('php://input'));
$search_keyword = $data->keyword;

function hasSubCats($id, $mysqli){
    $results = $mysqli->query("SELECT * FROM maincategory WHERE parentcatid='$id'");
    if($results->num_rows > 0){
        return true;
    }
    return false;
}
?>
<?php
    $sql="SELECT * FROM classes WHERE (classname LIKE '%$search_keyword%' OR classdescription LIKE '%$search_keyword%') AND status='1'";
    $q = $mysqli->query($sql);
    $maincats_ = array();
    $row=$q->fetch_assoc();
    if(sizeof($row) ==0){
        ?>
        <span style="padding-left: 700px;"> Record not Found </span>
        <?php
        return;
    }

    while($row=$q->fetch_assoc()){
        $maincats_[] = $row;
    }
foreach($maincats_ as $maincat_):
    ?>
    <li class="z-depth-1" style="padding:10px;border-bottom:1px solid grey;">
        <a style="font-weight:bold;" href='/details/<?php echo $maincat_['idclass'];?>'><?php echo $maincat_['classname'];?></a>
            <br>
            <span> Table Match : YES </span>
            <span> Column Match: NO </span>
            <span> Data: 0 </span>
    </li>
<?php
endforeach;


$sql="SELECT * FROM maincategory WHERE catname LIKE '%$search_keyword%' OR catdescription LIKE '%$search_keyword%'";
$q = $mysqli->query($sql);
$maincats_ = array();

while($row = $q->fetch_assoc()){
    $maincats_[] = $row;
}

foreach($maincats_ as $maincat_):
    ?>
    <li class="z-depth-1" style="padding:10px;border-bottom:1px solid grey;">
        <?php if(hasSubCats($maincat_['catid'], $mysqli)):?>
        <a style="font-weight:bold;" href='/category/<?php echo $maincat_['catid'];?>'><?php echo $maincat_['catname'];?></a>
            <?php else: ?>
            <a style="font-weight:bold;" href='/class/<?php echo $maincat_['catid'];?>'><?php echo $maincat_['catname'];?></a>
                <?php endif; ?><br>
                <span> Table Match : YES </span>
                <span> Column Match: NO </span>
                <span> Data: 0 </span>
    </li>
<?php
endforeach;



$sql="select c.classname,c.idclass, count(opv.value) AS columns, COUNT(DISTINCT o.idobject) as rows, IF(c.classname LIKE '%$search_keyword%','1','0') AS classesMatched,
        (SELECT COUNT(*) FROM classproperties WHERE propertyname LIKE '%$search_keyword%' AND idclass=c.idclass) AS columnsMatched
         from objectpropertyvalues opv 
                left join objects o on o.idobject=opv.idobject
                left join classes c on c.idclass=o.idclass
                WHERE (opv.value LIKE '%$search_keyword%' OR (SELECT COUNT(*) FROM classproperties WHERE propertyname LIKE '%$search_keyword%' AND idclass=c.idclass) > 0) AND c.classname <> ''
                group by c.classname
            ";
$q = $mysqli->query($sql);
$maincategories = array();
while($row = $q->fetch_assoc()){
    $maincategories[] = $row;
}
foreach ($maincategories as $main_category) :
    if($main_category['classname'] != ''):
        ?>
        <?php if($main_category['classesMatched'] == 0 && $main_category['columnsMatched'] == 0 && $main_category['rows'] == 0):?>
        <li class="z-depth-1" style="padding:10px;border-bottom:1px solid grey;"><a style="font-weight:bold;" href="/maincategories"></a><br />
                <span><?php echo $main_category['classesMatched'] > 0 ? 'Table Match: YES,': 'Table Match: NO,'; ?> </span>
                <?php echo $main_category['columnsMatched'] > 0 ? 'Column Match: YES,': 'Column Match: NO,'; ?> <?php echo "Data: ".$main_category['rows'];?>
        </li>
        <?php else : ?>
        <li class="z-depth-1" style="padding:10px;border-bottom:1px solid grey;"><a style="font-weight:bold;" href="/details/<?php echo $main_category['idclass'];?><?php if($main_category['classesMatched'] == 0 && $main_category['rows'] > 0):?>&s=<?php echo $search_keyword;endif;?>"><?php echo $main_category['classname'];?></a><br />
                <span><?php echo $main_category['classesMatched'] > 0 ? 'Table Match: YES,': 'Table Match: NO,'; ?> </span>
                <?php echo $main_category['columnsMatched'] > 0 ? 'Column Match: YES,': 'Column Match: NO,'; ?> <?php echo "Data: ".$main_category['rows'];?>
        </li>
        <?php
        endif;
    endif;
endforeach;
?>