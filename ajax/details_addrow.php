<?php
require_once("mysqli.php");
$data = json_decode(file_get_contents("php://input"));
$type = $data->type;
$myrow = $data->row;
$datenow = date('Y-m-d H:i:s');
if($type == "add"){
  $idclass = trim($mysqli->real_escape_string($myrow->idclass));
  $user = trim($mysqli->real_escape_string($myrow->user));
  $props = $myrow->props;
  $mysqli->query("INSERT INTO objects (`idobject`, `idclass`,`Status`, `change_by`) VALUES ('', '$idclass', '1', '$user')");
  $objj = $mysqli->query("SELECT * FROM objects ORDER BY idobject DESC LIMIT 1")->fetch_assoc();
  $idobject = $objj['idobject'];
  foreach ($props as $prop) {
      
    $idclassproperty = $mysqli->real_escape_string($prop->idclassproperty);
    $value = $mysqli->real_escape_string($prop->value);
    $mysqli->query("INSERT INTO objectpropertyvalues (`idobject`,`idclass`,`idclassproperty`,`value`, `change_by`, `change_date`) VALUES ('$idobject', '$idclass', '$idclassproperty', '$value', '$user', NOW())");
        
    //   $followers = $mysqli->query("SELECT * FROM followers WHERE idclass='$idclass'");
    //   $notification = "A new row has been added to <a href='/details/".$idclass."'>".$myrow->classname."</a> By ".$myrow->userfullname." on ".date('Y-m-d H:i:s');
    //   $notification = $mysqli->real_escape_string(trim($notification));
    //   while($fol = $followers->fetch_assoc()){
    //      $iduser = $fol['iduser'];
    //      $mysqli->query("INSERT INTO notifications (`user_by`, `user_to`, `notification`, `date`, `seen`) VALUES ('0', '$iduser', '$notification', '$datenow', '0')");
    //   }
      
    //   $kols = $mysqli->query("SELECT * FROM kol WHERE idclass='$idclass'");
    //   while($fol = $kols->fetch_assoc()){
    //       $iduser = $fol['iduser'];
    //       $mysqli->query("INSERT INTO notifications (`user_by`, `user_to`, `notification`, `date`, `seen`) VALUES ('0', '$iduser', '$notification', '$datenow', '0')");
    //   }
      
    //   $listowners = $mysqli->query("SELECT user.* FROM classes class INNER JOIN user ON user.id=class.idowner WHERE class.idclass='$idclass'");
    //     while($lowner = $listowners->fetch_assoc()){
    //         $iduser = $lowner['id'];
    //         $mysqli->query("INSERT INTO notifications (`user_by`, `user_to`, `notification`, `date`, `seen`) VALUES ('0', '$iduser', '$notification', '$datenow', '0')");
    //     }
  }
  echo "success";
}else if($type == "edit"){
    $idclass = trim($mysqli->real_escape_string($myrow->idclass));
    $user = trim($mysqli->real_escape_string($myrow->user));
    $props = $myrow->props;
    $idobject = $myrow->idobject;
    foreach ($props as $prop) {
        $idclassproperty = $mysqli->real_escape_string($prop->idclassproperty);
        $value = $mysqli->real_escape_string($prop->value);
        
        if($mysqli->query("SELECT * FROM objectpropertyvalues WHERE idobject='$idobject' AND idclass='$idclass' AND idclassproperty='$idclassproperty'")->num_rows > 0){
            $mysqli->query("UPDATE objectpropertyvalues SET value='$value', change_by='$user', change_date=NOW() WHERE idclassproperty='$idclassproperty' AND idclass='$idclass' AND idobject='$idobject' AND value <> '$value'"
                );
        }else{
            $mysqli->query("INSERT INTO objectpropertyvalues (`idobject`,`idclass`,`idclassproperty`,`value`, `change_by`, `change_date`) VALUES ('$idobject', '$idclass', '$idclassproperty', '$value', '$user', NOW())");
        }
        
        $mysqli->query("UPDATE objects SET change_by='$user' WHERE idobject='$idobject'");
    }
    
    // $followers = $mysqli->query("SELECT * FROM followers WHERE idclass='$idclass'");
    // $notification = "A row has been edited in <a href='/details/".$idclass."'>".$myrow->classname."</a> By ".$myrow->userfullname." on ".date('Y-m-d H:i:s');
    // $notification = $mysqli->real_escape_string(trim($notification));
    // while($fol = $followers->fetch_assoc()){
    //     $iduser = $fol['iduser'];
    //     $mysqli->query("INSERT INTO notifications (`user_by`, `user_to`, `notification`, `date`, `seen`) VALUES ('0', '$iduser', '$notification', '$datenow', '0')");
    // }

    // $kols = $mysqli->query("SELECT * FROM kol WHERE idclass='$idclass'");
    // while($fol = $kols->fetch_assoc()){
    //     $iduser = $fol['iduser'];
    //     $mysqli->query("INSERT INTO notifications (`user_by`, `user_to`, `notification`, `date`, `seen`) VALUES ('0', '$iduser', '$notification', '$datenow', '0')");
    // }
    
    // $listowners = $mysqli->query("SELECT user.* FROM classes class INNER JOIN user ON user.id=class.idowner WHERE class.idclass='$idclass'");
    // while($lowner = $listowners->fetch_assoc()){
    //     $iduser = $lowner['id'];
    //     $mysqli->query("INSERT INTO notifications (`user_by`, `user_to`, `notification`, `date`, `seen`) VALUES ('0', '$iduser', '$notification', '$datenow', '0')");
    // }
    echo "success";
}else if($type == "delete"){
    $idclass = $myrow->idclass;
    $idobject = $myrow->idobject;
    $chkrow = $mysqli->query("SELECT * FROM objectpropertyvalues WHERE idobject='$idobject' AND idclass='$idclass' AND value <> ''");
    while($row = $chkrow->fetch_assoc()){
        $idclassproperty = $row['idclassproperty'];
        if($mysqli->query("SELECT * FROM classproperties WHERE propertylookupfield='$idclassproperty'")->num_rows > 0){
            echo "islookup";
            die();
        }
    }
    
    $mysqli->query("UPDATE objects set Status='0', change_date=NOW() WHERE idobject='$idobject'");
    echo "success";
}
?>
